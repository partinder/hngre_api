'use strict';

/**
 * Module dependencies
 *
 * @type       {Function}
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var utils = require('../lib/utils');
var winston = require('../logger');

/**
 *  Group Schema
 */

var PredictionSchema = new Schema({
    userId: {
        type: String,
        required: true,
        trim: true
    },
    merchantId: {
        type: String,
        required: true,
        trim: true
    },
    dishId: {
        type: String,
        required: true,
        trim: true
    },
    score: {
        type: Number
    },
    veg_scale: {
        type: Number
    },
    location: {
        type: {
            type: String
        },
        coordinates: [Number]
    },
    address: {
        landmark: {
            type: String
        },
        zip: {
            type: String
        },
        region: {
            type: String
        },
        locality: {
            type: String
        },
        street_address: {
            type: String
        },
        neighbourhood: [String],
        country: {
            type: String
        }
    }
}, {
    collection: 'predictions'
});

/**
 *  Group static
 */
PredictionSchema.statics = {};

var Prediction = mongoose.model('Prediction', PredictionSchema);

module.exports = Prediction;