/*
    USER
    Model representing a user.
 */

var Mongoose = require('mongoose');
var Async = require('async');
var winston = require('../logger');

var DeviceSchema = require('./device');

var LoginDataSchema = new Mongoose.Schema({
    type: String,
    date: Date,
    data: {}
}, {
    _id: false
});

function getPhone(phone) {
    /*    phone = phone.split('');
    var phone1 = phone.splice(phone.length-4,4);
    var phone2 = phone.splice(phone.length-4,3);
    return phone.join('') + " " + phone2.join('') + " "+ phone1.join('');*/
    return phone;
}

function setPhone(phone) {
    return phone.replace(/[ \-\(\)\.]*/g, '');;
}

var UserSchema = new Mongoose.Schema({

    name: String,
    first_name: String,
    last_name: String,
    bio: String,
    website: String,
    website_display_name: String,
    location: String,

    birthday: Date,
    role: {
        type: String,
        enum: ['contributor', 'regular'],
        default: 'regular'
    },
    brithday_source: {
        type: String,
        enum: ['facebook', 'google', 'user', 'system'],
        default: 'system'
    },
    gender: {
        type: String,
        lowercase: true,
        enum: ['female', 'male']
    },
    veg_scale: {
        type: Array,
        default: [1, 2, 3]
    },

    email: {
        type: String,
        unique: true,
        sparse: true,
        lowercase: true
    },
    email_verified: {
        type: Boolean,
        default: false
    },
    phone_verified: {
        type: Boolean,
        default: false
    },

    phone: {
        type: String,
        unique: true,
        sparse: true,
        get: getPhone,
        set: setPhone
    },
    facebook: {
        type: String,
        unique: true,
        sparse: true
    },
    google: {
        type: String,
        unique: true,
        sparse: true
    },
    twitter: {
        type: String,
        unique: true,
        sparse: true
    },
    foursquare: {
        type: String,
        unique: true,
        sparse: true
    },

    login: [LoginDataSchema],
    devices: [DeviceSchema],

    profile_image: String,

    mixpanel_id: String,

    mixpanel_server_id: String,

    full_contacts: {},

    enabled: {
        type: Boolean,
        default: true
    },
    created: {
        type: Date,
        default: Date.now
    }

});

/**
 * Adds or updates a device for a given user.
 */
UserSchema.method('addOrUpdateDevice', function(deviceObject, cb) {

    if (!deviceObject.identifier) return cb(new Error("No device identifier found"));

    deviceObject.identifier = deviceObject.identifier.toUpperCase();
    deviceObject.last_seen = Date.now();

    var device = _.findWhere(this.devices, {
        identifier: deviceObject.identifier
    });

    if (!device) {
        this.devices.push(deviceObject);
    } else {
        var index = _.indexOf(this.devices, device);
        _.extend(device, deviceObject);
        this.devices[index] = device;
    }
    this.save(cb);
});

/**
 * Remove or updates a device for a given user.
 */
UserSchema.method('removeOrUpdateDevice', function(deviceObject, cb) {

    if (!deviceObject.identifier) return cb(new Error("No device identifier found"));

    deviceObject.identifier = deviceObject.identifier.toUpperCase();

    var device = _.findIndex(this.devices, {
        identifier: deviceObject.identifier
    });

    if (~device) {
        this.devices.splice(device, 1);
    }
    this.save(cb);
});

/**
 * Creates or updates profile. Convenience method for logging in.
 * @param updateObject
 * @param callback
 */
UserSchema.static('createOrUpdate', function(updateObject, userCallback) {

    winston.info('Create or update user %s', updateObject.id);

    //Generate query
    var queryObj = _.pick(updateObject, 'facebook', 'google', 'phone', '_id');

    if (updateObject.email) queryObj['email'] = updateObject.email;
    if (updateObject.device) queryObj['devices.identifier'] = updateObject.device.identifier;

    var queries = [];

    var queryOrder = ['email', 'phone', 'facebook', 'google', '_id', 'devices.identifier']; //Order in which queries will be run

    queryOrder.forEach(function(property) {

        if (queryObj.hasOwnProperty(property)) {
            queries.push(_.pick(queryObj, property));
        }
    });

    winston.info('Generated queries', queries);

    //Iterate over queries till user is found
    Async.eachSeries(queries, function(query, callback) {

        winston.info('Looking for user with query', query);

        User.findOne(query, function(err, user) {

            if (err) return callback({
                error: err
            });

            if (user) {
                winston.info('Found user %s', user.id);
                return callback({
                    user: user
                });
            }

            callback();
        });

    }, function(result) {

        var foundUser;
        var id = result && result.user && result.user.id ? result.user.id : "";
        winston.info('result user %s', id);
        if (result) {
            if (result.error) return userCallback(result.error);

            foundUser = result.user;
        }

        if (foundUser) {
            var userData = _.pick(updateObject, 'name', 'first_name', 'last_name', 'birthday', 'gender', 'veg_scale', 'facebook', 'google', 'phone', 'profile_image', 'phone_verified');

            _.extend(foundUser, userData);

            if (updateObject.email) {
                //Update email
                var shouldReplaceEmail = true;

                if (foundUser.email) {
                    if (updateObject.email === foundUser.email) {
                        shouldReplaceEmail = false;

                        if (updateObject.email_verified === true) {
                            foundUser.email_verified = true;
                        }
                    }
                }

                if (shouldReplaceEmail) {
                    foundUser.email = updateObject.email;
                    foundUser.email_verified = updateObject.email_verified;

                    if (foundUser.email_verified === false) {
                        //TODO send email for verification
                    }

                    foundUser.markModified('email');
                }
            }

            //Update devices
            if (updateObject.device) {
                foundUser.addOrUpdateDevice(updateObject.device, function(err, savedUser) {

                    if (err) {
                        winston.error('Error updating user %s', foundUser.id);
                        return userCallback(err);
                    } else {
                        winston.info('Updated user %s', foundUser.id);
                        userCallback(null, foundUser);
                    }
                });
            } else {
                userCallback(null, foundUser);
            }
        } else {
            winston.info('Could not find user, creating new');
            foundUser = new User(updateObject);
            foundUser.save(function(err, savedUser) {

                if (err) {
                    winston.error('Error updating user %s', foundUser.id);
                    return userCallback(err);
                } else {
                    winston.info('Updated user %s', savedUser.id);
                    userCallback(null, savedUser);
                }
            });
        }


    });
});


UserSchema.statics.updateDetails = function(user, callback) {

    if (!user._id) {
        callback(new Error('ID is required to update details'));
    } else {
        this.findOneAndUpdate(user._id, user, function(err, user) {

            if (err) {
                return callback(err);
            } else {
                return callback(null, user);
            }
        });
    }
}

UserSchema.statics.loadUsers = function(payload, callback) {
    var options = {
            criteria: {
                "$or": []
            },
            select: {
                _id: true,
                profile_image: true,
                name: true,
                first_name: true,
                last_name: true,
                email: true,
                phone: true
            }
        },
        phones = [];

    if (payload.facebook && payload.facebook.length > 0) {
        var facebook = _.pluck(payload.facebook, "id");
        options.criteria.$or.push({
            facebook: {
                "$in": facebook
            }
        });
    }
    if (payload.google && payload.google.length > 0) {
        var google = _.pluck(payload.google, "id");
        options.criteria.$or.push({
            google: {
                "$in": google
            }
        });
    }
    if (payload.emails && payload.emails.length > 0) {
        emails = payload.emails.map(function(email) {
            return email;
        });
        emails = emails.filter(function(email) {
            if (email) return true;
            else return false;
        });
        if (emails && emails.length > 0)
            options.criteria.$or.push({
                email: {
                    "$in": emails
                }
            });
    }
    if (payload.phones && payload.phones.length > 0) {
        phones = payload.phones.map(function(_phone) {
            return _phone.replace(/[ \-\(\)]*/g, '');
        });
        phones = phones.filter(function(_phone) {
            if (_phone) return true;
            else return false;
        });
        if (phones && phones.length > 0)
            options.criteria.$or.push({
                phone: {
                    "$in": phones
                }
            });
    }
    console.log(JSON.stringify(options.criteria));
    this.find(options.criteria).select(options.select).exec(function(err, users) {
        if (err) {
            winston.error('users query criteria is %j', options.criteria);
            winston.error('users not found', err);
            return callback(err, []);
        }
        return callback(null, users);
    });
};

var User = Mongoose.model('User', UserSchema);

module.exports = User;
