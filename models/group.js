'use strict';

/**
 * Module dependencies
 *
 * @type       {Function}
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var utils = require('../lib/utils');
var winston = require('../logger');

/**
 *  Group Schema
 */

var GroupSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: String,
    group_icon: String,
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    members: [Schema({
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        name: String,
        first_name: String,
        last_name: String,
        photo: String,
        birthday: Date,
        gender: {
            type: String,
            lowercase: true
        },
        inviteId: {
            type: Schema.Types.ObjectId,
            ref: 'Invitation'
        },
        phone: {
            type: String,
            set: function(p) {
                return p.replace(/[ \-\(\)\.]*/g, '');
            }
        },
        email: {
            type: String,
            lowercase: true
        },
        deepLink: String
    }, {
        _id: false
    })],
    createdAt: {
        type: Date,
        default: Date.now
    },
    deleted: {
        type: Boolean,
        default: false
    }

}, {
    collection: 'groups'
});

/**
 *  Group methods
 */
GroupSchema.methods = {
    addGroup: function(cb) {
        this.save(cb);
    },
    updateGroup: function(data, cb) {
        this.name = data.name || this.name;
        this.description = data.description || this.description;
        this.save(cb);
    },
    removeGroup: function(cb) {
        this.deleted = true;
        this.save(cb);
    },
    addMember: function(userId, memberId, cb) {
        var User = require('../models/user');
        var that = this;
        User.findOne({
            _id: memberId
        }, function(err, user) {
            if (err) {
                winston.error('Loading user error', memberId);
                return cb(err);
            }
            if (!user) return cb(new Error('no such user'));

            var index = utils.indexof(that.members, {
                userId: userId
            });
            if (index === -1) return cb(new Error('not authorized'));
            that.members.addToSet({
                userId: user._id,
                name: user.name,
                first_name: user.first_name,
                last_name: user.last_name,
                gender: user.gender,
                birthday: user.birthday,
                photo: user.profile_image
            });
            that.save(cb);
        })
    },
    removeMember: function(userId, memberId, cb) {
        var index = utils.indexof(this.members, {
            userId: userId
        });

        if (index === -1) return cb(new Error('not authorized'));

        index = utils.indexof(this.members, {
            userId: memberId
        });

        if (index === -1) { //Look in invite ids as well

            index = utils.indexof(this.members, {
                inviteId: memberId
            });
        }

        if (index === -1) return cb(new Error('no such user found in the group'));
        this.members.splice(index, 1);
        this.save(cb);
    }
};

/**
 *  Group pre-save hook
 */
GroupSchema.pre('save', function(next) {
    next();
});

/**
 *  Group pre-remove hook
 */
GroupSchema.pre('remove', function(next) {
    next();
});

/**
 *  Group static
 */
GroupSchema.statics = {
    load: function(groupId, cb) {
        var options = {
            _id: groupId
        };
        this.findOne(options).exec(function(err, group) {
            if (err) {
                winston.error('loadin group error', groupId);
                return cb(err, null);
            }
            return cb(null, group);
        });
    },
    loadMyGroupById: function(groupId, userId, cb) {
        var options = {
            _id: groupId,
            "members.userId": userId
        };
        this.findOne(options).exec(function(err, group) {
            if (err) {
                winston.error('loadin group error', groupId);
                return cb(err, null);
            }
            return cb(null, group);
        });
    },
    loadGroups: function(userId, cb) {
        var options = {
            "members.userId": userId,
            "deleted": false
        };
        this.find(options).exec(function(err, groups) {
            if (err) {
                winston.error('loading user groups error', options);
                return cb(err, null);
            }
            cb(null, groups);
        });
    }
};

var Group = mongoose.model('Group', GroupSchema);

module.exports = Group;
