/*
    TIMESHEET
    Contains models and logic associated with time sheets (Hours).
 */

function TimeSlot () {
    this.closed = false;
    this.hour24 = false;
    this.fromTime = nil;
    this.toTime = nil;

}

TimeSlot.prototype.overlap = function (timeSlot) {
    //..
    return false;
}
TimeSheet.prototype.equals = function (timeSlot) {
    //..
    return false;
}

function TimeSheet (string) {
    this.mon = [];
    this.tue = [];
    this.wed = [];
    this.thu = [];
    this.fri = [];
    this.sat = [];

}



var Mongoose = require('mongoose');

var timeSlotSchema = new Mongoose.Schema({
    closed : Boolean,
    hour_24 : Boolean,
    from_time : String,
    to_time : String
});

timeSlotSchema.pre('save', function (next) {
    if (this.closed) {

    } else if (this.hour_24) {

    } else {

    }
    next();
});

var timeSheetSchema = new Mongoose.Schema({
    sunday : [timeSlotSchema],
    monday : [timeSlotSchema],
    tuesday : [timeSlotSchema],
    wednesday : [timeSlotSchema],
    thursday : [timeSlotSchema],
    friday : [timeSlotSchema],
    saturday : [timeSlotSchema]
})

timeSheetSchema.pre ('save', function (next) {
    next();
});

module.exports = timeSheetSchema;