(function() {
    'use strict';
    /**
     * Module dependencies
     *
     * @type       {Function}
     */
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    var moment = require('moment');
    var winston = require('../logger');


    /**
     * Notification Schema
     *
     * @type       {Schema}
     */
    var NotificationSchema = new Schema({
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        initiator: {
            from: {
                type: String
            },
            type: {
                type: String
            }
        },
        event: String,
        icon: String,
        to: {
            type: String,
            enum: ['inapp', 'inapp-push'],
            default: 'inapp'
        },
        title: {
            type: String
        },
        body: {
            type: String,
            required: true
        },
        sms_body: {
            type: String
        },
        push_body: {
            type: String
        },
        path: {
            type: String,
            required: true
        },
        status: {
            type: String,
            enum: ['unviewed', 'viewed', 'read'],
            default: 'unviewed'
        },
        type: {
            type: String,
            enum: ['info', 'action'],
            default: 'info'
        },
        sender: {
            first_name: {
                type: String
            },
            name: {
                type: String
            },
            icon: {
                type: String
            },
            gender:{
                type:String
            }
        },
        created: {
            type: Date,
            default: Date.now
        },
        modified: {
            type: Date
        }
    });

    /**
     * Notification Methods
     */
    NotificationSchema.methods = {};

    /**
     * Notification Pre-save hook
     */



    /**
     * Notification queries
     */

    NotificationSchema.statics = {
        /**
         * Get the notification using notification id
         *
         * @method     load
         * @param      {<type>}    id      { description }
         * @param      {Function}  cb      { description }
         */
        load: function(condition, cb) {
            condition.type = 'info';
            winston.info(condition);
            var query = this.findOne(condition);
            query.exec(function(err, notification) {
                if (err) {
                    winston.error('loading notitification error', condition);
                    return cb(err, null);
                }
                return cb(null, notification);
            });
        },
        /**
         * Get top notifications  by time series
         *
         * @method     loadAll
         * @param      {number}    options  { description }
         * @param      {Function}  cb       { description }
         */
        loadAll: function(options, cb) {

            options.criteria.type = "info";
            if (!options.select) {
                options.select = "title body path status type icon created";
            }
            if (!options.pages) {
                options.pages = {};
                options.pages.skip = 0;
                options.pages.limit = 10;
            } else {
                options.pages.skip = options.pages.skip || 0;
                options.pages.limit = options.pages.limit || 10;
            }
            options.pages.sort = {};
            options.pages.sort.created = -1;
            if (options.pages.skip === 0) {
                var query = this.find(options.criteria, options.select, options.pages);
                query.exec(function(err, notifications) {
                    if (err) {
                        winston.error('loading all notifications error', options);
                        return cb(err);
                    }
                    return cb(null, notifications);
                });
            } else {
                var that = this;
                var criteria = options.criteria;
                criteria.status = "viewed";
                var query = this.findOne(options.criteria, null, {
                    sort: {
                        modified: -1
                    }
                });
                query.exec(function(err, doc) {
                    if (err) {
                        winston.error('loading all notifications error', options);
                        return cb(err);
                    }
                    if (doc) {
                        var modified = moment(doc.modified).toISOString();
                        options.criteria.modified = {
                            $lt: modified
                        };
                    }
                    var query = that.find(options.criteria, options.select, options.pages);
                    query.exec(function(err, notifications) {
                        if (err) {
                            winston.error('loading all notifications error', options);
                            return cb(err);
                        }
                        return cb(null, notifications);
                    });
                });
            }
        },
        /**
         * Get top notifications  by time series
         *
         * @method     loadCount
         * @param      {number}    options  { description }
         * @param      {Function}  cb       { description }
         */
        loadCount: function(options, cb) {
            var that = this;
            options.criteria.status = "viewed";
            options.criteria.type = "info";
            options.pages = {
                sort: {
                    modified: -1
                }
            };
            var query = this.findOne(options.criteria, null, options.pages);
            query.exec(function(err, doc) {
                if (err) {
                    winston.error(err);
                    return cb(err)
                }
                options.criteria.status = "unviewed";
                if (doc) {
                    var modified = moment(doc.modified).toISOString();
                    options.criteria.created = {
                        $gt: modified
                    };
                }

                var query = that.count(options.criteria);
                query.exec(function(err, notifications) {
                    if (err) {
                        winston.error('loading notification counts error', options);
                        return cb(err);
                    }
                    return cb(null, notifications);
                });
            });
        },
        /**
         * Mark the notifications as unread
         *
         * @method     markAsUnread
         * @param      {<type>}    options  { description }
         * @param      {Function}  cb       { description }
         */
        markAsViewed: function(options, cb) {
            options.criteria.type = 'info';
            options.criteria.status = 'unviewed';
            this.update(options.criteria, {
                $set: {
                    status: 'viewed',
                    modified: new Date()
                }
            }, {
                multi: true
            }, cb);
        },
        /**
         * Mark the notification as read
         *
         * @method     markAsRead
         * @param      {<type>}    options      { description }
         * @param      {Function}  cb      { description }
         */
        markAsRead: function(options, cb) {
            this.update(options.criteria, {
                $set: {
                    status: 'read',
                    modified: new Date()
                }
            }, cb);
        }
    };

    var Notification = mongoose.model('Notification', NotificationSchema);
    module.exports = Notification;
}());