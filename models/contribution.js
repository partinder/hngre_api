(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    var Types = Schema.Types;

    var ContributionSchema = new Schema({
        merchant_name: String,
        merchant_id: {
            type: Types.ObjectId,
            ref: 'merchants'
        },
        dish_name: String,
        location: String,
        veg_type: String,
        veg_scale: [],
        dish_id: {
            type: Types.ObjectId
        },
        dish_url: String,
        tags: {
            type: [String]
        },
        comment: {
            type: String
        },
        address: String,
        userId: {
            type: Types.ObjectId,
            ref: 'user'
        },
        status: {
            type: String,
            enum: ['new', 'processing', 'done'],
            default: 'new'
        },
        created: {
            type: Date,
            default: Date.now
        }
    }, {
        collection: 'contributions'
    });

    var Contribution = mongoose.model('Contribution', ContributionSchema);

    module.exports = Contribution;

}())
