/*
    INTERACTION
    Model for user-dish interaction data
 */

var Mongoose = require ('mongoose');

var EventType = {
    APPOPENED           : 'AppOpen',
    APPCLOSE            : 'AppClose',
    ADDEDTOWISHLIST     : 'WL+',
    REMOVEDFROMWISHLIST : 'WL-',
    RATED               : 'R+',
    REMOVEDFROMHISTORY  : 'R-',
    CLEARWISHLIST       : 'WL0',
    CLEARHISTORY        : 'R0',
    MERCHANTCALL        : 'Call',
    GOTDIRECTIONS       : 'Map',
    MERCHANTACTION      : 'MerchantAction'
};

var eventList = _.values(EventType);

var InteractionSchema = new Mongoose.Schema({

    _user: {type: Mongoose.Schema.Types.ObjectId, ref: 'User', index: true, sparse: true},
    device: {type: String, index: true, sparse: true},
    timestamp: {type: Date, default: Date.now},
    event: {type: String, required: true},
    data: {}
});

InteractionSchema.static('event', function (event, user, device, data, callback) {

    if (!event) return callback (new Error('Event is required'));

    //if (eventList.indexOf(event) < 0) throw new Error('Unrecognized event sent for interaction');

    var interaction = new this({
        _user: user,
        device: device,
        event: event,
        data: data
    });

    interaction.save(function (err) {

        if (err)
        {
            console.log('Error saving interaction:', err);
        }

        if (callback)
        {
            if (err) return callback(err);

            callback(null, {success: true});
        }
    })
});

var Interaction = Mongoose.model('Interaction', InteractionSchema);

Interaction.type = EventType;

module.exports = Interaction;