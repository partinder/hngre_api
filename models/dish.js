/*
	DISH
	Model and methods associated with the dish.
 */

var Mongoose = require ('mongoose');

var DishSchema = new Mongoose.Schema ({
	//name : String,
	//cuisine : String,
	//sub_cuisine : String,
	//ingredients : [String],
	//course : [String],
	//index : {
	//	fat : Number,
	//	chilly : Number,
	//	texture : Number,
	//	cookedness : Number,
	//	sweet : Number,
	//	sour : Number,
	//	umami : Number
	//},
	//tags : [String],
	//influencers : [String],
	_merchant : {type: Mongoose.Schema.Types.ObjectId, ref: 'Merchant'}
	//photo : String,
	//description : String,
	//veg_type : Number,
	//approve_status : Boolean
});

var OldDishSchema = new Mongoose.Schema ({
	dish_name : String,
	dish_cuisine : String,
	dish_sub_cuisine : String,
	dish_primary_ingredient : String,
	dish_secondary_ingredient : String,
	dish_course : String,
	dish_fat : String,
	dish_chilly : String,
	dish_texture : String,
	dish_cookedness : String,
	dish_sweet : String,
	dish_sour : String,
	dish_umami : String,
	dish_tags : String,
	dish_influencers : String,
	dish_merchant_id : {type: String, ref: 'Merchants'},
	dish_photo : String,
	dish_type : String,
	dish_approve_status : String
}, {collection: 'dishes'});

DishSchema.methods.toJSON = function () {

	var obj = this.toObject();

	delete obj.index;
	delete obj.tags;
	delete obj.influencers;
	delete obj.approve_status;
	delete obj.__v;

	return obj;
}

var Dish = Mongoose.model('Dish', DishSchema);

module.exports = Dish;