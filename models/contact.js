(function() {
    'use strict';
    /**
     * Module dependencies
     *
     * @type       {Function}
     */
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    var winston = require('../logger');


    /**
     * Notification Schema
     *
     * @type       {Schema}
     */
    var ContactSchema = new Schema({
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            unique: true,
            index: true
        },
        contacts: [{}],
        facebooks: [{}],
        googles: [{}],
        created: {
            type: Date,
            default: Date.now
        },
        modified: {
            type: Date
        }
    });

    var Contact = mongoose.model('Contact', ContactSchema);
    module.exports = Contact;
}());