/*
	RATING
	Model and methods associated with ratings.
 */

var Mongoose = require('mongoose');
var winston = require('../logger');

var RatingSchema = new Mongoose.Schema
({
	_dish: {type: Mongoose.Schema.Types.ObjectId, ref: 'Dish', required: true, index: true},
	_user: {type: Mongoose.Schema.Types.ObjectId, ref: 'User', required: true, index: true},
	_merchant: {type: Mongoose.Schema.Types.ObjectId, ref: 'Merchant', required: true, index: true},
	rating: {type: Number, min: 1, max: 5},
	not_for_me: {type: Boolean, default: false},
	eat_later: {type: Boolean},
	timestamp: {type: Date, default: Date.now}
});

RatingSchema.index ({_dish: 1, _user: 1, _merchant: 1}, {unique: true});
	//The index above makes sure that only one rating exists per dish for a user.

RatingSchema.statics.findForDish = function (dishId, callback) {

	var query = this.find({dish: dishId}).populate('user', 'name');

	if (callback)
	{
		query.exec(function(err, ratings) {
			if (err)
			{
				return callback (err);
			}
			else
			{
				callback(null, ratings);
			}
		});
	}
	else
	{
		return query;
	}
}

RatingSchema.statics.findForUser = function (userId, callback)
{
	var query = this.find ({user: userId});
	query.populate ('dish', 'name');

	if (callback)
	{
		query.exec (function(err, ratings) {
			if (err)
			{
				return callback (err);
			}
			else
			{
				callback(null, ratings);
			}
		});

	}
	else
	{
		return query;
	}
};

RatingSchema.statics.saveRating =  function (userId, dishId, merchantId, setRating, unsetRating, callback)
{
	setRating.timestamp = Date.now();

	var updateObject = {};

	if (_.keys(setRating).length > 0) updateObject.$set = setRating;
	if (_.keys(unsetRating).length > 0) updateObject.$unset = unsetRating;

	var query = this.findOneAndUpdate({_dish:dishId, _user:userId, _merchant: merchantId}, updateObject, {upsert: true});

	query.exec(function (err, rating) {

		console.log('Rating query response', err, rating);

		if (err) return callback(err);

		callback (null, rating);
	});
};

RatingSchema.statics.rateDish = function (userId, dishId, merchantId, rating, callback)
{
    this.saveRating(userId, dishId, merchantId, {rating:rating}, {}, callback);
};

RatingSchema.statics.removeRating = function (userId, dishId, merchantId, callback)
{
	this.saveRating(userId, dishId, merchantId, {}, {rating: 1}, callback);
};

RatingSchema.statics.notForMe = function (userId, dishId, merchantId, notForMe, callback)
{
    this.saveRating(userId, dishId, merchantId, {not_for_me: notForMe}, {eat_later: 1}, callback);
};

RatingSchema.statics.eatLater = function (userId, dishId, merchantId, eatLater, callback)
{
    this.saveRating(userId, dishId, merchantId, {eat_later: eatLater}, {not_for_me: 1}, callback);
};

RatingSchema.statics.ratingsForUser = function (userId, callback)
{
	var queryObject = {_user: userId, rating: {$ne: null}};

	if (callback)
	{
		this.find(queryObject, {_id: 0, _user: 0}, function (err, ratings) {
			winston.info('Ratings for', { userId:userId, ratings: ratings});
			if (err) return callback (err);
			else callback(null, ratings);
		});
	}
	else
	{
		return queryObject;
	}
};

RatingSchema.statics.wishListForUser = function (userId, callback) {

	var queryObject = {_user: userId, eat_later: true};

	if (callback)
	{
		this.find(queryObject, {_id:0, _user: 0, eat_later: 0}, function (err, wishlist) {
			winston.info('Wish list for ', {userId:userId, wishlist:wishlist });
			if (err) return callback (err);
			else callback(null, wishlist);
		});
	}
	else
	{
		return queryObject;
	}
};

RatingSchema.statics.deleteForUser = function (userId, callback)
{
	this.remove({_user: userId}, function (err, result) {
		if (err) return callback(err);
		else callback (null, result);
	});
};

//RatingSchema.fetchUnrated = function (params, callback) {
//	//List restaurants near the user which have not yet been rated by him.
//
//}

var Rating = Mongoose.model('Rating', RatingSchema);

module.exports = Rating;
