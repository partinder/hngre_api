/*
    RECOMMENDATION
    Models and methods associated with recommendations.
 */

var Mongoose = require('mongoose');

var RecommendedDishesSchema = new Mongoose.Schema({
    _dish: {
        type: Mongoose.Schema.Types.ObjectId
    }, //ID of dish
    _merchant: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Merchant'
    },
    predicted_rating: Number //Recommendation score
}, {
    _id: false
});

var RecommendationType = {
    RECOMMENDATION: 'recommendation',
    SEARCH: 'search',
    WISHLIST: 'wish list',
    RESTAURANT: 'restaurant',
    DISH: 'dish',
    CONTRIBUTOR: 'contributor'
};

var RecommendationSchema = new Mongoose.Schema({
    _user: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'User',
        index: true
    }, //User ID for whom recommendation is generated
    group_id: {
        type: String
    },
    type: {
        type: String,
        required: true,
        enum: _.values(RecommendationType)
    }, //Type of recommendations
    rec_dishes: [{}],
    /*{
            _dish: {
                type: Mongoose.Schema.Types.ObjectId
            },
            _merchant: {
                type: Mongoose.Schema.Types.ObjectId,
                ref: 'Merchant'
            },
            predicted_rating: Number
        }*/
    rec_merchants: [{}],
    /*{
            _merchant: {
                type: String
            },
            _dishes: [{
                _dish: {
                    type: String
                },
                predicted_ratings: {}
            }]
        }*/ //ID of previous recommendation in this series
    params: {}, //Parameters on basis of which dishes are selected
    time_sent: [{
        type: Date,
        default: Date.now
    }], //Time at which reply sent to user
    time_taken: Number //Time taken from request to reply
});

var Recommendation = Mongoose.model('Recommendation', RecommendationSchema);

Recommendation.type = RecommendationType;

module.exports = Recommendation;
