/*
    Response
 */

var Mongoose = require('mongoose');

var UnauthorizedSchema = new Mongoose.Schema({
    response: {},
    url: {},
    payload: {},
    params: {},
    headers:{},
    ip_address:String,
    created: Date
});

var Unauthorized = Mongoose.model('Unauthorized', UnauthorizedSchema);

module.exports = Unauthorized;
