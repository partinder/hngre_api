/*
	MERCHANT
	Model and methods associated with merchants.
 */

var Mongoose = require ('mongoose');
var timeSheetSchema = require('./timesheet');

var MerchantSchema = new Mongoose.Schema({
	//name : {type: String, index: true},
	//yelp : {
	//	link: String,
	//	rating: Number,
	//	review_count : Number
	//},
	//cuisine : {type: [String], index: true},
	//contact : {
	//	primary_email: String,
	//	email: [String],
	//	phone: [String]
	//},
	//coordinates:{ index: "2dsphere", type: {}},
	//contact_person:[{
	//	name: String,
	//	email: String,
	//	phone: String,
	//	designation: String
	//}],
	//url: String,
	//address:{
	//	street_address: {type: String, index: true},
	//	locality: {type: String, index: true},
	//	region: {type: String, index: true},
	//	zip: {type: String, index: true},
	//	landmark: String,
	//	neighbourhood : {type: [String], index: true}
	//},
	//info: {
	//	hours: {},
	//	parking: [String],
	//	credit_card: Boolean,
	//	price_range: String,
	//	attire: String,
	//	groups: Boolean,
	//	kids: Boolean,
	//	reservations: Boolean,
	//	delivery: {type: Boolean, index: true},
	//	takeout: {type: Boolean, index: true},
	//	table_service: Boolean,
	//	outdoor_seating: Boolean,
	//	wifi: Boolean,
	//	meal: [String],
	//	music: String,
	//	best_nights: [String],
	//	alcohol: String,
	//	alcohol_byo: Boolean,
	//	smoking: Boolean,
	//	coat: Boolean,
	//	noise: String,
	//	dancing: String,
	//	ambience: [String],
	//	tv: Boolean,
	//	caters: Boolean,
	//	merchant_type: [String],
	//	wheelchair: Boolean,
	//	happyhours: {}
	//},
	//zagat: {
	//	food: Number,
	//	decor: Number,
	//	service: Number
	//},
	//social : {
	//	facebook: String,
	//	twitter: String,
	//	instagram: String
	//},
	//transit: [String],
	//tags: {type:[String], index:true},
	//influencers: [String],
    //
	//status: { type: String, default : "pool"},
	//photo: String,
	//source: String,
	//hngre_url: String
});

var OldMerchantSchema = new Mongoose.Schema ({
	merchant_name : String,
	merchant_yelp_rating : String,
	merchant_yelp_review_count : String,
	merchant_cuisine1 : String,
	merchant_cuisine2 : String,
	merchant_cuisine3 : String,
	merchant_tel : String,
	merchant_email : String,
	merchant_old_email : String,
	merchant_weburl : String,
	merchant_street_address : String,
	merchant_locality : String,
	merchant_region : String,
	merchant_zip : String,
	merchant_street_address_landmark : String,
	merchant_neighborhood1 : String,
	merchant_neighborhood2 : String,
	merchant_neighborhood3 : String,
	merchant_transit1 : String,
	merchant_transit2 : String,
	merchant_transit3 : String,
	merchant_hours : String,
	merchant_parking : String,
	merchant_credit_card_accept_status : String,
	merchant_price_range : String,
	merchant_attire : String,
	merchant_good_for_groups : String,
	merchant_good_for_kids : String,
	merchant_takes_reservations : String,
	merchant_delivery : String,
	merchant_takeout : String,
	merchant_table_service : String,
	merchant_outdoor_seating : String,
	merchant_wifi : String,
	merchant_good_for_meal : String,
	merchant_music : String,
	merchant_best_nights : String,
	merchant_happy_hours : String,
	merchant_alcohol : String,
	merchant_alcohol_byo : String,
	merchant_smoking : String,
	merchant_coat_check : String,
	merchant_noise_level : String,
	merchant_dancing : String,
	merchant_ambience : String,
	merchant_tv : String,
	merchant_caters : String,
	merchant_wheelchair : String,
	merchant_tags : String,
	merchant_influencers : String,
	merchant_number_of_dishes : String,
	merchant_type : String,
	merchant_dishes : String,
	merchant_zagat_rating_food : String,
	merchant_zagat_rating_decor : String,
	merchant_zagat_rating_service : String,
	merchant_facebook : String,
	merchant_twitter : String,
	merchant_longtitude : String,
	merchant_latitude : String,
	merchant_contact_person_name : String,
	merchant_contact_person_phone : String,
	merchant_contact_person_email : String,
	merchant_contact_person_designation : String,
	merchant_source : String,
	merchant_yelp_link : String,
	merchant_status : String,
	merchant_unique_url : String,
	merchant_hashed_unique_id : String,
	merchant_photo : String
}, {collection: 'merchants'});



MerchantSchema.methods.toJSON = function () {

	var obj = this.toObject();

	delete obj.yelp;
	delete obj.contact;
	delete obj.contact_person;
	delete obj.zagat;
	delete obj.tags;
	delete obj.influencers;
	delete obj.info;
	delete obj.source;
	delete obj.hngre_url;
	delete obj.status;
	delete obj.__v;

	return obj;
}

function extractTimeSheet (timeString) {
	
}

var Merchant = Mongoose.model('Merchant', MerchantSchema);


module.exports = Merchant;