'use strict';

/**
 * Module dependencies
 *
 * @type       {Function}
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var utils = require('../lib/utils');
var winston = require('../logger');

/**
 *  Group Schema
 */

var PredictionMerchantSchema = new Schema({
    userId: {
        type: String,
        required: true,
        trim: true
    },
    merchantId: {
        type: String,
        required: true,
        trim: true
    },
    available_veg_type: [Number],
    dishes: [Schema({
        dishId: {
            type: String,
            required: true,
            trim: true
        },
        score: {
            type: Number
        },
        veg_scale: {
            type: Number
        }
    })],
    location: {
        type: {
            type: String
        },
        coordinates: [Number]
    },
    address: {
        landmark: {
            type: String
        },
        zip: {
            type: String
        },
        region: {
            type: String
        },
        locality: {
            type: String
        },
        street_address: {
            type: String
        },
        neighbourhood: [String],
        country: {
            type: String
        }
    }
}, {
    collection: 'prediction_merchants'
});

/**
 *  Group static
 */
PredictionMerchantSchema.statics = {};

var PredictionMerchant = mongoose.model('PredictionMerchant', PredictionMerchantSchema);

module.exports = PredictionMerchant;