/*
    DEVICE
 */

var Mongoose = require ('mongoose');
var winston = require('../logger');

var DeviceSchema = new Mongoose.Schema({

    identifier: {type: String, unique: true, sparse: true, uppercase:true },
    agent: {},
    apn_notification: {token: String, active: Boolean },
    gcm_notification: {token: String, active: Boolean },
    last_seen: Date,
    //user: {type: Mongoose.Schema.Types.ObjectId, unique: true, index: true},
    active: { type: Boolean, default : true }
}, {_id: false});

//var Device = Mongoose.model('Device', DeviceSchema);

module.exports = DeviceSchema;