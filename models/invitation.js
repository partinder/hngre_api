'use strict';

/**
 *  Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

function getPhone(phone) {
    /*    phone = phone.split('');
    var phone1 = phone.splice(phone.length-4,4);
    var phone2 = phone.splice(phone.length-4,3);
    return phone.join('') + " " + phone2.join('') + " "+ phone1.join('');*/
    return phone;
}

function setPhone(phone) {
    phone = phone.replace(/[ \-\(\)]*/g, '');
    return phone;
}

var InvitationSchema = new Schema({
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    invitee: {
        name: {
            type: String,
            trim: true
        },
        first_name: {
            type: String
        },
        last_name: {
            type: String
        },
        email: {
            type: String,
            trim: true
        },
        phone: {
            type: String,
            trim: true,
            get: getPhone,
            set: setPhone
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 *  Methods
 */

var Invitation = mongoose.model('Invitation', InvitationSchema);

module.exports = Invitation;