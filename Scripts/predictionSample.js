//For creating a sample dataset in predictionIO

var predictionio = require('predictionio-driver');

var client = new predictionio.Events(1);

client.status().
	then (function (status) {
		console.log(status);
	});


for (var i = 0; i < 10; i++) {
	console.log('Adding user ', i);
	client.createUser({uid: i}, function (err, result) {
		if (err) {
			console.error(err);
		} else {
			console.log(result);
		}
	});
}

for (var i = 0; i < 50; i++) {
	console.log('Adding item ', i);
	client.createItem({iid: i, properties: {pio_itypes: ['type1']}}, function (err, result) {
		if (err) {
			console.error(err);
		} else {
			console.log(result);
		}
	});
}

for (var i = 0; i < 10; i++) {
	for (var j = 0; j < 10; j++) {
		var num = Math.floor(Math.random() * 50);
		console.log('Creating action on user and item: ', i, num );
		client.createAction({
    		event: 'view',
    		uid: i,
    		iid: num,
    		eventTime: new Date().toISOString()
		}).
		then(function(result) {
            console.log(result); // Prints "{eventId: 'something'}"
    	}).
    	catch(function(err) {
        	console.error(err); // Something went wrong
    	});
	}
}

