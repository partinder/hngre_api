

"use strict";

var MongoClient = require('mongodb').MongoClient;


MongoClient.connect('mongodb://52.5.0.16/db_hngre', function (err, db) {

    if (err)
    {

        console.log('ERROR Connecting to old database: ', err);

    }
    else
    {
        var itemCount = 0;
        var modifiedCount = 0;

        var users = db.collection('users');

        var userStream = users.find().stream();

        userStream.on('error', function (err) {
            console.log('ERROR creating user stream:', err);
        });

        userStream.on('end', function() {
            console.log('Reading ended');
        });

        userStream.on('data', function (item) {

            itemCount++;

            if (item.email && item.email.hasOwnProperty('address'))
            {
                var emailObj = item.email;

                item.email = emailObj.address;
                item.email_verified = emailObj.verified;
            }

            users.update({_id: item._id}, item, function (err, status){

                if (err) console.log('Error updating user.',err, item);

                modifiedCount ++;

                if (modifiedCount === itemCount)
                {
                    console.log('Completed update, count is', itemCount);
                    process.exit(1);
                }

            })

        });
    }
})
