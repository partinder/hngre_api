/*
    MERCHANT-LOCATION
    Script for transforming the location data stored by the Python application for Hngre (in separate fields) into GeoJSON format to enable location-based queries.
*/

var MongoClient = require('mongodb').MongoClient;

saveExternal();


function transformInternal (callback) { //Saves location data as sub-document of existing database.

    MongoClient.connect('mongodb://localhost:27017/my_database', function (err, db) {

        if (err) return callback(err);

        var merchantCollection = db.collection('merchants');

        var bulk = merchantCollection.initializeOrderedBulkOp();

        var counter = 0;

        merchantCollection.find().forEach(function (doc) {

            bulk.find({"_id": doc._id}).updateOne({
                "$set": {
                    "location": geoJsonForMerchant(doc)
                }//,
                //"$unset": {  "longitude": 1, "latitude": 1 }
            });

            counter++;
            if (counter % 1000 == 0) {
                bulk.execute();
                bulk = merchantCollection.initializeOrderedBulkOp();
            }

        });

        bulk.execute(function (err, results) {
            if (err) return callback(err);

            callback(null, results);
        });

    });
}

function saveExternal (callback) { //Saves location data in separate database

    MongoClient.connect('mongodb://localhost:27017/mydb', function (err, db) {

        if (err) throw err;

        var collectionName = 'merchant_locations';

        db.createCollection(collectionName, function (err, result) {

            if (err) throw err;

            var locationCollection = db.collection(collectionName);
            var merchantCollection = db.collection('merchants');

            var bulk = locationCollection.initializeUnorderedBulkOp();

            merchantCollection.find().toArray(function (err, array) {

                if (err) throw err;

                console.log('Number of documents:', array.length);

                array.forEach(function (merchant) {

                    console.log('id', merchant._id, 'name', merchant.merchant_name, 'long', merchant.merchant_longtitude, 'lat', merchant.merchant_latitude);

                    bulk.find({_id: merchant._id}).upsert().replaceOne(
                        {
                            "_id": merchant._id,
                            "location": geoJsonForMerchant(merchant)
                        }
                    );
                });

                bulk.execute(function (err, results) {

                    if (err) throw err;
                    console.log (JSON.stringify(results));

                    locationCollection.ensureIndex({location:'2dsphere'}, function (err, results) {
                        console.log('Index result', err, results);

                        //Test query
                        locationCollection.geoNear(-73, 40, {spherical: true}, function (err, results) {
                            console.log('Test query result', err, JSON.stringify(results, null, 4));
                            db.close();
                        });
                    });
                });
            });
        });
    });
}

function geoJsonForMerchant (merchant) {
    var geoJson = {
        "type": "Point",
        "coordinates": [parseFloat(merchant.merchant_longtitude), parseFloat(merchant.merchant_latitude)]
    }
    return geoJson;
}