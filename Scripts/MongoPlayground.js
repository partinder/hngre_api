/*
    MONGO PLAYGROUND
    For testing queries
 */

var mongodb = require('mongodb');

var Mongoose = require('mongoose');

var User = require('../models/user');
var Rating = require('../models/rating');
var Merchant = require('../models/merchant');
var Dish = require('../models/dish');

//var timeString = 'Mon 11:00 am - 3:30 pm, 5:00 pm - 11:30 pm, Tue 11:00 am - 3:30 pm, 5:00 pm - 11:30 pm, Wed 11:00 am - 3:30 pm, 5:00 pm - 11:30 pm, Thu 11:00 am - 3:30 pm, 5:00 pm - 11:30 pm, Fri 11:00 am - 3:30 pm, 5:00 pm - 12:30 am, Sat 11:00 am - 3:30 pm, 5:00 pm - 12:30 am, Sun 11:00 am - 10:30 pm,';

//Generate sample of data for dishes


