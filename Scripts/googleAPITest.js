/*
    Google-API-Test
    Sample of using the node module googleapis to access get user data.
*/

var google  = require('googleapis');

var OAuth2 = google.auth.OAuth2;

var clientID = '161504962992-bosclgismo1h24vn2f58u1e2hm2hkot9.apps.googleusercontent.com';
var clientSecret = 'SnmYxuYGR--XDEPvVzXQF0gp';

var oauth2Client = new OAuth2(clientID, clientSecret);

var authCode = '4/pO2HNpp3tOh9g5MlJZgRsl9B69p4P_JhXTkkhVDh8VU.Ajr3AsyvmvUfcp7tdiljKKY5SgjjlQI';

oauth2Client.getToken(authCode, function (err, tokens) {
    console.log(JSON.stringify(err), tokens);
    if (err) {
        console.error('ERROR');
    } else {
        console.log('tokens', tokens);
        oauth2Client.setCredentials(tokens);
        var plus = google.plus('v1');

        plus.people.get({userId: 'me', auth: oauth2Client}, function (err, response) {
            //Handle response, send callback
            console.log(err, response);
        });
    }
});