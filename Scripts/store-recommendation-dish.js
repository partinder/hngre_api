(function() {
    'use strict';
    var mongo = require('mongojs');
    var ObjectId = mongo.ObjectId;
    var connection = "mongodb://localhost:27017/db_hngre";
    var baseURI = "http://54.84.67.184:9500"
    var request = require('request');
    var moment = require('moment');
    var jsonfile = require('jsonfile');
    var async = require('async');
    var _ = require('underscore');

    var dateData = moment().format('YYYY-MM-DD')
    var data = jsonfile.readFileSync('./storage.json');
    if (!data[dateData]) {
        data[dateData] = {
            "users": {
                "total": 0,
                "skip": 0,
                "failed": [],
                "merchants": {
                    "total": 0,
                    "skip": 0,
                    "failed": []
                }
            }
        };
        jsonfile.writeFileSync('./storage.json', data);
    }

    var db = mongo(connection, ['predictions', 'users', 'merchants']);

    if (!db) {
        throw new Error('could not connect to ' + connection);
    }

    function getRecommendations(options, callback) {
        request(options, function(err, response, body) {
            if (!err && response.statusCode == 200) {
                console.log('predictions are', body);
                callback(null, body);
            } else {
                callback(err, {});
            }
        });
    }

    function saveDishesScores(toSave, callback) {
        var total = toSave.length;

        function saveUserPredictions() {
            var p = toSave[total - 1];
            //console.log("To save doc ", p);
            db.predictions.findOne({
                userId: p.userId,
                merchantId: p.merchantId,
                dishId: p.dishId
            }, function(err, prediction) {
                if (err) {
                    console.log('failed to load prediction');
                    return callback(err);
                }
                if (!prediction) {
                    db.predictions.save(p, function(err) {
                        if (err) {
                            console.log('failed to load prediction');
                            return callback(err);
                        }
                        if (--total) {
                            saveUserPredictions();
                        } else {
                            return callback();
                        }
                    });
                } else {
                    db.predictions.findAndModify({
                        query: {
                            userId: p.userId,
                            merchantId: p.merchantId,
                            dishId: p.dishId
                        },
                        update: {
                            $set: {
                                score: p.score
                            }
                        },
                        new: true
                    }, function(err, doc, lastErrorObject) {
                        if (err) {
                            console.log('failed to update prediction');
                            return callback(err);
                        }
                        if (--total) {
                            saveUserPredictions();
                        } else {
                            return callback();
                        }
                    });
                }
            });
        }
        if (total > 0) {
            saveUserPredictions();
        }
    }

    function getUser(callback) {
        db.users.find({}).skip(data[dateData].users.skip).limit(1).toArray(function(err, user) {
            if (err) {
                console.log('failed to load user');
                data[dateData].users.failed.push(data[dateData].users.skip);
                data[dateData].users.skip = data[dateData].users.skip + 1;
                jsonfile.writeFileSync('./storage.json', data);
                return callback(err);
            }
            if (user) {
                user = user[0];
            }
            console.log("user id : ", user._id);
            callback(null, user);
        });
    }

    function getMerchant(callback) {
        db.merchants.find({
            status: "completed"
        }).skip(data[dateData].users.merchants.skip).limit(1).toArray(function(err, merchant) {
            if (err) {
                console.log('failed to load merchant');
                data[dateData].users.merchants.failed.push();
                data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                jsonfile.writeFileSync('./storage.json', data);
                return callback(err);
            }
            if (merchant) {
                merchant = merchant[0];
            }
            callback(null, merchant);
        });
    }

    function countUsers(callback) {
        db.users.count({}, function(err, total) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (total > 0) {
                data[dateData].users.total = total;
                jsonfile.writeFileSync('./storage.json', data);
            }
            return callback(null, total);
        })
    }


    function countMerchants(callback) {
        db.merchants.count({
            status: "completed"
        }, function(err, total) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (total > 0) {
                data[dateData].users.merchants.total = total;
                jsonfile.writeFileSync('./storage.json', data);
            }
            return callback(null, total);
        })
    }

    function prepareOptions(user, merchant) {
        var path = baseURI + '/recommend';
        var dishes = [];
        if (merchant.dishes && merchant.dishes.length && merchant.dishes.length > 0) {
            merchant.dishes.forEach(function(dish) {
                dishes.push({
                    merchantId: merchant._id,
                    dishId: dish._id
                });
            });
        }
        if (!dishes || !dishes.length || !(dishes.length > 0)) {
            return null;
        }
        var options = {
            url: path,
            method: 'POST',
            json: true,
            body: JSON.stringify({
                users: [{
                    userId: user._id,
                    age: moment().diff(user.birthday, 'years'),
                    gender: user.gender
                }],
                dishes: dishes
            })
        };
        return options;
    }

    function prepareDocument(predictions, merchant) {
        var toSave = [];
        for (var user in predictions) {
            if (predictions.hasOwnProperty(user)) {
                var merchants = predictions[user];
                for (var m in merchants) {
                    if (merchants.hasOwnProperty(m)) {
                        var dishes = merchants[m];
                        for (var dish in dishes) {
                            if (dishes.hasOwnProperty(dish)) {
                                var score = dishes[dish];
                                var data_ = {
                                    userId: user,
                                    merchantId: m,
                                    dishId: dish,
                                    score: score
                                };
                                var d = null;
                                for (var i = 0, len = merchant.dishes.length; i < len; i++) {
                                    if (merchant.dishes[i]._id.toString() == dish) {
                                        d = merchant.dishes[i];
                                        break;
                                    }
                                }
                                if (d) {
                                    data_.veg_type = d.veg_type
                                }
                                if (m == merchant._id) {
                                    data_.location = merchant.location,
                                    data_.address = merchant.address
                                    toSave.push(data_);
                                } else {
                                    toSave.push(data_);
                                }
                            }
                        }
                    }
                }
            }
        }
        return toSave;
    }

    function recordPrediction() {
        countUsers(function(err, total) {
            if (err && total < 1) {
                return console.log(err);
            }
            async.whilst(function() {
                    data = jsonfile.readFileSync('./storage.json');
                    if (!(data[dateData].users.total > 0 && data[dateData].users.skip < data[dateData].users.total)) {
                        console.log("All done");
                    } else {
                        return data[dateData].users.total > 0 && data[dateData].users.skip < data[dateData].users.total;
                    }
                }, function(callback) {
                    getUser(function(err, user) {
                        if (err || !user) {
                            return callback(err);
                        }
                        countMerchants(function(err, total) {
                            if (err) {
                                console.log(err);
                                return callback(err);
                            }
                            async.whilst(function() {
                                    data = jsonfile.readFileSync('./storage.json');
                                    if (!(data[dateData].users.merchants.total > 0 && data[dateData].users.merchants.skip < data[dateData].users.merchants.total)) {
                                        data[dateData].users.merchants.failed = [];
                                        data[dateData].users.merchants.skip = 0;
                                        data[dateData].users.skip = data[dateData].users.skip + 1;
                                        jsonfile.writeFileSync('./storage.json', data);
                                        callback();
                                    } else {
                                        return data[dateData].users.merchants.total > 0 && data[dateData].users.merchants.skip < data[dateData].users.merchants.total;
                                    }
                                },
                                function(callback) {
                                    getMerchant(function(err, merchant) {
                                        if (err || !merchant) {
                                            return callback(err);
                                        }
                                        var options = prepareOptions(user, merchant);
                                        if (!options) {
                                            data[dateData].users.merchants.failed.push(data[dateData].users.merchants.skip);
                                            data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                                            jsonfile.writeFileSync('./storage.json', data);
                                            return callback();
                                        }
                                        getRecommendations(options, function(err, predictions) {
                                            if (err || !predictions) {
                                                data[dateData].users.merchants.failed.push(data[dateData].users.merchants.skip);
                                                data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                                                jsonfile.writeFileSync('./storage.json', data);
                                                return callback();
                                            }
                                            var toSave = prepareDocument(predictions, merchant);
                                            if (!toSave || !toSave.length || !(toSave.length > 0)) {
                                                data[dateData].users.merchants.failed.push(data[dateData].users.merchants.skip);
                                                data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                                                jsonfile.writeFileSync('./storage.json', data);
                                                return callback();
                                            }
                                            saveDishesScores(toSave, function(err) {
                                                if (err) {
                                                    console.log(err);
                                                    data[dateData].users.merchants.failed.push(data[dateData].users.merchants.skip);
                                                    data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                                                    jsonfile.writeFileSync('./storage.json', data);
                                                    return callback();
                                                }
                                                data[dateData].users.merchants.skip = data[dateData].users.merchants.skip + 1;
                                                jsonfile.writeFileSync('./storage.json', data);
                                                return callback();
                                            });
                                        });
                                    })
                                },
                                function(err) {
                                    if (err) {
                                        console.log(err);
                                        callback();
                                    }
                                });
                        });
                    });
                },
                function(err) {
                    if (err) {
                        console.log(err);
                    }
                });
        });
    }

    function start() {
        recordPrediction();
    }
    start();
}())