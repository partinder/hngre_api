//Returns n recommendations for a user from predictionIO server, input from the console as arguments.

var predictionio = require('predictionio-driver');

var engine = new predictionio.Engine('http://localhost:8000'); // Engine url

var userid = process.argv[2];
var number = parseInt(process.argv[3]);

if (!userid) {
	throw new Error('User ID required as first parameter.')
}

if (isNaN(number)) 
	number = 5;

console.log('Getting %d predictions for user', number, userid)

var query = {
   	'user': userid,
    'num': number
};

var string = JSON.stringify(query);
console.log(string);

console.log(query);

engine.sendQuery(query).
    then(function (result) {
        console.log(result);
    });