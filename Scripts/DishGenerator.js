/*
    Dish Generator
 */

var Mongoose = require('mongoose');
var Merchant = require('../models/merchant');
var Dish = require('../models/dish');

var chance = require('chance');

Mongoose.connect('mongodb://localhost/db_hngre');

Mongoose.connection.on('connected', function () {
    Merchant = Mongoose.model('Merchant', Merchant.schema);
    Dish = Mongoose.model('Dish', Dish.schema);

    Merchant.distinct('_id', function (err, merchantIDs) {

        if (err) throw err;

        console.log('Merchants found: ', merchantIDs);

        merchantIDs.forEach(function (merchantId) {
            //Create 5 dishes per merchant

            var dishChance = new chance();

            for (var i = 0; i < 5; i++) {
                var newDish = new Dish ({
                    name : dishChance.string({length:15}),
                    veg_type : dishChance.integer({min:1,max:3}),
                    _merchant : merchantId
                });

                newDish.save(function (err) {
                    if (err) console.log ('ERROR Saving dish' + JSON.stringify(newDish) + err);
                    else {
                        console.log('Dish named ' + newDish.name + ' saved successfully for merchant ' + merchantId);
                    }
                });
            }
        });
    });
});