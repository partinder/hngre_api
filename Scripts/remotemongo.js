/*
    REMOTE-MONGO
    Script for copying data from remote MongoDB instance to local via an SSH tunnel.

    Needs refactorization: Use MongoDB driver instead of Mongoose.
 */

var mongoose = require('mongoose');
var Tunnel = require('tunnel-ssh');

var dishSchema = require ('./models/dish.js');
var merchantSchema = require ('./models/merchant.js');

var config = {
    remotePort: 27017, //localport
    localPort: 9322, //remoteport
    verbose: true, // dump information to stdout
    disabled: false, //set this to true to disable tunnel (useful to keep architecture for local connections)
    sshConfig: { //ssh2 configuration (https://github.com/mscdex/ssh2)
        host: 'ec2-54-191-14-157.us-west-2.compute.amazonaws.com',
        port: 22,
        username: 'ubuntu',
        privateKey: require('fs').readFileSync('/hngre.pem'),
        // passphrase: 'verySecretString' // option see ssh2 config 
    }
};

var tunnel = new Tunnel(config);
tunnel.connect(function (error) {
    if (error) {
    	console.log(error);
    	// tunnel.close();
    } else {
      
      console.log('Connected to remote mongo!!');
   		var remoteMongo = mongoose.createConnection('mongodb://localhost:9322/my_database');
      var localMongo = mongoose.createConnection('mongodb://localhost:27017/mydb');

      remoteMongo.on('error', function (err) {
        console.log('Remote mongo error', err);
      });

      localMongo.on('error', function (err) {
        console.log('Local mongo error', err);
      });

   		// var remoteMerchant = remoteMongo.model('Merchant', merchantSchema);
      // var localMerchant = localMongo.model('Merchant', merchantSchema);
   		var remoteDish = remoteMongo.model('Dish', dishSchema);
      var localDish = localMongo.model('Dish', dishSchema);

      //For streaming merchants
      // var stream = remoteMerchant.find({}).stream();

      // stream.on('data', function (merchant) {
      //   var objMerchant = merchant.toObject();
      //   console.log(objMerchant);
      //   var newLocalMerchant = new localMerchant (objMerchant);
      //   newLocalMerchant.save(function(err, merchant) {
      //     if (err) return console.log(err);
      //     console.log('Saved merchant: ', merchant._id);
      //   })
      // });

      // stream.on('end', function() {
      //   console.log('Stream ended. Exiting...');  
      //   tunnel.close();
      //   process.exit();
      // });

      //For streaming dishes.

      var stream = remoteDish.find({}).stream();

      var dishCount = 0;
      var savedCount = 0;

      stream.on('data', function (dish) {
        dishCount++;
        var objDish = dish.toObject();
        console.log(objDish);
        var newLocalDish = new localDish (objDish);

        newLocalDish.save(function(err, dish) {
          if (err) return console.log(err);
          savedCount++;
          console.log('Saved dish: ', dish._id);
        })
      });

      stream.on('end', function() {
        console.log('Stream ended. Exiting...');  
        tunnel.close();
        // process.exit();
      });
      
   		// Dish.find({}, function (err, dishes) {
   		// 	if (err) {
   		// 		console.log(err);
   		// 	} else {
   		// 		console.log ('Dishes - ', dishes);
   		// 		data.push(dishes);
   		// 		check();
   		// 	}
   		// })

    }
    //or start your remote connection here .... 
    //mongoose.connect(...);
    //close tunnel to exit script 
    // tunnel.close();
});

