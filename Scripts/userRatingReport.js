/*
    User Rating Report
 */

GLOBAL._ = require('underscore');
var fs = require('fs');
var mongoose = require('mongoose');
var json2csv = require('json2csv');
var HngreMailer = require('../operations/email');
var config = require('../config');
var moment = require('moment');

var User = require('../models/user');
var Rating = require('../models/rating');
var Merchant = require('../models/merchant');

var finalJSON = [];

var totalRatings = 0, fetchedData = 0;

var dbURI = config.mongodb.uri;

mongoose.connect(dbURI);

mongoose.connection.on('error', function (error) {

    throw error;
});

mongoose.connection.on('disconnected', function () {

    console.log('Mongoose default connection ended.');
});

mongoose.connection.on('connected', function () {

    console.log('Mongoose default connection open at ', {uri: dbURI});

    User.find({}, function (err, users) {

        if (err) throw err;

        users.forEach(function (user) {

            Rating.find({_user: user._id, rating: {$gt: 0}}, function (err, ratings) {

                if (err) throw err;
                totalRatings += ratings.length;
                console.log('Ratings are now at', totalRatings);

                ratings.forEach(function (rating) {

                    Merchant.aggregate([
                        {$match: {_id: rating._merchant}},
                        {$unwind: '$dishes'},
                        {$match: {'dishes._id': rating._dish}}
                    ], function (err, results) {

                        if (err) throw err;

                        fetchedData ++;
                        console.log('Fetched', fetchedData);
                        //console.log(merchant);
                        var merchant = results[0];

                        if (merchant)
                        {
                            try {

                                var record = {};
                                record.userID = user._id.toString();
                                record.userName = user.name;
                                if (user.profile_image) record.profileImage = user.profile_image;
                                record.vegPreference = user.veg_scale;
                                record.gender = user.gender;
                                record.age = moment().diff(user.birthday, 'years');
                                record.dishID = merchant.dishes._id.toString();
                                record.dishName = merchant.dishes.name;
                                record.merchantID = merchant._id.toString();
                                record.merchantName = merchant.name;
                                record.rating = rating.rating;
                                record.time = rating.timestamp;

                                finalJSON.push(record);

                            } catch (err) {

                                throw err;
                            }
                        }

                        if (fetchedData >= totalRatings)
                        {
                            console.log(finalJSON);

                            //var xls = json2xls(finalJSON);
                            //console.log(xls);

                            //finalJSON = {a: "b", c: 'd', e: 'f'};

                            json2csv({data: finalJSON}, function (err, csv) {
                                if (err) throw err;
                                console.log(csv);
                                fs.writeFileSync('data.csv', csv, 'binary');

                            });

                            //var email = new HngreMailer('internal');
                            //email.subject = 'User Rating Report';
                            //email.text = 'PFA';
                            //email.addAttachment({fileName: 'report.csv', contents: csv, contentType: 'text/csv', encoding: 'utf-8'});
                            //email.send(function (err, res) {
                            //    console.log(err, res);
                            //});
                        }
                    });
                });
            });
        });
    });
});
