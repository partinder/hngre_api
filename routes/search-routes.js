/*
    SEARCH ROUTES
 */

var SearchHandler = require('../handlers/search-handler');
var Joi = require('joi');


var searchQueryValidator = {
    query: Joi.object().keys({
        term: Joi.string().trim().required(),
        scope: Joi.string(),
        lon: Joi.number(),
        lat: Joi.number(),
        neighbourhood: Joi.string(),
        zip: Joi.string(),
        locality: Joi.string(),
        region: Joi.string(),
        country: Joi.string(),
        current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
        offset: Joi.number().default(0),
        group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
    })
}

module.exports = [

    { //Search for dishes with given parameters
        method: 'GET',
        path: '/search/dishes',
        handler: SearchHandler.search,
        config: {
            auth: 'token',
            validate: searchQueryValidator
        }
    },

    { //Search for merchants with given parameters
        method: 'GET',
        path: '/search/merchants',
        handler: SearchHandler.searchForMerchants,
        config: {
            auth: 'token',
            validate: searchQueryValidator,
            tags: ['api']
        }
    },

    { //Give suggestions based on search parameters (formation of search query)
        method: 'GET',
        path: '/search/suggest',
        handler: SearchHandler.suggest,
        config: {
            auth: 'token',
            validate: searchQueryValidator,
            tags: ['api']
        }
    },

    { //Suggest locations (zip codes, neighbourhoods) within a locality or around coordinates
        method: 'GET',
        path: '/search/locations',
        handler: SearchHandler.location,
        config: {
            auth: 'token',
            validate: searchQueryValidator,
            tags: ['api']
        }
    },

    { //Search for localities
        method: 'GET',
        path: '/search/localities',
        handler: SearchHandler.locality,
        config: {
            auth: 'token',
            validate: {
                params: Joi.object().keys({
                    country: Joi.string(),
                    region: Joi.string()
                })
            }
        }
    }
];