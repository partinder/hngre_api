(function() {
    /**
     * Module dependencies
     */
    'use strict';
    var Joi = require('joi');
    var InvitationHandler = require('../handlers/invitation-handler');

    module.exports = [
        /**
         * Invitation resolution
         */
        {
            method: 'GET',
            path: '/invitation/{inviteId}',
            handler: InvitationHandler.resolve,
            config: {
                auth: 'token',
                validate: {
                    params: {
                        inviteId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                    }
                },
                tags: ['api']
            }
        }
    ];
    
}())