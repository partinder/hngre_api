/*
    DISH ROUTES
    Route definitions and handling related to dishes.
 */

var DishHandler = require('../handlers/dish-handler');
var Joi         = require('joi');

module.exports = [

    {   //Get a specific dish
        method: 'GET',
        path: '/dishes/{dish_id}/{merchant_id?}',
        handler: DishHandler.findById,
        config: {
            auth: 'token',
            validate: {
                params: {
                    dish_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
                    merchant_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            }
        }
    },

    {   //Get all dishes for a particular merchant
        method: 'GET',
        path: '/dishes/merchant/{merchantId}',
        handler: DishHandler.findByMerchant,
        config: {
            auth: 'token',
            validate: {
                params: {
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                }
            }
        }
    },

    {	//Set rating for a dish by a user.
        method: 'POST',
        path: '/dishes/{dish_id}/{merchant_id}/action',
        handler: DishHandler.takeAction,
        config: {
            auth: 'token',
            validate: {
                params: {
                    dish_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    merchant_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                },
                payload: Joi.object().keys({
                    rating: Joi.number().integer().min(1).max(4),
                    not_for_me: Joi.bool(),
                    eat_later: Joi.bool()
                }).xor('rating','not_for_me','eat_later')
            }
        }
    },

    {
        //Get dishes on which action has been taken by the user.
        method: 'GET',
        path: '/dishes/action',
        handler: DishHandler.getActions,
        config: {
            auth: 'token',
            validate: {
                query: {
                    rated: Joi.boolean().default(false),
                    eat_later: Joi.boolean().default(false)
                }
            }
        }
    },

    {
        //Delete actions taken by the user
        method: 'DELETE',
        path: '/dishes/action',
        handler: DishHandler.clearActions,
        config: {
            auth: 'token',
            validate: {
                query: {
                    dish_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    merchant_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    rated: Joi.bool().default(false),
                    eat_later: Joi.bool().default(false)
                }
            }
        }
    },

    {	//Compute recommendations for user and send them back.
        method: 'GET',
        path: '/dishes/recommend',
        handler: DishHandler.recommend,
        config: {
            auth: 'token',
            validate: {
                query: Joi.object().keys({
                    current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    lon: Joi.number(),
                    lat: Joi.number(),
                    country: Joi.string(),
                    region: Joi.string(),
                    locality: Joi.string(),
                    neighbourhood: Joi.string(),
                    zip: Joi.string(),
                    size: Joi.number().default(10)
                }).and('lon', 'lat')
            }
        }
    },

    {   //Get sample dishes
        method: 'GET',
        path: '/dishes/recommend/sample',
        handler: DishHandler.sampleRecommendations
    }
];
