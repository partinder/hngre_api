/*
    MISC ROUTES
    Contains definitions and handling for miscellaneous routes.
 */

var LocationUpdateHandler = require('../handlers/location-update-handler');
var InteractionHandler = require('../handlers/interaction-handler');
var Joi = require('joi');
var Boom = require('boom');

var HngreMail = require('../operations/email');
var User = require('../models/user');

var staticStrings = {
    locationdisplay: 'New York - Delhi NCR'
};

module.exports = [

    { //Test response method
        method: 'GET',
        path: '/test',
        handler: function(request, reply) {
            var agent = request.plugins.scooter.toJSON();
            var _agent = request.headers['user-agent'];
            console.log(_agent);
            if (_agent && ~_agent.indexOf('HngreConsumer')) {
                //HngreConsumer/1.2.1 (iPhone; iOS 9.2.1; Scale/2.00)            
                var index1 = _agent.indexOf('/');
                if (~index1) {
                    var index2 = _agent.indexOf(' ');
                    if (~index2) {
                        var appVersion = _agent.substring(index1 + 1, index2);
                        appVersion = appVersion.split('.');
                        if (appVersion.length === 3) {
                            if (!agent.agent) {
                                agent.agent = {};
                            }
                            agent.agent.major = appVersion[0];
                            agent.agent.minor = appVersion[1];
                            agent.agent.patch = appVersion[2];
                        }
                    }
                }
            }
            reply({
                message: 'Welcome to the Hngre API',
                agent: agent
            });
        },
        config: {
            auth: false
        }
    },

    { //For posting feedback from app
        method: 'POST',
        path: '/feedback',
        handler: function(request, reply) {

            console.log('Feedback received: ', request.payload.feedback);

            var userId = request.auth.credentials.user_id;

            User.findById(userId, function(err, user) {

                console.log(user.toObject());

                var userName = user.name;
                var userEmail = request.payload.email;
                var userFeedback = request.payload.feedback;

                var messageString = 'UserId: ' + userId + '\nUser: ' + userName + '\nEmail: ' + userEmail + '\nFeedback:\n\n' + userFeedback;

                console.log('Email content:\n', messageString);

                //Send email to the team with feedback content
                var mail = new HngreMail('internal');
                mail.to = 'akash.gupta@hngre.com, partinder@hngre.com, rohit@hngre.com';
                if (request.payload.type === "contribution-access")
                    mail.subject = 'Request for Contribution Access';
                else mail.subject = 'Feedback Received From Consumer App';
                mail.text = messageString;
                mail.send();

            });

            reply({
                success: true
            });
        },
        config: {
            auth: 'token',
            validate: {
                payload: {
                    feedback: Joi.string().required(),
                    email: Joi.string().email().required(),
                    type: Joi.string().valid(['feedback', 'contribution-access']).default('feedback')
                }
            },
            tags: ['api']
        }
    },

    { //Post interactions which cannot be determined at the back end
        method: 'POST',
        path: '/track_interaction',
        handler: InteractionHandler.trackInteraction,
        config: {
            auth: {
                strategy: 'token',
                mode: 'optional'
            },
            validate: {
                payload: {
                    deviceId: Joi.string(),
                    event: Joi.string().required(),
                    dishId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    phone: Joi.string().when('event', {
                        is: 'call',
                        then: Joi.required(),
                        otherwise: Joi.forbidden()
                    }),
                    actionName: Joi.string().when('event', {
                        is: 'action',
                        then: Joi.required(),
                        otherwise: Joi.forbidden()
                    }),
                    properties: Joi.object()
                }
            },
            tags: ['api']
        }
    },

    { //Terms document for info
        method: 'GET',
        path: '/info/terms',
        handler: {
            file: './public/terms.html'
        },
        config: {
            auth: false,
            tags: ['api']
        }
    },

    { //Privacy policy document for info
        method: 'GET',
        path: '/info/privacy',
        handler: {
            file: './public/privacy.html'
        },
        config: {
            auth: false,
            tags: ['api']
        }
    },

    { //Strings required throughout the app
        method: 'GET',
        path: '/static/strings/{key}',
        handler: function(request, reply) {

            var returnString = staticStrings[request.params.key];
            if (returnString) {
                reply({
                    value: returnString
                })
            } else {
                reply(Boom.notFound('No value for specified key'));
            }

        },
        config: {
            validate: {
                params: {
                    key: Joi.string().required()
                }
            },
            auth: false,
            tags: ['api']
        }
    },

    { //Webhook for events from branch.io (INCOMPLETE)
        method: 'POST',
        path: '/branch_event',
        handler: function(request, reply) {
            reply({})
        },
        config: {
            auth: false
        }
    },

    { //For periodic location updates of the user (INCOMPLETE)
        method: 'POST',
        path: '/location_update/location',
        handler: LocationUpdateHandler.location,
        config: {
            auth: 'token',
            validate: {
                payload: {
                    lat: Joi.number().required(),
                    lon: Joi.number().required()
                }
            }
        }
    },

    { //For posting a visit of the user (INCOMPLETE)
        method: 'POST',
        path: '/location_update/visit',
        handler: LocationUpdateHandler.visit,
        config: {
            auth: 'token',
            validate: {
                payload: {
                    lat: Joi.number().required(),
                    lon: Joi.number().required()
                }
            }
        }
    },

    { //For posting a visit of the user (INCOMPLETE)
        method: 'GET',
        path: '/locations',
        handler: LocationUpdateHandler.getLocations,
        config: {
            auth: {
                strategy: 'token',
                mode: 'optional'
            }
        }
    },

    { 
        method: 'GET',
        path: '/loaderio-38ca06b4e407256ca2ad78268e168982.txt',
        handler: {
            file: './public/loaderio-38ca06b4e407256ca2ad78268e168982.txt'
        },
        config: {
            auth: false,
            tags: ['api']
        }
    }
];
