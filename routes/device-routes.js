/*
    USER ROUTES
    Contains route definitions and handlers associated with users.
 */

var UserHandler = require('../handlers/user-handler');
var Joi = require('joi');

var deviceIdentifierRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i; //Device UUID
var objectIdRegex = /^[0-9a-fA-F]{24}$/;

module.exports = [

    { //Add a device for the user
        method: 'POST',
        path: '/device/{device_id}/notification_token',
        handler: UserHandler.addDevice,
        config: {
            auth: 'token',
            validate: {
                params: {
                    device_id: Joi.string().regex(deviceIdentifierRegex).required()
                },
                payload: {
                    apn_token: Joi.string(),
                    gcm_token: Joi.string()
                }
            },
            tags: ['api']
        }
    }
];