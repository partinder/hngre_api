/*
    MERCHANT ROUTES
    Route definitions and handling related to Merachants.
 */


var Joi = require('joi');
var MerchantHandler = require('../handlers/merchant-handler');
var DishHandler = require('../handlers/v2/dish-handler');
var config = require('../config');

module.exports = [

    { //Get merchant profile
        method: 'GET',
        path: '/merchants/{merchantId}',
        handler: MerchantHandler.findById,
        config: {
            auth: 'token',
            validate: {
                params: {
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    { //Get all dishes for a particular merchant
        method: 'GET',
        path: '/merchant/dishes',
        handler: MerchantHandler.findByMerchantDishes,
        config: {
            auth: 'token',
            validate: {
                query: Joi.object().keys({
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    dishId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }).or('merchantId', 'dishId')
            }
        }
    },

    { //Get hours for merchant (Can also be implemented via query)
        method: 'GET',
        path: '/merchants/{merchantId}/hours',
        handler: MerchantHandler.getHours,
        config: {
            auth: 'token',
            validate: {
                params: {
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            }
        }
    },

    { //Get status for merchant (Can also be implemented via query)
        method: 'GET',
        path: '/merchants/{merchantId}/hours/status',
        handler: MerchantHandler.getHoursStatus,
        config: {
            auth: 'token',
            validate: {
                params: {
                    merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            }
        }
    },

    { //Compute recommendations for user and send them back.
        method: 'GET',
        path: '/merchants/recommend',
        handler: DishHandler.recommend,
        config: {
            auth: 'token',
            validate: {
                query: Joi.object().keys({
                    current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    lon: Joi.number(),
                    lat: Joi.number(),
                    country: Joi.string(),
                    region: Joi.string(),
                    locality: Joi.string(),
                    neighbourhood: Joi.string(),
                    zip: Joi.string(),
                    size: Joi.number().default(7),
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }).and('lon', 'lat')
            },
            tags: ['api']
        }
    },

    { //Compute recommendations for user and send them back.
        method: 'GET',
        path: '/merchants/recommend/{user_id}',
        handler: DishHandler.recommend,
        config: {
            validate: {
                query: Joi.object().keys({
                    current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    lon: Joi.number(),
                    lat: Joi.number(),
                    country: Joi.string(),
                    region: Joi.string(),
                    locality: Joi.string(),
                    neighbourhood: Joi.string(),
                    zip: Joi.string(),
                    size: Joi.number().default(7),
                    token: Joi.string().valid(config.api_server_token).required()
                }).and('lon', 'lat')
            }
        }
    },  

    { //Compute recommendations for user and send them back.
        method: 'GET',
        path: '/merchants/recommend/test',
        handler: MerchantHandler.recommend1,
        config: {
            auth: 'token',
            validate: {
                query: Joi.object().keys({
                    current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    lon: Joi.number(),
                    lat: Joi.number(),
                    country: Joi.string(),
                    region: Joi.string(),
                    locality: Joi.string(),
                    neighbourhood: Joi.string(),
                    zip: Joi.string(),
                    size: Joi.number().default(7),
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }).and('lon', 'lat')
            }
        }
    },

    {
        //Get dishes on which action has been taken by the user.
        method: 'GET',
        path: '/merchants/action',
        handler: DishHandler.getActions,
        config: {
            auth: 'token',
            validate: {
                query: {
                    rated: Joi.boolean().default(false),
                    eat_later: Joi.boolean().default(false)
                }
            },
            tags: ['api']
        }
    },

];