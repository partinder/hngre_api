/*
    DISH ROUTES
    Route definitions and handling related to dishes.
 */

var GroupHandler = require('../handlers/group-handler');
var Joi = require('joi');

var membersArraySchema = Joi.array().items(
    Joi.object().keys({
        userId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
        name: Joi.string().optional().allow(null),
        first_name: Joi.string().optional().allow(null),
        last_name: Joi.string().optional().allow(null)
    }).unknown(true),
    Joi.object().keys({
        email: Joi.string().email(),
        name: Joi.string().optional().allow(null),
        first_name: Joi.string().optional().allow(null),
        last_name: Joi.string().optional().allow(null)
    }).unknown(true),
    Joi.object().keys({
        phone: Joi.string(),
        name: Joi.string().optional().allow(null),
        first_name: Joi.string().optional().allow(null),
        last_name: Joi.string().optional().allow(null)
    }).unknown(true)
).max(5);

module.exports = [{

        //create new group
        method: 'POST',
        path: '/groups',
        handler: GroupHandler.create,
        config: {
            auth: 'token',
            validate: {
                payload: Joi.object().keys({
                    name: Joi.string().required(),
                    description: Joi.string().allow(null),
                    members: membersArraySchema,
                    group_icon: Joi.string().allow(null)
                }).unknown(true)
            },
            tags: ['api']
        }
    },

    { //Get a specific group
        method: 'GET',
        path: '/groups/{group_id}',
        handler: GroupHandler.loadMyGroup,
        config: {
            auth: 'token',
            validate: {
                params: {
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    { //Get a specific user's groups
        method: 'GET',
        path: '/users/{user_id}/groups',
        handler: GroupHandler.loadGroups,
        config: {
            auth: 'token',
            validate: {
                params: {
                    user_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    { //Get a specific user's groups
        method: 'GET',
        path: '/users/me/groups',
        handler: GroupHandler.loadGroups,
        config: {
            auth: 'token',
            tags: ['api']
        }
    },

    {
        //Delete group
        method: 'DELETE',
        path: '/groups/{group_id}',
        handler: GroupHandler.remove,
        config: {
            auth: 'token',
            validate: {
                params: {
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    { //update group.
        method: 'PUT',
        path: '/groups/{group_id}',
        handler: GroupHandler.update,
        config: {
            auth: 'token',
            validate: {
                payload: {
                    name: Joi.string(),
                    description: Joi.string()
                }
            },
            tags: ['api']
        }
    },

    { //add new member into the group
        method: 'GET',
        path: '/groups/{group_id}/add/{member_id}',
        handler: GroupHandler.joinGroup,
        config: {
            auth: 'token',
            validate: {
                params: {
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    member_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            }
        }
    },

    { //add new member into the group
        method: 'POST',
        path: '/groups/{group_id}/add',
        handler: GroupHandler.addMembers,
        config: {
            auth: 'token',
            validate: {
                params: {
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                },
                payload: {
                    members: membersArraySchema
                }
            },
            tags: ['api']
        }
    },

    {
        //remove a member from the group
        method: 'DELETE',
        path: '/groups/{groupId}/{type}/{memberId}',
        handler: GroupHandler.leaveGroup,
        config: {
            auth: 'token',
            validate: {
                params: {
                    groupId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    type: Joi.string().valid('member', 'invited'),
                    memberId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    {
        //group image uplaod
        method: 'POST',
        path: '/groups/upload',
        config: {
            auth: 'token',
            validate: {
                query: {
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            payload: {
                maxBytes: 209715200,
                output: 'stream',
                parse: false
            },
            handler: GroupHandler.uploadImage,
            tags: ['api']
        }
    },

];