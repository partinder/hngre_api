(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Joi = require('joi');
    var Contribution = require('../handlers/contribution-handler');
    var mapVegType = function(c) {
        console.log(c);
        switch (c.veg_type) {
            case 'vegan':
                {
                    return [1];
                };
                break;
            case 'veg':
                {
                    return [1, 2];
                };
                break;
            case 'nonveg':
                {
                    return [3];
                };
                break;
            default:
                {
                    return [1, 2, 3];
                }
        }
    };
    mapVegType.description = "Mapping Veg Type";

    module.exports = [{
        //contribution image uplaod
        method: 'POST',
        path: '/contribution/upload',
        config: {
            auth: 'token',
            validate: {
                query: {
                    merchant_name: Joi.string().required(),
                    dish_name: Joi.string().required(),
                    location: Joi.string(),
                    veg_type: Joi.string().valid('veg', 'nonveg', 'vegan'),
                    veg_scale: Joi.default(mapVegType)
                }
            },
            payload: {
                maxBytes: 209715200,
                output: 'stream',
                parse: false
            },
            handler: Contribution.uploadImage,
            tags: ['api']
        }
    }];
}())
