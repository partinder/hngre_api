/*
    USER ROUTES
    Contains route definitions and handlers associated with users.
 */

var UserHandler = require('../handlers/user-handler');
var GroupHandler = require('../handlers/group-handler');
var Joi = require('joi');
var config = require('../config');

var deviceIdentifierRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i; //Device UUID
var objectIdRegex = /^[0-9a-fA-F]{24}$/;

module.exports = [

    { //Login User
        method: 'POST',
        path: '/users/login',
        handler: UserHandler.login,
        config: {
            auth: {
                strategy: 'token',
                mode: 'optional'
            },
            validate: {
                payload: {
                    loginType: Joi.string().valid('facebook', 'google', 'temp', 'device', 'digits').required(),
                    token: Joi.string().when('loginType', {
                        is: 'facebook',
                        then: Joi.required(),
                        otherwise: Joi.forbidden()
                    }),
                    authCode: Joi.string().when('loginType', {
                        is: 'google',
                        then: Joi.required(),
                        otherwise: Joi.forbidden()
                    }),
                    email: Joi.string().email().when('loginType', {
                        is: 'temp',
                        then: Joi.required()
                    }),
                    name: Joi.string().when('loginType', {
                        is: 'temp',
                        then: Joi.required()
                    }),
                    device: Joi.string().regex(deviceIdentifierRegex).required(),
                    phone: Joi.string().when('loginType', {
                        is: 'digits',
                        then: Joi.required()
                    }),
                    digitsToken: Joi.string().when('loginType', {
                        is: 'digits',
                        then: Joi.required()
                    }),
                    digitsTokenSecret: Joi.string().when('loginType', {
                        is: 'digits',
                        then: Joi.required()
                    }),
                    inviteId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    { //Logout user from device
        method: 'GET',
        path: '/logout/{device_id}', //Can be either 'me' or userId
        handler: UserHandler.logout,
        config: {
            auth: {
                strategy: 'token',
                mode: 'optional'
            },
            validate: {
                params: {
                    device_id: Joi.string().regex(deviceIdentifierRegex).required()
                }
            }
        }
    },

    { //Logout user from device
        method: 'GET',
        path: '/users/logout/{device_id}', //Can be either 'me' or userId
        handler: UserHandler.logout,
        config: {
            auth: {
                strategy: 'token',
                mode: 'optional'
            },
            validate: {
                params: {
                    device_id: Joi.string().regex(deviceIdentifierRegex).required()
                }
            },
            tags: ['api']
        }
    },

    { //Get user details
        method: 'GET',
        path: '/users/{userId}', //Can be either 'me' or userId
        handler: UserHandler.profile,
        config: {
            auth: 'token',
            validate: {
                params: {
                    userId: Joi.alternatives().try(Joi.string().valid('me'), Joi.string().regex(objectIdRegex)).required()
                }
            },
            tags: ['api']
        }
    },

    { //Edit user profile
        method: 'POST',
        path: '/users/{userId}',
        handler: UserHandler.editProfile,
        config: {
            auth: 'token',
            validate: {
                params: {
                    userId: Joi.string().regex(objectIdRegex)
                }
            },
            tags: ['api']
        }
    },

    { //Get user image
        method: 'GET',
        path: '/users/{userId}/image',
        handler: UserHandler.getPhoto,
        config: {
            auth: false,
            validate: {
                params: {
                    userId: Joi.string().regex(objectIdRegex).required()
                }
            }
        }
    },

    { //Add a device for the user
        method: 'POST',
        path: '/users/device',
        handler: UserHandler.addDevice,
        config: {
            auth: 'token',
            validate: {
                payload: {
                    device: Joi.string().regex(deviceIdentifierRegex).required(),
                    token: Joi.any()
                }
            }
        }
    },

    { //Get hngre confidence index for the user
        method: 'GET',
        path: '/users/me/hngreIndex',
        handler: UserHandler.getHngreIndex,
        config: {
            auth: 'token',
            validate: {
                query: {
                    location: Joi.boolean().default(false)
                }
            }
        }
    },

    { //Get hngre confidence index for the user
        method: 'GET',
        path: '/users/{userId}/hngreIndex',
        handler: UserHandler.getHngreIndex,
        config: {
            auth: 'token',
            validate: {
                query: {
                    location: Joi.boolean().default(false)
                }
            }
        }
    },

    {
        //group image uplaod
        method: 'POST',
        path: '/users/me/photo/upload',
        config: {
            auth: 'token',
            payload: {
                maxBytes: 209715200,
                output: 'stream',
                parse: false
            },
            handler: UserHandler.uploadImage
        }
    },

    {
        //get userId using facebook Id, google Id, email and phone number
        method: 'POST',
        path: '/users/me/contacts',
        handler: GroupHandler.getUsers,
        config: {
            auth: 'token',
            validate: {
                payload: Joi.array().items(Joi.object().keys({
                    name: Joi.string(),
                    first_name: Joi.string(),
                    last_name: Joi.string(),
                    emails: Joi.array(),
                    phones: Joi.array()
                }))
            },
            tags: ['api']
        }
    },

    {
        //merge user account  with the user
        method: 'POST',
        path: '/users/{hngre_generated_id}/merge',
        handler: UserHandler.mergeAccount,
        config: {
            validate: {
                params: {
                    hngre_generated_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                },
                payload: {
                    user_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
                    first_name: Joi.string(),
                    last_name: Joi.string(),
                    phone: Joi.string(),
                    role: Joi.string().valid('contributor').default('contributor'),
                    token: Joi.string().valid(config.api_server_token).required()
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/users/{contributor_id}/dishes',
        handler: UserHandler.dishesDiscovered,
        config: {
            auth: 'token',
            validate: {
                params: {
                    contributor_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                },
                query: {
                    current_rec_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                    offset: Joi.number().default(0),
                    lon: Joi.number(),
                    lat: Joi.number(),
                    country: Joi.string(),
                    region: Joi.string(),
                    locality: Joi.string(),
                    group_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            },
            tags: ['api']
        }
    },

    {
        method: 'GET',
        path: '/users/friends/branchlink',
        handler: UserHandler.getBranchLink,
        config: {
            auth: 'token',
            validate: {
                query: {
                    channel: Joi.string().valid('Email', 'Facebook', 'SMS', 'WhatsApp').required()
                }
            },
            tags: ['api']
        }
    }
];
