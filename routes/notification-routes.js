(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Joi = require('joi');
    var Notification = require('../handlers/notification-handler');
    var config = require('../config');
    var constants = require('../constants');

    module.exports = [
        /**
         * Create a notification
         */
        {
            method: 'POST',
            path: '/notifications',
            handler: Notification.create,
            config: {
                validate: {
                    payload: {
                        user: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                        title: Joi.string(),
                        body: Joi.string().required(),
                        path: Joi.string(),
                        type: Joi.string().default('info')
                    }
                }
            }
        },
        /**
         * Create a notifications
         */
        {
            method: 'POST',
            path: '/notifications/generate',
            handler: Notification.generateNotifications,
            config: {
                validate: {
                    query: {
                        token: Joi.string().valid(config.api_server_token).required()
                    },
                    payload: {
                        userId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
                        data: Joi.object().keys({
                            event: Joi.string().required(),
                            title: Joi.string(),
                            body: Joi.string(),
                            path: Joi.string(),
                            type: Joi.string().valid('info', 'action').default('info'),
                            to: Joi.string().valid('inapp', 'inapp-push').default('inapp'),
                            icon: Joi.string().default(constants.default.hngre_icon),
                            initiator: Joi.object().keys({
                                from: Joi.string(),
                                type: Joi.string()
                            }),
                            sender: Joi.object().keys({
                                first_name: Joi.string(),
                                icon: Joi.string()
                            })
                        })
                    }
                }
            }
        },
        /**
         * Get top notifications by time series
         */
        {
            method: 'GET',
            path: '/notifications/latest',
            handler: Notification.getAll,
            config: {
                auth: 'token',
                validate: {
                    query: {
                        page: Joi.number().default(0),
                        perPage: Joi.number().default(10)
                    }
                },
                tags: ['api']
            }
        },
        /**
         * Get top notifications counts by time series
         */
        {
            method: 'GET',
            path: '/notifications/count',
            handler: Notification.getCounts,
            config: {
                auth: 'token',
                tags: ['api']
            }
        },
        /**
         * Get the notification by notification id
         */
        {
            method: 'GET',
            path: '/notifications/{notification_id}',
            handler: Notification.get,
            config: {
                auth: 'token',
                validate: {
                    params: {
                        notification_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                    }
                },
                tags: ['api']
            }
        },
        /**
         * Mark the notifications as unread
         */
        {
            method: 'PUT',
            path: '/notifications/mark/viewed',
            handler: Notification.markAsViewed,
            config: {
                auth: 'token',
                validate: {
                    payload: {
                        notification_ids: Joi.array().items(Joi.string())
                    }
                },
                tags: ['api']
            }
        },
        /**
         * Mark the notifications as read
         */
        {
            method: 'PUT',
            path: '/notifications/{notification_id}/mark/read',
            handler: Notification.markAsRead,
            config: {
                auth: 'token',
                validate: {
                    params: {
                        notification_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                    }
                },
                tags: ['api']
            }
        }
    ]
}());