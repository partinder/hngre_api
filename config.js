/*
    CONFIG
    The configuration file for the Hngre API.
 */

var fs = require('fs');
var config;
var join = require('path').join;

console.log('Current environment', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
    config = {
        mongodb: {
            uri: 'mongodb://52.74.138.138/db_hngre'
        },
        elasticsearch: {
            uri: 'http://52.76.238.93:9200',
            log: false
        },
        branch: {
            key: 'key_live_llaHDxYLT3Zwwewt353WokdcByci9bC2',
            uri: 'https://api.branch.io/v1/url'
        },
        recEngine: {
            uri: 'http://52.74.138.138:9500'
        },
        notifier: {
            uri: 'http://localhost:3031',
            url: 'http://localhost:3031/api/events?access_token=1234'
        },
        redis: {
            uri: "redis://localhost:6379"
        },
        serverConfig: {
            host: '0.0.0.0',
            port: 1232,
            tls: {
                key: fs.readFileSync(join(__dirname, 'SSL/api.hngre.com.key')),
                cert: fs.readFileSync(join(__dirname, 'SSL/ssl-bundle.crt'))
            }
        },
        fullContact: {
            apiKey: '36dbd10a3546d540',
            protocol: 'https'
        },
        api_server_token: "UBOhX3",
        admin: {
            merchants: {
                update: {
                    uri: 'http://192.168.1.200:3000/mergeuser'
                }
            },
            server_token: "6ji4nuh4lp3i"
        },
        google_analytics: {
            tracking_id: 'UA-73684304-3'
        },
        mixpanel_analytics: {
            token: '3c0dbad913270bb894daab9648987bc6',
            apiKey: 'c9bd4282dd8bd425083a24c749219a42',
            apiSecret: '2a47eb821384d12ecebbd513df72af90'
        }
    };
} else {
    config = {
        mongodb: {
            uri: 'mongodb://localhost/db_hngre'
        },
        elasticsearch: {
            uri: 'http://localhost:9200',
            log: true
        },
        branch: {
            key: 'key_live_llaHDxYLT3Zwwewt353WokdcByci9bC2',
            uri: 'https://api.branch.io/v1/url'
        },
        recEngine: {
            uri: 'http://192.168.1.200:9500'
        },
        notifier: {
            uri: 'http://localhost:3031',
            url: 'http://localhost:3031/api/events?access_token=1234'
        },
        redis: {
            uri: "redis://localhost:6379"
        },
        serverConfig: {
            host: '0.0.0.0',
            port: 3001
        },
        fullContact: {
            apiKey: '36dbd10a3546d540',
            protocol: 'http'
        },
        api_server_token: "UBOhX3",
        admin: {
            merchants: {
                update: {
                    uri: 'http://192.168.1.200:3000/mergeuser'
                }
            },
            server_token: "6ji4nuh4lp3i"
        },
        google_analytics: {
            tracking_id: 'UA-73684304-4'
        },
        mixpanel_analytics: {
            token: 'c85a64cbd0eb4e1c5a0c2dbfc9acb614',
            apiKey: '55db747c5519cf96d6660e9aad7733ef',
            apiSecret: '8df2ebd18e8e2d0fcbedabf1c4fbdf2e'
        }
    };
}

module.exports = config;
