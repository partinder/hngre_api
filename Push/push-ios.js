/*
    PUSH-IOS
    Creates and manages the agent for sending push notifications to iOS devices using APNs.
 */

// Locate your certificate
var join = require('path').join,
    pfx = join(__dirname, './hngreconsumer-ios-dev.p12');

// Create a new agent
var apnagent = require('apnagent'),
    agent = new apnagent.Agent();

agent.set('pfx file', pfx);
agent.set('passphrase','hngre12345');

agent.enable('sandbox');

agent.connect(function (err) {

    // gracefully handle auth problems
    if (err && err.name === 'GatewayAuthorizationError')
    {
        console.log('APNS Authentication Error: %s', err.message);
    }

    // handle any other err (not likely)
    else if (err)
    {
        console.log(err);
    }

    // it worked!
    var env = agent.enabled('sandbox') ? 'sandbox' : 'production';

    console.log('apnagent [%s] gateway connected', env);
});

agent.on('message:error', function (err, msg) {

    console.log(err, msg);
});

module.exports = {
    
    agent: agent,
    
    notify: function (token, alert, userInfo) {

        var pushNotification = agent.createMessage()
                                    .device(token)
                                    .alert(alert)
                                    .sound('default');

        if (userInfo)
        {
            var keys = _.keys(userInfo);

            keys.forEach(function (key) {
                pushNotification.set(key, userInfo[key]);
            });
        }

        pushNotification.send(function (err) {

                // handle apnagent custom errors
                if (err && err.toJSON)
                {
                    console.log('Error sending notification', err);
                }

                // handle anything else (not likely)
                else if (err)
                {
                    console.log('Error sending notification', err.message);
                }

                // it was a success
                else
                {
                    console.log('Successfully sent notification');
                }
            });
    }
};