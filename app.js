/*
    APP
    The Hngre Server API
 */

if (process.env.NODE_ENV === 'production') require('newrelic');

GLOBAL._ = require('underscore');

var hapi = require('hapi');
var mongoose = require('mongoose');
var GoodWinston = require('good-winston');
var logger = require('./logger');
var error = require('./error');

var userAuth = require('./operations/auth/user-authentication');
var userRoutes = require('./routes/user-routes');
var deviceRoutes = require('./routes/device-routes');
var dishRoutes = require('./routes/dish-routes');
var merchantRoutes = require('./routes/merchant-routes');
var miscRoutes = require('./routes/misc-routes');
var searchRoutes = require('./routes/search-routes');
var groupRoutes = require('./routes/group-routes');
var notificationRoutes = require('./routes/notification-routes');
var invitationRoutes = require('./routes/invitation-routes');
var contributionRoutes = require('./routes/contribution-routes');

var config = require('./config');

//Documentation
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
var auth = require('basic-auth');

//TEST - Initialize the Alohar waterfall api
//var alohar = require('./operations/location-update/alohar-waterfall');

//Initialising server
var server = new hapi.Server();

server.connection(config.serverConfig);

process.on('uncaughtException', function(err) {
    logger.log('error', 'Uncaught exception', err);
    error.report(err, { error: 'Uncaught exception' });
    process.exit(1);
});

server.ext('onRequest', function(request, reply) {
    //console.log(request.headers['user-agent']); 
    return reply.continue();
});

server.ext('onPreAuth', function(request, reply) {
    //Check user agent information
    //Continue if iOS app, forbidden if other
    var _agent = request.headers['user-agent'];
    if (process.env.NODE_ENV === 'production') {
        if (_agent && ~_agent.indexOf('HngreConsumer')) {
            //HngreConsumer/1.2.1 (iPhone; iOS 9.2.1; Scale/2.00)            
            return reply.continue();
        } else {
            return reply.continue();
            /*var credentials = auth(request);
            var username = "suresh";
            var password = "Pa$$w0rd";
            if (!credentials || credentials.name !== username || credentials.pass !== password) {
                return reply('Access denied').header('WWW-Authenticate', 'Basic realm="developer"').code(401);
            } else {
                return reply.continue();
            }*/
        }
    } else {
        return reply.continue();
    }
});

server.ext('onPostAuth', function(request, reply) {
    //console.log(request.plugins.scooter.toJSON()); 
    //console.log("User Agent", request.headers['user-agent']);
    return reply.continue();
});

//Initialising Mongoose

var dbURI = config.mongodb.uri;

mongoose.connect(dbURI);

mongoose.connection.on('error', function(err) {
    logger.log('error', 'Mongoose default connection error', err);
    error.report(err, { error: 'Mongoose default connection error' });
    process.exit(1);
});

mongoose.connection.on('disconnected', function() {
    logger.log('info', 'Mongoose default connection ended.');
    error.report(new Error('Mongodb disconnected.'), { error: 'Mongoose default connection ended.' });
});

mongoose.connection.on('connected', function() {

    logger.log('info', 'Mongoose default connection open at ', {
        uri: dbURI
    });

    server.register([{
            register: require('hapi-auth-jwt'),
            options: {}
        }, {
            register: require('scooter'),
            options: {}
        }, {
            register: require('good'),
            options: {
                opsInterval: 1000,
                reporters: [
                    new GoodWinston({
                        /*    ops: '*',*/
                        request: '*',
                        response: '*',
                        log: '*',
                        error: '*'
                    }, logger)
                ]
            }
        },
        Inert,
        Vision, {
            register: HapiSwagger,
            options: {
                info: {
                    title: "Hngre API Documentation",
                    contact: {
                        name: "Partinder Singh",
                        email: "partinder@hngre.com"
                    }
                },
                schemes: ['https'],
                // host: 'api.hngre.com',
                tags: [{
                    name: 'users',
                    description: 'Login & User Data'
                }, {
                    name: 'search',
                    description: 'Search for Merchants and Locations'
                }, {
                    name: 'groups',
                    description: 'Create and maintain groups'
                }, {
                    name: 'merchants',
                    description: 'Merchant Recommendations and Data'
                }, {
                    name: 'notifications',
                    description: 'User Notifications'
                }, {
                    name: 'device',
                    description: 'Device information'
                }, {
                    name: 'invitation',
                    description: 'Manage invitations'
                }, {
                    name: 'users',
                    description: 'Login & User Data'
                }, {
                    name: 'track_interaction',
                    description: 'Analytics and Interaction Events'
                }, {
                    name: 'feedback',
                    description: 'User Feedback'
                }, {
                    name: 'info',
                    description: 'App Information'
                }, {
                    name: 'static',
                    description: 'Static Data'
                }]
            }
        }
    ], function(err) {

        if (err) throw err;

        //server.auth.strategy('simple', 'basic', { validateFunc: validate });
        server.auth.strategy('token', 'jwt', 'optional', userAuth.authOptions); //Setting as default

        //Adding Routes
        server.route(userRoutes);
        server.route(deviceRoutes);
        server.route(dishRoutes);
        server.route(merchantRoutes);
        server.route(searchRoutes);
        server.route(miscRoutes);
        server.route(groupRoutes);
        server.route(notificationRoutes);
        server.route(invitationRoutes);
        server.route(contributionRoutes);

        //Finally, starting the server
        server.start(function() {
            logger.log('info', 'Hngre API server started at', server.info.uri);
        });
    });

});
