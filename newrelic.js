/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name: ['Hngre API'],
  /**
   * Your New Relic license key.
   */
  license_key: '106c1cb78ca35f376b7cb1984dad7acdeb0a56bc',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level: 'info',
    filepath : '/var/log/newrelic_agent.log'
  }
}
