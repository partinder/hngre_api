(function() {
    'use strict';
    var should = require('should');
    var assert = require('assert');
    var request = require('supertest');
    var helper = require('./helper');

    describe('Routing', function() {

        // to perform user test!
        describe('Account', function() {

            it('should return token for the user', function(done) {

                request(helper.serviceUrl)
                    .post(helper.users.login.path)
                    .send(helper.users.login.data)
                    .expect(200)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        res.body.should.have.property('token');
                        res.body.token.should.not.equal(null);
                        res.body.should.have.property('user');
                        res.body.user.should.have.property('_id');
                        res.body.user.should.have.property('name');
                        res.body.user.should.have.property('veg_scale');
                        res.body.user.should.have.property('phone_verified');
                        res.body.user.should.have.property('login_count');
                        res.body.user.should.have.property('rate_count');
                        done();
                        Authorization += res.body.token;
                        user_id = res.body.user._id;
                    });
            });

            it('should return user details', function(done) {
                request(helper.serviceUrl)
                    .get(helper.users.details.path)
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                        user = res.body;
                    });
            });

            it('should return user photo', function(done) {
                request(helper.serviceUrl)
                    .get(helper.users.photo.path(user_id))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                        user = res.body;
                    });
            });

            it('should correctly update an existing account', function(done) {

                request(helper.serviceUrl)
                    .post(helper.users.edit.path(user_id))
                    .set({
                        Authorization: Authorization
                    })
                    .send(helper.users.edit.data)
                    .expect('Content-Type', /json/)
                    .expect(200) //Status code
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        // Should.js fluent syntax applied
                        res.body.should.have.property('_id');
                        res.body.gender.should.equal('male');
                        done();
                    });
            });

            it('should return user hngre index', function(done) {

                request(helper.serviceUrl)
                    .get(helper.users.hngreIndex.path(user_id))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200) //Status code
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        // Should.js fluent syntax applied
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return user contacts', function(done) {

                request(helper.serviceUrl)
                    .post(helper.users.contacts.path(user_id))
                    .set({
                        Authorization: Authorization
                    })
                    .send(helper.users.contacts.data)
                    .expect(200) //Status code
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        // Should.js fluent syntax applied
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return user profile photo', function(done) {

                var filename = helper.users.upload.filename,
                    boundary = Math.random()
                request(helper.serviceUrl)
                    .post(helper.users.upload.path(user_id))
                    .set({
                        Authorization: Authorization
                    })
                    .set('Content-Type', 'multipart/form-data; boundary=' + boundary)
                    .write('--' + boundary + '\r\n')
                    .write('Content-Disposition: form-data; name="image"; filename="' + filename + '"\r\n')
                    .write('Content-Type: image/png\r\n')
                    .write('\r\n')
                    .write(helper.users.upload.data)
                    .write('\r\n--' + boundary + '--')
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        res.should.have.status(200)
                        done();
                    });
            });

            it('should return success when device is added', function(done) {
                request(helper.serviceUrl)
                    .post(helper.users.device.path)
                    .set({
                        Authorization: Authorization
                    })
                    .send(helper.users.device.data)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        done();
                    })
            });            

        });
    });

}())
