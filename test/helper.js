(function() {
    'use strict';
    var fs = require('fs');

    module.exports = {

        port: 3001,

        host: 'http://192.168.1.201',

        serviceUrl: 'http://192.168.1.201:3001',

        users: {
            details: {
                path: '/users/me'
            },
            login: {
                path: '/users/login',
                data: {
                    name: 'vgheri',
                    email: 'test@gmail.com',
                    loginType: 'temp'
                }
            },
            edit: {
                path: function(userId) {
                    return '/users/' + userId;
                },
                data: {
                    veg_scale: [3],
                    gender: 'male',
                    birthday: new Date(1988, 7, 14)
                }
            },
            photo: {
                path: function(userId) {
                    return '/users/' + userId + '/image';
                }
            },
            hngreIndex: {
                path: function(userId) {
                    return '/users/' + userId + '/hngreIndex';
                }
            },
            contacts: {
                path: function(userId) {
                    return '/users/me/contacts';
                },
                data: [{
                    name: "",
                    first_name: "Suresh",
                    last_name: "Mahawar",
                    emails: ['suresh.mahawar@hngre.com'],
                    phones: ['+918802193481']
                }, {
                    name: "",
                    first_name: "Partinder",
                    last_name: "Singh",
                    emails: ['partinder@hngre.com'],
                    phones: ['+919456514991']
                }]
            },
            upload: {
                path: '/users/me/photo/upload',
                filename: 'FC4ULOK5Rt.png',
                data: fs.readFileSync('test/FC4ULOK5Rt.png')
            },
            device: {
                path: '/users/device',
                data: {
                    device: '',
                    token: ''
                }
            }
        },

        merchants: {
            dishes: {
                path: function(merchantId) {
                    return "/merchants/" + merchantId;
                }
            },
            mdishes: {
                path: function(merchantId, dishId) {
                    if (merchantId && dishId) {
                        return "/merchant/dishes?merchantId=" + merchantId + "&dishId=" + dishId;
                    } else if (merchantId) {
                        return "/merchant/dishes?merchantId=" + merchantId;
                    } else if (dishId) {
                        return "/merchant/dishes?dishId=" + dishId;
                    } else {
                        return "/merchant/dishes?merchantId=&dishId=";
                    }
                }
            },
            hours: {
                path: function(merchantId) {
                    return "/merchants/" + merchantId + "/hours";
                }
            },
            hourStatus: {
                path: function(merchantId) {
                    reutrn "/merchants/" + merchantId + "/hours/status";
                }
            },
            recommendation: {
                path: function(params) {
                    var data = {
                        current_rec_id: "",
                        lon: 77.0553272,
                        lat: 28.4122836,
                        country: "India",
                        region: "Haryana",
                        locality: "Gurgaon",
                        neighbourhood: "DLF Phase 4",
                        zip: "122001",
                        size: 7,
                        group_id: ""
                    };
                    if (params.isLatLon) {
                        if (params.isGroupId && params.isRecId) {
                            data = _.pick(data, 'current_rec_id', 'lon', 'lat', 'size', 'group_id');
                        } else if (params.isGroupId) {
                            data = _.pick(data, 'lon', 'lat', 'size', 'group_id');
                        } else if (params.isRecId) {
                            data = _.pick(data, 'current_rec_id', 'lon', 'lat', 'size');
                        } else {
                            data = _.pick(data, 'lon', 'lat', 'size');
                        }
                    } else {
                        if (params.isZip) {
                            if (params.isGroupId && params.isRecId) {
                                data = _.pick(data, 'current_rec_id', 'country', 'region', 'locality', 'size', 'group_id', 'zip');
                            } else if (params.isGroupId) {
                                data = _.pick(data, 'country', 'region', 'locality', 'size', 'group_id', 'zip');
                            } else if (params.isRecId) {
                                data = _.pick(data, 'current_rec_id', 'country', 'region', 'locality', 'size', 'zip');
                            } else {
                                data = _.pick(data, 'country', 'region', 'locality', 'size', 'zip');
                            }
                        } else {
                            if (params.isGroupId && params.isRecId) {
                                data = _.pick(data, 'current_rec_id', 'country', 'region', 'locality', 'size', 'group_id', 'neighbourhood');
                            } else if (params.isGroupId) {
                                data = _.pick(data, 'country', 'region', 'locality', 'size', 'group_id', 'neighbourhood');
                            } else if (params.isRecId) {
                                data = _.pick(data, 'current_rec_id', 'country', 'region', 'locality', 'size', 'neighbourhood');
                            } else {
                                data = _.pick(data, 'country', 'region', 'locality', 'size', 'neighbourhood');
                            }
                        }
                    }
                    var param = qs.stringify(data);
                    if (params.userId) {
                        return "/merchants/recommend/" + params.userId + "?" + param;
                    } else if (params.isReal) {
                        return "/merchants/recommend" + "?" + param;
                    } else {
                        return "/merchants/recommend/test" + "?" + param;
                    }
                }
            },
            history: {
                path: function(data) {
                    if (data.isRated && data.isEatLater) {
                        return "/merchants/action?rated=true&eat_later=true";
                    } else if (data.isRated) {
                        return "/merchants/action?=rated=true";
                    } else if (data.isEatLater) {
                        return "/merchants/action?eat_later=true";
                    }
                }
            }
        },

        clearDb: function(callback) {
            db.dropDatabase(callback);
        },

        clearCollection: function(collection, callback) {
            db[collection].remove(callback);
        }
    };
}())
