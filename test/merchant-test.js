(function() {
    'use strict';
    var should = require('should');
    var assert = require('assert');
    var request = require('supertest');
    var helper = require('./helper');

    describe('Routing', function() {
        //to perform merchant test
        describe('Merchant', function() {
            it('should return dishes by merchantId', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.dishes.path(merchantId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return merchant dishes by merchantId and dishId', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.mdishes.path(merchantId, dishId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return merchant dishes by merchantId', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.mdishes.path(merchantId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return merchant dishes by dishId', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.mdishes.path(null, dishId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return merchant hours', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.hourStatus.path(merchantId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return merchant hours', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.hours.path(merchantId))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return test recommendation', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: true,
                        isGroupId: false,
                        isRecId: false,
                        isReal: false
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return real recommendation by lat lon', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: true,
                        isGroupId: false,
                        isRecId: false,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by lat lon with current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: true,
                        isGroupId: false,
                        isRecId: true,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by lat lon with group id and current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: true,
                        isGroupId: true,
                        isRecId: true,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

            it('should return real recommendation by neighbourhood', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: false,
                        isRecId: false,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by neighbourhood with current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: false,
                        isRecId: true,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by neighbourhood with group id and current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: true,
                        isRecId: true,
                        isReal: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by zip', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: false,
                        isRecId: false,
                        isReal: true,
                        isZip: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by zip with current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: false,
                        isRecId: true,
                        isReal: true,
                        isZip: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });


            it('should return real recommendation by zip with group id and current rec id', function(done) {
                request(helper.serviceUrl)
                    .get(helper.merchants.recommendation.path({
                        isLatLon: false,
                        isGroupId: true,
                        isRecId: true,
                        isReal: true,
                        isZip: true
                    }))
                    .set({
                        Authorization: Authorization
                    })
                    .expect(200)
                    .end(function(err, res) {
                        res.body.should.not.equal(null);
                        done();
                    });
            });

        });
    });
}());
