/**
 * Created by hngre on 9/3/15.
 */

var User = require('./../models/user');
var Group = require('./../models/group');
var Rating = require('./../models/rating');
var Device = require('./../models/device');
var userAuth = require('../operations/auth/user-authentication');
var recommendation = require('../operations/recommendation/v2');
var account = require('../operations/account-merge');
var unauthorized = require('../operations/unAuthorized');
var common = require('../operations/api-common');
var pushios = require('../Push/push-ios');
var Boom = require('boom');
var winston = require('../logger');
var constants = require('../constants');
var async = require('async');
var multiparty = require('multiparty');
var fs = require('fs');
var Imager = require('imager');
var imagerConfig = require('../imager');
var invite = require('../operations/invitation/invite-resolve');
var fullcontact = require('../operations/external-apis/fullcontact');
var geoip = require('geoip-lite');
var ObjectId = require('mongoose').Types.ObjectId;

module.exports = {

    login: function(request, reply) {
        //test
        try {
            var ip = request.headers['X-Forwarded-For'] || request.headers['x-forwarded-for'] || request.info.remoteAddress;
            winston.info('ip address %s', ip);
            var geo = geoip.lookup(ip);
            winston.info(geo);
        } catch (err) {}

        var loginParams = request.payload;

        if (request.auth.credentials) loginParams._id = request.auth.credentials.user_id;

        loginParams.agent = request.plugins.scooter.toJSON();
        var _agent = request.headers['user-agent'];
        if (_agent && ~_agent.indexOf('HngreConsumer')) {
            //HngreConsumer/1.2.1 (iPhone; iOS 9.2.1; Scale/2.00)            
            var index1 = _agent.indexOf('/');
            if (~index1) {
                var index2 = _agent.indexOf(' ');
                if (~index2) {
                    var appVersion = _agent.substring(index1 + 1, index2);
                    appVersion = appVersion.split('.');
                    if (appVersion.length === 3) {
                        loginParams.agent.major = appVersion[0];
                        loginParams.agent.minor = appVersion[1];
                        loginParams.agent.patch = appVersion[2];
                    }
                }
            }
        }
        //console.log('agent',_agent, loginParams.agent);
        userAuth.authenticateUser(loginParams, function(err, token, user) {

            if (err) {
                reply(Boom.unauthorized(err));
                winston.error('Error Authenticating user', {
                    user: request.payload,
                    error: err.message
                });
                unauthorized.logUnauthorized(request, Boom.unauthorized(err));
            } else {
                reply({
                    token: token,
                    user: user
                });

                User.findOne({
                    _id: user._id
                }, function(err, user) {
                    if (err) {
                        winston.error('Error in user fetching', err);
                        return err;
                    }
                    if (user.full_contacts) {
                        return;
                    }
                    fullcontact.getContactsByEmail(user.email, function(err, data) {
                        if (err) {
                            winston.error('Error in get contacts by email using fullcontact api', err);
                            return err;
                        }
                        if (!data) return;
                        user.full_contacts = data;
                        user.save(function(err) {
                            if (err) {
                                winston.error('Error in full contacts user saving', err);
                                return err;
                            }
                        });
                    });
                });

                //to test invite resolve
                var inviteId = request.payload.inviteId;
                if (inviteId) {
                    invite.resolve(user._id, inviteId);
                }
            }
        })
    },

    logout: function(request, reply) {
        var deviceId = request.params.device_id;

        winston.info('Logging out from device id ', deviceId);

        if (!request.auth || !request.auth.credentials || !request.auth.credentials.user_id) {
            return reply({
                success: true
            });
        }

        var userId = request.auth.credentials.user_id;

        User.findOne({
            _id: userId
        }, function(err, user) {

            if (err) return reply({
                success: true
            });

            var deviceObject = {
                identifier: deviceId,
                active: false,
                last_seen: Date.now()
            };
            winston.info(user);
            if (!user) {
                return reply({
                    success: true
                });
            }
            user.removeOrUpdateDevice(deviceObject, function(err) {

                if (err) return reply({
                    success: true
                });

                reply({
                    success: true
                });
            });
        });
    },

    connectAccount: function(request, reply) {

        //TODO create mechanism

    },

    profile: function(request, reply) {

        var userId;
        if (request.params.userId === 'me' && request.auth.credentials) {
            userId = request.auth.credentials.user_id
        } else {
            userId = request.params.userId;
        }

        if (userId) {
            User.findById(userId, function(err, user) {

                if (err) {
                    winston.error('Error getting user profile ', JSON.stringify({
                        user: userId,
                        error: err
                    }));
                    reply(err);
                } else {
                    if (user) {
                        common.setDataForUserProfile(user, function(err, returnUser) {

                            if (err) {
                                reply(Boom.badImplementation(err.message))
                            } else {
                                reply(returnUser);
                            }
                        });
                    } else {
                        reply(Boom.notFound('User does not exist'));
                    }
                }
            });
        } else {
            reply(Boom.badRequest('User ID not found'));
        }
    },

    editProfile: function(request, reply) {


        User.findById(request.params.userId, function(err, user) {

            if (err) {
                console.log(err);
                winston.error('(UPDATE) Error finding user profile:', {
                    user: request.auth.credentials.user_id,
                    error: err
                });
                return reply(Boom.badImplementation(err.message))
            }

            if (user) {
                winston.log('Before edit', user);
                var editedFields = _.pick(request.payload, 'birthday', 'gender', 'veg_scale', 'alohar_id', 'mixpanel_id', 'website', 'website_display_name', 'first_name', 'first_name', 'bio');
                if (editedFields.birthday) {
                    editedFields.birthday_source = "user";
                }

                if (editedFields.website && editedFields.website.trim() && (editedFields.website.trim().indexOf('http://') !== 0 || editedFields.website.trim().indexOf('https://') !== 0)) {
                    editedFields.website = 'http://' + editedFields.website.trim();
                }

                _.extend(user, editedFields);
                winston.log('After edit', user);

                user.save(function(err, updatedUser) {

                    if (err) {
                        winston.error('Error updating user profile', user);
                        return reply(Boom.badImplementation(err.message));
                    }

                    var returnUser = common.setDataForUserProfile(updatedUser);
                    reply(returnUser);
                });
            } else {
                reply(Boom.notFound('User does not exist'));
            }
        });
    },

    addDevice: function(request, reply) {

        var userId = request.auth.credentials.user_id;
        var deviceId = request.params.device_id;
        var apnToken = request.payload.apn_token;
        var dcmToken = request.payload.gcm_token;

        winston.info('Registering device id ', deviceId, apnToken, dcmToken);

        User.findOne({
            _id: userId
        }, function(err, user) {

            if (err) return reply(err);

            var deviceObject = {
                identifier: deviceId,
                agent: request.plugins.scooter.toJSON(),
                active: true
            };

            if (apnToken) {
                deviceObject.apn_notification = {
                    token: apnToken,
                    active: true
                }
            }
            if (dcmToken) {
                deviceObject.gcm_notification = {
                    token: dcmToken,
                    active: true
                }
            }

            user.addOrUpdateDevice(deviceObject, function(err) {

                if (err) return reply(err);

                reply({
                    success: true
                });
            });
        });
    },

    getPhoto: function(request, reply) {

        var userId = request.params.userId;

        User.findById(userId, function(err, user) {

            if (err) return reply(err);

            winston.info(user);

            if (user.profile_image) {
                reply({
                    url: user.profile_image
                });
            } else {
                reply(Boom.notFound('User image does not exist'));
            }
        });
    },

    getHngreIndex: function(request, reply) {
        var userId = request.params.userId || request.auth.credentials.user_id;
        var location = request.query.location;
        async.parallel([getUser, getRatingCount], function(err, results) {
            if (err) {
                winston.error(err);
                reply(err);
            }
            var user = results[0];
            var ratingCount = results[1] > 0 ? results[1] : 1;
            var isSignedIn = request.auth.credentials.user_id ? true : false;
            var isRated10 = ratingCount > 9;
            var suggestions = [{
                text: "age",
                value: isSignedIn
            }, {
                text: "gender",
                value: isSignedIn
            }, {
                text: "foodPreferences",
                value: isSignedIn
            }, {
                text: "location",
                value: location
            }, {
                text: "signedIn",
                value: isSignedIn
            }, {
                text: "ratedTenDishes",
                value: isRated10
            }];

            var genderPoint = isSignedIn ? 20 : 0,
                birthdayPoint = isSignedIn ? 20 : 0,
                locationPoint = location === true ? 20 : 0;
            var rate_count = Math.max(0, Math.min((Math.log10(ratingCount) / Math.log10(250)) * 118, 118));
            var index = (genderPoint + birthdayPoint + locationPoint + rate_count) / 180;
            var level = "";
            if (index < 0.33) {
                level = 'Low';
            } else if (index < 0.6) {
                level = 'Average';
            } else if (index < 0.794) {
                level = 'Good';
            } else if (index < 0.876) {
                level = 'High';
            } else {
                level = 'Very High';
            }

            reply({
                level: level,
                hngreIndex: index,
                suggestions: suggestions
            });

        });

        function getUser(callback) {
            User.findById(userId, function(err, user) {
                if (err) return reply(err);
                winston.info(user);
                callback(null, user);
            });
        }

        function getRatingCount(callback) {
            Rating.count({
                _user: userId,
                rating:{
                    $exists: true
                }
            }, function(err, count) {
                if (err) {
                    return reply(err);
                }
                callback(null, count);
            });
        }
    },
    uploadImage: function(request, reply) {

        var userId = request.auth.credentials.user_id;
        var form = new multiparty.Form();
        form.parse(request.payload, function(err, fields, files) {
            if (err || !files) {
                return reply(err);
            }
            winston.info(files);
            var images = files.file ? files.file : undefined;
            imagerConfig.storage.uploadDirectory = constants.cloudfront.user.folder;
            var imager = new Imager(imagerConfig, 'S3');

            imager.upload(images, function(err, cdnUri, _files) {
                if (err) return reply(err);
                var image = cdnUri;
                if (_files.length) {
                    image = constants.cloudfront.host + constants.cloudfront.user.folder + constants.cloudfront.user.prefix + _files[0];
                } else {
                    image = constants.cloudfront.host + constants.cloudfront.user.folder + constants.cloudfront.user.prefix + _files;
                }
                User.findOne({
                    _id: userId
                }, function(err, user) {
                    if (err) {
                        winston.error(err);
                        return reply(err);
                    }
                    if (!user) {
                        unauthorized.logUnauthorized(request, Boom.unauthorized('Unauthorized user'));
                        return reply(Boom.unauthorized('Unauthorized user'));
                    }
                    user.profile_image = image;
                    user.save(function(err) {
                        if (err) {
                            winston.error(err);
                            return reply(err);
                        }
                        reply({
                            url: image
                        });
                        console.log(images[0].path);
                        fs.unlinkSync(images[0].path);
                    });
                });
            }, 'user');
        });
    },
    mergeAccount: function(request, reply) {
        var hngre_generated_id = request.params.hngre_generated_id;
        var payload = request.payload;
        account.merge(hngre_generated_id, payload, function(err, result) {
            if (err) {
                console.log(err);
                return reply(err);
            }
            reply(result);
        });
    },
    getBranchLink: function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var query = request.query;
        if (!userId) {
            return reply(Boom.unauthorized());
        }

        User.findOne({
            _id: userId
        }, function(err, user) {
            if (err) {
                console.log(err);
            }
            if (!user) {
                return reply(Boom.unauthorized());
            }
            var branchObj = {
                sdk: "api",
                feature: "invite",
                channel: query.channel,
                branch_key: "key_live_llaHDxYLT3Zwwewt353WokdcByci9bC2",
                data: {
                    hngre_inviting_user_image_url: user.profile_image,
                    hngre_inviting_user_short_name: user.first_name,
                    hngre_inviting_user_id: userId,
                    hngre_inviting_full_name: user.name
                }
            };

            var options = {
                uri: 'https://api.branch.io/v1/url',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: branchObj,
                method: "post",
                json: true
            };
            var req = require("request");
            req(options, function(err, response, body) {
                if (err) {
                    console.log(err);
                    return reply(Boom.badImplementation());
                } else {
                    return reply(body);
                }
            });
        });
    },
    dishesDiscovered: function(request, reply) {

        var contributorId = request.params.contributor_id;

        var userId = request.auth.credentials.user_id;

        var query = request.query;
        if (!userId) {
            unauthorized.logUnauthorized(request, Boom.unauthorized("unAuthorized user"));
            return reply(Boom.unauthorized());
        }
        var groupId = query.group_id;
        var userIds = [ObjectId(userId)];
        Group.findOne({
            _id: groupId,
            "members.userId": userId
        }, function(err, group) {
            if (err) {
                winston.error('Group %s not found', groupId);
            }
            if (group && group.members && group.members.length > 0) {
                userIds = _.pluck(group.members, "userId");
                userIds = _.filter(userIds, function(user) {
                    if (user) return true;
                    else return false;
                });
            }
            query.userId = userId;
            query.userIds = userIds;
            winston.info("Fetching contributor dishes recommendation for users", userIds);
            winston.info("Fetching contributor dishes recommendation with query", query);
            recommendation.byContributorId(contributorId, userIds, query, function(err, results) {
                if (err) {
                    if (err.message == 'Unauthorized user') {
                        unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                        return reply(Boom.unauthorized(err.message));
                    }
                    return reply(err);
                }
                results.merchants = _.shuffle(results.merchants);
                results.merchants = results.merchants.map(function(m) {
                    winston.info("Merchant Id : ", m._id, " Merchant Name : ", m.name);
                    m.dishes = m.dishes.map(function(d) {
                        winston.info("Dish Id : ", d._id, " Dish Name : ", d.name);
                        winston.info("Dish Scores : ", d.scores);
                        if (!d.scores) {
                            d.scores = {};
                        }
                        return d;
                    });
                    return m;
                });
                reply(results);
            });
        });
    }
};
