/**
 * Created by hngre on 9/3/15.
 */

var Merchant = require('../models/merchant.js');
var Dish = require('../models/dish');
var User = require('../models/user');
var Rating = require('../models/rating');
var Interaction = require('../models/interaction');
var Recommend = require('../operations/recommendation/recommender');
var DishSource = require('../operations/elasticsearch-common');
var AddUserToRec = require('../operations/recommendation/rec-add-user');
var DishResponse = require('../operations/dish-response');
var notification = require('../operations/notifications/notifications');
var Search = require('../operations/search/search-elastic');
var HngreMailer = require('../operations/email');
var constants = require('../constants');
var error = require('../error');
var unauthorized = require('../operations/unAuthorized');

var winston = require('../logger');
var Boom = require('boom');
var Async = require('async');

module.exports = {

    findById: function(request, reply) {
        winston.info('Request for dish id %s', request.params.dishId);

        DishSource.fetchDishes([request.params.dish_id], function(err, dishes) {

            var params = {};
            if (request.params.merchant_id) params.merchantId = request.params.merchant_id;

            DishResponse.prepare(DishResponse.type.DISH, dishes, params, function(err, result) {

                if (err) return reply(err);
                else {
                    reply(null, result);
                }
            });
        });
    },

    findByMerchant: function(request, reply) {

        winston.info('Requested dishes for merchant id %s', request.params.merchantId);

        var query = {
            index: "hngre",
            type: "dishes",
            body: {
                query: {
                    terms: {
                        'merchant._id': [request.params.merchantId]
                    }
                }
            }
        };

        var client = DishSource.client();

        client.search(query, function(err, result) {

            if (err) {
                reply(err);
                return error.report(err, query);
            } else {
                var returnDishes = [];

                if (result.hits.hits.length > 0) {
                    returnDishes = DishSource.parseDishDocument(result.hits.hits);
                }

                var params = {
                    merchantId: request.params.merchantId
                };

                try {
                    params.userId = request.auth.credentials.user_id;
                } catch (err) { /*No problem*/ }

                DishResponse.prepare(DishResponse.type.RESTAURANT, returnDishes, params, function(err, formattedDishes) {

                    if (err) return reply(err);

                    reply(formattedDishes.dishes);
                });

            }
        });

    },

    recommend: function(request, reply) {

        var userId = request.auth.credentials.user_id;

        if (userId) {
            winston.info('Fetching recommendations for user id %s', userId, request.query);

            Async.parallel([

                function(callback) {

                    Recommend(userId, request.query, function(err, recommendations) {

                        if (err) {
                            winston.error('Error while generating recommendation', err);
                            callback(err);
                        } else {
                            callback(null, recommendations);
                        }
                    });
                },

                function(callback) {

                    Search.merchantAtLocation(request.query, function(err, result) {

                        if (err) return callback(null, null);

                        callback(null, result);
                    })
                }

            ], function(err, results) {

                if (err) {
                    if (err.message === 'Unauthorized user') {
                        unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                        return reply(Boom.unauthorized(err.message));
                    }

                    var errorMail = new HngreMailer('internal');
                    errorMail.to = 'akash.gupta@hngre.com, partinder@hngre.com, rohit@hngre.com';
                    errorMail.subject = 'Error sending recommendations';
                    errorMail.text = 'User:', request.auth.credentials.user_id, '\nQuery:', request.query, '\n\nError:', err;

                    return reply(Boom.badRequest(err.message));
                }

                var finalReply = results[0];

                //Check for matches and attach them
                if (results[1]) finalReply.matches = results[1];

                reply(finalReply);
            });
        } else {
            unauthorized.logUnauthorized(request, Boom.unauthorized("Unauthorized user"));
            reply(Boom.unauthorized());
        }
    },

    //Post Action for Dish and Merchant
    takeAction: function(request, reply) {

        var dishId = request.params.dish_id;
        var merchantId = request.params.merchant_id;
        var userId;

        try {
            userId = request.auth.credentials.user_id;
        } catch (err) {
            userId = request.payload.userId;
        }

        if (!userId) return reply(Boom.badRequest('Missing UserID'));

        var rateObj = request.payload;
        rateObj.user = userId;
        rateObj.dish = dishId;
        rateObj.merchant = merchantId;

        winston.info('Rating dish', rateObj.dish);

        var callback = function(err, rating) {

            if (err) {
                winston.error('Rating error', err);
                reply(Boom.badImplementation(err.message));
            } else {
                winston.info('Rating from database', rating._id);
                // notification testing purpuse
                /*                User.findOne({
                                    _id: userId
                                }, function(err, user) {
                                    if (err) {
                                        winston.error(err);
                                    }
                                    if (user) {
                                        var data = {
                                            event: constants.rate_dish.event,
                                            title: constants.rate_dish.setTitle(),
                                            body: constants.rate_dish.setBody(dishId, rating.rating),
                                            path: constants.default.setDishPath(dishId, merchantId, constants.rate_dish.setTitle()),
                                            initiator: {
                                                from: constants.rate_dish.initiator.from,
                                                type: constants.rate_dish.initiator.type
                                            },
                                            to: constants.rate_dish.to,
                                            sender: {
                                                first_name: user.first_name,
                                                icon: user.profile_image
                                            }
                                        };
                                        notification.generateNotifications([userId], data, function(err) {
                                            winston.error(err);
                                        });
                                    }
                                })*/
                // end
                var responseObj = {
                    success: true
                };

                if (rating) responseObj.rating = rating.rating;

                reply(responseObj);

                AddUserToRec(userId);
            }
        };

        if (request.payload.hasOwnProperty('rating')) {
            var rating = request.payload.rating;
            Rating.rateDish(userId, dishId, merchantId, rating, callback);
            /*            Interaction.event(Interaction.type.RATED, userId, {
                            dish: dishId,
                            merchant: merchantId,
                            rating: rating
                        });*/
        } else if (request.payload.hasOwnProperty('not_for_me')) {
            var notForMe = request.payload.not_for_me;
            Rating.notForMe(userId, dishId, merchantId, notForMe, callback);
        } else if (request.payload.hasOwnProperty('eat_later')) {
            var eatLater = request.payload.eat_later;
            Rating.eatLater(userId, dishId, merchantId, eatLater, callback);

            /*            if (eatLater) {
                            Interaction.event(Interaction.type.ADDEDTOWISHLIST, userId, {
                                dish: dishId,
                                merchant: merchantId
                            });
                        } else {
                            Interaction.event(Interaction.type.REMOVEDFROMWISHLIST, userId, {
                                dish: dishId,
                                merchant: merchantId
                            });
                        }*/
        } else {
            reply(Boom.badRequest('Invalid payload'));
        }
    },

    //Get dishes on which actions have been taken by a user
    getActions: function(request, reply) {

        var userId;
        try {
            userId = request.auth.credentials.user_id
        } catch (err) {
            userId = request.query.userId;
        }

        if (!userId) {
            return reply(Boom.forbidden('Missing authentication'));
        }

        var queries = [];
        var shouldPopulatePredictions = false;

        if (request.query.rated) {
            queries.push({
                rating: {
                    $ne: null
                }
            });
        }

        if (request.query.eat_later) {
            queries.push({
                eat_later: true
            });
            shouldPopulatePredictions = true;
        }

        var query = {
            _user: userId
        };

        if (queries.length > 0) {
            _.extend(query, {
                $or: queries
            });
        }
        winston.info('query', query);

        Rating.find(query, function(err, ratings) {

            if (err) {
                return reply(err);
            } else {
                var dishIds = []; //List of dishIds to be fetched from the elasticsearch server.

                ratings.forEach(function(rating) {
                    var dishId = rating._dish.toString();
                    dishIds.push(dishId);
                });

                if (dishIds.length > 0) {
                    DishSource.fetchDishes(dishIds, function(err, dishes) {

                        if (err) return reply(err);
                        else {
                            var params = {
                                userId: userId,
                                shouldPopulatePredictions: shouldPopulatePredictions,
                                history: ratings
                            };

                            DishResponse.prepare(DishResponse.type.WISHLIST, dishes, params, function(err, returnData) {

                                if (err) return reply(err);

                                reply(returnData.dishes);
                            });
                        }
                    });
                } else {
                    reply([]);
                }
            }
        });
    },

    clearActions: function(request, reply) {

        var userId;

        try {
            userId = request.auth.credentials.user_id
        } catch (err) {
            userId = request.query.userId;
        }

        if (!userId) {
            return reply(Boom.forbidden('Missing authentication'));
        }

        var query = {
            _user: userId
        };

        if (request.query.dish_id) query._dish = request.query.dish_id;
        if (request.query.merchant_id) query._merchant = request.query.merchant_id;

        if (request.query.rated) query.rating = {
            $exists: true
        };
        if (request.query.eat_later) query.eat_later = true;

        winston.info('query', query);

        Rating.remove(query, function(err, result) {

            if (err) {
                return reply(err);
            } else {
                winston.info('user id %s', userId, result);
                reply({
                    success: true
                });

                /*                if (!query.dish_id && !query.merchant_id) {
                                    if (query.rating) {
                                        Interaction.event(Interaction.type.REMOVEDFROMHISTORY, query._user, {
                                            dish: query._dish,
                                            merchant: query._merchant
                                        });
                                    } else if (query.eat_later) {
                                        Interaction.event(Interaction.type.REMOVEDFROMWISHLIST, query._user, {
                                            dish: query._dish,
                                            merchant: query._merchant
                                        });
                                    } else {

                                    }
                                } else {
                                    if (query.rating) {
                                        Interaction.event(Interaction.type.CLEARHISTORY, query._user);
                                    } else if (query.eat_later) {
                                        Interaction.event(Interaction.type.CLEARWISHLIST, query._user);
                                    } else {

                                    }
                                }*/
            }
        });
    },

    sampleRecommendations: function(request, reply) {

        var dishIds = [
            "5550f98d79db64c37ca4cecf",
            "5550be0779db64c37ca4ce5a",
            "5550abcc55763eb25808513b",
            "554878c4a7f099eb400d67bd",
            "554c776455763eb258084fe1",
            "552f6db9acbf94330cfc785e",
            "554b72ec55763eb258084f4d",
            "554b00a5a7f099eb400d6854",
            "554db6a255763eb258085045",
            "5526883f63e70ed5046f586e",
            "555063cd55763eb2580850e1",
            "5550692555763eb2580850ec",
            "552d1be6f2f613f72e14e23c",
            "555100a279db64c37ca4cee1",
            "5538730eacbf94330cfc7982",
            "553f30c3acbf94330cfc7a00",
            "554df69155763eb25808508f",
            "5523ef844fd0398e5d437785",
            "5548d687a7f099eb400d682e",
            "55372d8eacbf94330cfc78ef",
            "5524e3514fd0398e5d4377bb",
            "5550545255763eb2580850c8"
        ];

        DishSource.fetchDishes(dishIds, function(err, dishes) {

            dishes.forEach(function(dish) {
                //Assign a random score.
                dish.score = Math.round(Math.random() * (100 - 1) + 1);
            });

            if (err) return reply(err);

            reply(dishes);
        });
    }
}
