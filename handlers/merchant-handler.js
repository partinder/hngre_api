/**
 * Created by hngre on 9/3/15.
 */

var common = require('../operations/api-common');
var prediction = require('../operations/prediction');
var elasticsearchCommon = require('../operations/elasticsearch-common');
var MerchantResponse = require('../operations/merchant-response');
var Merchant = require('../models/merchant');
var Predictions = require('../models/predictions');
var PredictionMerchants = require('../models/prediction_merchants');
var Recommendation = require('../models/recommendation');
var User = require('../models/user');
var Group = require('../models/group');
var Foursquare = require('../operations/external-apis/foursquare-connect');
var Boom = require('boom');
var winston = require('../logger');
var ObjectId = require('mongoose').Types.ObjectId;
var constants = require('../constants');
var error = require('../error');
var unauthorized = require('../operations/unAuthorized');

module.exports = {

    findById: function(request, reply) {

        var merchantId = encodeURIComponent(request.params.merchantId);
        var query = Merchant.findOne({
            _id: merchantId
        });
        query.lean();
        query.exec(function(err, merchant) {

            if (err) {

                winston.error('Error fetching merchant', {
                    id: merchantId
                });
                reply(Boom.badImplementation(err.message));

            } else {

                if (merchant) {

                    var returnMerchant = common.setDataForUserProfile(merchant);
                    reply(returnMerchant);

                } else {

                    reply(Boom.notFound('Merchant does not exist'));
                }
            }
        });
    },

    findByMerchantDishes: function(request, reply) {
        winston.info('Requested dishes for merchant id %s', request.query.merchantId);
        winston.info('Requested dish for dish id %s', request.query.dishId);

        var query = {
            index: "hngre",
            type: "merchants",
            body: {
                query: {
                    terms: {}
                }
            }
        };

        if (request.query.merchantId) {
            query.body.query.terms._id = [request.query.merchantId];
        } else {
            query.body = {
                size: 1,
                query: {
                    nested: {
                        path: "dishes",
                        query: {
                            match: {
                                "dishes._id": {
                                    query: request.query.dishId
                                }
                            }
                        },
                        inner_hits: {}
                    }
                }
            };
        }
        winston.info(query);
        var client = elasticsearchCommon.client();

        client.search(query, function(err, result) {

            if (err) {
                reply(err);
                return error.report(err, query);
            } else {
                var returnDishes = [];

                if (result.hits.hits.length > 0) {
                    returnDishes = elasticsearchCommon.parseDishDocument(result.hits.hits);
                }
                if (!request.query.merchantId) {
                    request.query.merchantId = returnDishes._id;
                }
                if (request.query.dishId) {
                    if (returnDishes && returnDishes.length && returnDishes.length > 0) {
                        returnDishes[0].dishes = returnDishes[0].dishes.filter(function(dish) {
                            if (dish._id === request.query.dishId) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }
                }
                var params = {
                    merchantId: request.query.merchantId,
                    all: true
                };

                try {
                    params.userId = request.auth.credentials.user_id;
                    params.userIds = [request.auth.credentials.user_id];
                } catch (err) { /*No problem*/ }

                MerchantResponse.prepare(MerchantResponse.type.RESTAURANT, returnDishes, params, function(err, result) {

                    if (err) return reply(err);
                    result.merchants = result.merchants.map(function(m) {
                        m.dishes = m.dishes.map(function(d) {
                            if (d.scores) {
                                delete d.scores;
                            }
                            return d;
                        });
                        return m;
                    });
                    if (request.query.dishId) {
                        reply(result.merchants);
                    } else if (request.query.merchantId) {
                        reply(result);
                    }
                });

            }
        });
    },

    getHours: function(request, reply) {

        var merchantId = encodeURIComponent(request.params.merchantId);
        var query = Merchant.findOne({
            _id: merchantId
        });
        query.lean();
        query.exec(function(err, merchant) {

            if (err) {

                winston.error('Error fetching merchant', {
                    id: merchantId
                });
                reply(Boom.badImplementation(err.message));

            } else {

                if (merchant) {

                    reply(merchant.hours);

                } else {

                    reply(Boom.notFound('Merchant does not exist'));
                }
            }
        });
    },
    getHoursStatus: function(request, reply) {
        var merchantId = encodeURIComponent(request.params.merchantId);
        var query = Merchant.findOne({
            _id: merchantId
        });
        query.lean();
        query.exec(function(err, merchant) {

            if (err) {

                winston.error('Error fetching merchant', {
                    id: merchantId
                });
                reply(Boom.badImplementation(err.message));

            } else {

                if (merchant) {
                    var status = common.merchantOpenStatus(merchant);
                    reply({
                        is_open: status
                    });

                } else {

                    reply(Boom.notFound('Merchant does not exist'));
                }
            }
        });
    },

    recommend1: function(request, reply) {
        var userId = request.auth.credentials.user_id;

        var query = request.query;
        if (!userId) {
            unauthorized.logUnauthorized(request, Boom.unauthorized("Unauthorized user"));
            return reply(Boom.unAuthorized());
        }
        if (!query) return reply(Boom.badRequest(new Error("query suppose to be non-empty")));
        var groupId = query.group_id;
        var userIds = [ObjectId(userId)];
        Group.findOne({
            _id: groupId,
            "members.userId": userId
        }, function(err, group) {
            if (err) {
                winston.error('Group %s not found', groupId);
            }
            if (group && group.members && group.members.length > 0) {
                userIds = _.pluck(group.members, "userId");
                userIds = _.filter(userIds, function(user) {
                    if (user) return true;
                    else return false;
                });
            }

            query.userId = userId;
            query.range = {
                min: constants.range.min,
                max: constants.range.max
            };
            query.distance = {
                min: constants.distance.min,
                max: constants.distance.max
            };
            winston.info("Fetching recommendation for users", userIds);
            winston.info("Fetching recommendation with query", query);
            prediction.getUsersByIds(userIds, function(err, users) {
                if (err) {
                    winston.error(err);
                    return reply(Boom.badRequest(err));
                }
                //console.log(users);
                if (users && users.length && users.length > 1) {
                    query.users = users;
                    prediction.getMerchantsByRangeAndDistance(query, function(err, merchants) {
                        if (err) {
                            winston.error(err);
                            return reply(Boom.badRequest(err));
                        }
                        return reply(merchants);
                    });
                } else if (users && users.length && users.length === 1) {
                    query.user = users[0];
                    prediction.getDishesByRangeAndDistance(query, function(err, merchants) {
                        if (err) {
                            winston.error(err);
                            return reply(Boom.badRequest(err));
                        }
                        return reply(merchants);
                    });
                } else {
                    winston.info("Failed to load user");
                }

            });
        });
    }
}
