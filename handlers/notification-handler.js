(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Notification = require('../models/notification');
    var winston = require('../logger');
    var User = require('../models/user');
    var config = require('../config');
    var req = require('request');
    var async = require('async');
    var unauthorized = require('../operations/unAuthorized');
    /**
     * Get top notifications by time series
     *
     * @method     getAll
     * @param      {<type>}  request  { description }
     * @param      {<type>}  reply    { description }
     */
    exports.getAll = function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var query = request.query;
        var options = {
            criteria: {
                user: userId
            },
            pages: {}
        }
        if (query) {
            options.pages.skip = parseInt(query.page);
            options.pages.limit = parseInt(query.perPage);
        }
        Notification.loadAll(options, function(err, notifications) {
            if (err) {
                winston.error('Notification loading error', err);
                return reply(err);
            }
            reply(notifications);
            var notification_ids = _.pluck(notifications, '_id');
            options = {
                criteria: {
                    _id: {
                        $in: notification_ids
                    },
                    user: userId
                }
            };
            Notification.markAsViewed(options, function(err, result) {
                if (err) {
                    winston.error('Notification updating mark As viewed error', err);
                }
                winston.info(result);
            });
        })
    };

    /**
     * Get top notifications counts by time series
     *
     * @method     getCounts
     * @param      {<type>}  request  { description }
     * @param      {<type>}  reply    { description }
     */
    exports.getCounts = function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var query = request.query;
        var options = {
            criteria: {
                user: userId
            }
        };

        Notification.loadCount(options, function(err, count) {
            if (err) {
                winston.error('Notification count loading error', err);
                return reply(err);
            }
            reply({
                latest: count
            });
        })
    };

    /**
     * Get the notification by notification id
     *
     * @method     get
     * @param      {<type>}    request  { description }
     * @param      {Function}  reply    { description }
     * @return     {<type>}    { description_of_the_return_value }
     */
    exports.get = function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var notificationId = request.params.notification_id;
        var condition = {
            user: userId,
            _id: notificationId
        };
        Notification.load(condition, function(err, notification) {
            if (err) {
                winston.error('Notification loading error', err);
                return reply(err);
            }
            return reply(notification);
        })
    };
    /**
     * Mark the notification as unread(but viewed)
     *
     * @method     markAsUnread
     * @param      {<type>}    request  { description }
     * @param      {Function}  reply    { description }
     */
    exports.markAsViewed = function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var notification_ids = request.payload.notification_ids ? request.payload.notification_ids : [];
        var options = {
            criteria: {
                _id: {
                    $in: notification_ids
                },
                user: userId
            }
        };
        Notification.markAsViewed(options, function(err, result) {
            if (err) {
                winston.error('Notification updating mark As unread error', err);
                return reply(err);
            }
            return reply({
                success: true
            });
        });
    };
    /**
     * Mark the notification as read
     *
     * @method     markAsRead
     * @param      {<type>}    request  { description }
     * @param      {Function}  reply    { description }
     */
    exports.markAsRead = function(request, reply) {
        var userId = request.auth.credentials.user_id;
        var notificationId = request.params.notification_id;
        var options = {
            criteria: {
                _id: notificationId,
                user: userId
            }
        };
        Notification.markAsRead(options, function(err, result) {
            if (err) {
                winston.error('Notification updating mark as read error', err);
                return reply(err);
            }
            return reply({
                success: true
            });
        });
    };
    /**
     * Create a new notification
     *
     * @method     create
     * @param      {<type>}  request  { description }
     * @param      {<type>}  reply    { description }
     */
    exports.create = function(request, reply) {
        var payload = request.payload;
        payload = _.pick(payload, 'user', 'title', 'body', 'path', 'icon');
        var notification = new Notification(payload);
        notification.save(function(err) {
            if (err) {
                winston.error('Notification creating error ', err);
                return reply(err);
            }
            return reply(notification);
        });
    };

    /**
     * { function_description }
     *
     * @method     generateNotifications
     * @param      {<type>}    userIds  { description }
     * @param      {<type>}    data     { description }
     * @param      {Function}  cb       { description }
     * @return     {<type>}    { description_of_the_return_value }
     */
    exports.generateNotifications = function(request, reply) {
        var userId = request.payload.userId;
        var data = request.payload.data;
        console.log(data);
        winston.info(userId, data);
        User.findOne({
            _id: userId
        }, function(err, user) {
            if (err) {
                winston.error(err);
                return reply(err);
            }
            var notification = new Notification(_.extend({
                user: user._id
            }, data));
            notification.save(function(err) {
                if (err) {
                    winston.error('Notification creating error ', err);
                    return reply(new Error('failed to save notification'));
                }
                var options = {
                    criteria: {
                        user: user._id
                    }
                };
                Notification.loadCount(options, function(err, count) {
                    if (err && count == 0) {
                        winston.error('Notification count loading error', err);
                        count = 1;
                    }
                    console.log(notification.to);
                    if (notification.to === "inapp") return reply({
                        inapp_track_id: notification._id
                    });
                    var deviceIds = [];
                    var form = null;
                    winston.info('device id ', user.devices);
                    if (user.devices) {
                        user.devices.forEach(function(device) {
                            if (device.identifier && device.active && device.gcm_notification.active && device.gcm_notification.token) {
                                deviceIds.push(device.gcm_notification.token);
                            }
                        });
                        form = {
                            event: notification.event,
                            push: {
                                deviceIds: deviceIds,
                                title: notification.title,
                                body: notification.body,
                                badge: count,
                                data: {
                                    notification_id: notification._id.toString(),
                                    hngre_api_uri: notification.path
                                }
                            }
                        };
                    }

                    if (deviceIds && deviceIds.length == 0 && user.phone && user.phone_verified) {
                        form = {
                            event: notification.event,
                            sms: {
                                phone: user.phone,
                                body: notification.body
                            }
                        }
                    } else if (deviceIds && deviceIds.length == 0 && user.email && user.email_verified) {
                        form = {
                            event: notification.event,
                            email: {
                                email: user.email,
                                name: user.name,
                                path: notification.path
                            }
                        }
                    }
                    if (!form) {
                        return reply(new Error('No device gcm token, phone number, email id, so failed!!'));
                    }

                    winston.info('Sending Notifications');

                    winston.info(form);
                    req.post({
                        url: config.notifier.url,
                        form: form
                    }, function(err, response, body) {
                        if (err) {
                            winston.error('Error sending notification', err);
                            return reply(err);
                        }
                        var push_track_id = "";
                        try {
                            body = JSON.parse(body);
                            push_track_id = body.push_track_id;
                        } catch (err) {
                            push_track_id = body;
                        }
                        return reply({
                            inapp_track_id: notification._id.toString(),
                            push_track_id: push_track_id
                        });
                    });
                })
            });
        });
    };

}());