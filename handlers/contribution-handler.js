(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Contribution = require('../models/contribution');
    var User = require('../models/user');
    var Boom = require('boom');
    var multiparty = require('multiparty');
    var fs = require('fs');
    var Imager = require('imager');
    var imagerConfig = require('../imager');
    var config = require('../config');
    var constants = require('../constants');
    var utils = require('../lib/utils');
    var async = require('async');
    var winston = require('../logger');
    var ObjectId = require('mongoose').Types.ObjectId;
    var unauthorized = require('../operations/unAuthorized');

    /**
     * 
     * Upload image of the contribution
     *
     * @method     uploadImage
     * @param      {<type>}  request  { description }
     * @param      {<type>}  reply    { description }
     */
    exports.uploadImage = function(request, reply) {
        var merchantName = request.query.merchant_name;
        var dishName = request.query.dish_name;
        var location = request.query.location;
        var vegType = request.query.veg_type;
        var vegScale = request.query.veg_scale;
        var userId = request.auth.credentials.user_id;
        var form = new multiparty.Form();
        form.parse(request.payload, function(err, fields, files) {
            if (err || !files) {
                winston.error(err);
                return reply(err);
            }
            winston.info(files);
            reply({
                success: true,
                message: "Thank you!!"
            });
            var images = files.file && files.file.length > 0 ? [files.file[0]] : undefined;
            imagerConfig.storage.uploadDirectory = constants.cloudfront.contribution.folder;
            var imager = new Imager(imagerConfig, 'S3');
            imager.upload(images, function(err, cdnUri, _files) {
                if (err) return winston.error(err);
                var image = cdnUri;
                winston.info(cdnUri, _files);
                if (_files.length) {
                    image = constants.cloudfront.host + constants.cloudfront.contribution.folder + constants.cloudfront.contribution.prefix + _files[0];
                } else {
                    image = constants.cloudfront.host + constants.cloudfront.contribution.folder + constants.cloudfront.contribution.prefix + _files;
                }
                var contribution = new Contribution({
                    merchant_name: merchantName,
                    dish_name: dishName,
                    location: location,
                    veg_type: vegType,
                    veg_scale: vegScale,
                    userId: ObjectId(userId),
                    dish_url: image
                });
                contribution.save(function(err, doc) {
                    if (err) {
                        return winston.error(err);
                    }
                    winston.info(doc);
                    console.log(images[0].path);
                    fs.unlinkSync(images[0].path);
                });

            }, 'contribution');
        });
    };
}())
