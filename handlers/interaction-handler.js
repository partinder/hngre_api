/*
    INTERACTION HANDLER
 */

var Interaction = require('../models/interaction');
var User = require('../models/user');
var Boom = require('boom');
var winston = require('../logger');
var config = require('../config');
var ua = require('universal-analytics');
var Mixpanel = require('mixpanel');
var mixpanel = Mixpanel.init(config.mixpanel_analytics.token);
var unauthorized = require('../operations/unAuthorized');

module.exports = {

    trackInteraction: function(request, reply) {

        var userId = request.auth.credentials ? request.auth.credentials.user_id : undefined;
        var deviceId = request.payload.deviceId;
        var agent = request.headers['user-agent'];

        if (!userId && !deviceId) return reply(Boom.badRequest('Auth or Device ID is required'));

        var interactionEvent = request.payload.event;
        var interactionData = request.payload.properties || {};

        var possibleData = _.pick(request.payload, 'dishId', 'merchantId', 'phone', 'actionName');
        _.extend(interactionData, possibleData);

        switch (interactionEvent) {
            case 'call':
                {
                    interactionEvent = Interaction.type.MERCHANTCALL;
                }
                break;

            case 'map':
                {
                    interactionEvent = Interaction.type.GOTDIRECTIONS;
                }
                break;

            case 'open':
                {
                    interactionEvent = Interaction.type.APPOPENED;
                    console.log('User', userId, "Device Id", deviceId);
                    if (userId && deviceId) {
                        User.findOneAndUpdate({
                            _id: userId,
                            "devices.identifier": deviceId
                        }, {
                            $set: {
                                "devices.$.last_seen": Date.now()
                            }
                        }, function(err) {
                            winston.error(err);
                        });
                    }
                }
                break;

            case 'close':
                {
                    interactionEvent = Interaction.type.APPCLOSE;
                }
                break;

            case 'action':
                {
                    interactionEvent = Interaction.type.MERCHANTACTION;
                }
                break;

            default:
                {

                }
                break;
        }
        if (userId) {
            var visitor = ua(config.google_analytics.tracking_id, userId);
            visitor.event({
                ec: userId,
                ea: interactionEvent,
                el: interactionEvent,
                ev: interactionData
            }).send()
            User.findOne({ _id: userId }, function(err, user) {
                if (err) {
                    console.log(err);
                }
                if (user) {
                    mixpanel.people.set(deviceId, {
                        $first_name: user.first_name,
                        $last_name: user.last_name,
                        $name: user.name,
                        $deviceId: deviceId,
                        $userId: userId,
                        $email: user.email,
                        $veg_scale: user.veg_scale.join(', '),
                        $gender: user.gender,
                        $userAgent: agent
                    }, function(err1) {
                        if (err1) {
                            console.log(err1);
                        }
                        if (interactionEvent == "Log in") {
                            user.mixpanel_server_id = "";
                        }
                        interactionData.distinct_id = deviceId;
                        mixpanel.track(interactionEvent, interactionData);
                    });
                } else {
                    mixpanel.people.set(deviceId, {
                        $deviceId: deviceId,
                        $userAgent: agent
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        }
                        interactionData.distinct_id = deviceId;
                        mixpanel.track(interactionEvent, interactionData);
                    });
                }
            });
        } else {
            var visitor = ua(config.google_analytics.tracking_id, deviceId);
            visitor.event({
                ec: deviceId,
                ea: interactionEvent,
                el: interactionEvent,
                ev: interactionData
            }).send();
            mixpanel.people.set(deviceId, {
                $deviceId: deviceId,
                $userAgent: agent
            }, function(err) {
                if (err) {
                    console.log(err);
                }
                interactionData.distinct_id = deviceId;
                mixpanel.track(interactionEvent, interactionData);
            });
        }
        Interaction.event(interactionEvent, userId, deviceId, interactionData, function(err, result) {

            if (err) return reply(err);

            reply(result);
        });
    }
};
