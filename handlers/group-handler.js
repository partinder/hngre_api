'use strict';
/**
 *  Module dependencies
 */
var Group = require('../models/group');
var Contact = require('../models/contact');
var notification = require('../operations/notifications/notifications');
var Invitation = require('../models/invitation');
var User = require('../models/user');
var Boom = require('boom');
var HngreMailer = require('../operations/email');
var unauthorized = require('../operations/unAuthorized');
var facebook = require('../operations/auth/facebook-auth');
var google = require('../operations/auth/google-auth');
var multiparty = require('multiparty');
var fs = require('fs');
var Imager = require('imager');
var imagerConfig = require('../imager');
var req = require('request');
var config = require('../config');
var constants = require('../constants');
var utils = require('../lib/utils');
var async = require('async');
var winston = require('../logger');
var ObjectId = require('mongoose').Types.ObjectId;


/**
 * load group by group id
 *
 * @method     load
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */
exports.load = function(request, reply) {
    var groupId = request.params.group_id;
    Group.load(groupId, function(err, group) {
        if (err) {
            winston.error('Group loading error by group Id %s', groupId);
            return reply(err);
        }
        reply(group);
    });
};

/**
 * load group by group id
 *
 * @method     load
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */
exports.loadMyGroup = function(request, reply) {
    var groupId = request.params.group_id;
    var userId = request.auth.credentials.user_id;
    if (!userId) {
        unauthorized.logUnauthorized(request, Boom.unauthorized("Unauthorized user"));
        return reply(Boom.unauthorized("Unauthorized user"));
    }
    Group.loadMyGroupById(groupId, userId, function(err, group) {
        if (err) {
            winston.error('Group loading error by group Id %s', groupId);
            return reply(err);
        }
        if (!group) return reply(null);
        reply(group);
    });
};

/**
 * load groups by user id
 *
 * @method     loadGroups
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 */
exports.loadGroups = function(request, reply) {
    var userId = request.params.user_id || request.auth.credentials.user_id;
    Group.loadGroups(userId, function(err, groups) {
        if (err) {
            winston.error('Group loading error by user Id %s', userId);
            return reply(err);
        }
        reply(groups);
    });
};

/**
 * create new group
 *
 * @method     create
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 */
exports.create = function(request, reply) {
    console.log(request.payload);
    var payload = request.payload;
    var user_id = request.auth.credentials.user_id;

    var userIds = [user_id];
    var emails = []; //to send email
    var phones = []; // to send sms
    var members = [];
    var invitees = [];
    var me = {};

    payload.members.forEach(function(member) {
        if (member.userId) {
            userIds.push(member.userId);
        } else if (member.email) {
            emails.push(member);
            invitees.push({
                sender: user_id,
                invitee: member
            });
        } else if (member.phone) {
            phones.push(member);
            invitees.push({
                sender: user_id,
                invitee: member
            });
        }
    });

    User.find({
        _id: {
            $in: userIds
        }
    }, {
        name: true,
        gender: true,
        birthday: true,
        profile_image: true,
        first_name: true,
        last_name: true,
        devices: true
    }, function(err, users) {
        if (err) {
            winston.error('User loading error by userIds ', userIds);
            return reply(err);
        }
        users = users.filter(function(user) {
            members.push({
                userId: user.id,
                name: user.name,
                first_name: user.first_name,
                last_name: user.last_name,
                gender: user.gender,
                birthday: user.birthday,
                photo: user.profile_image
            });
            if (!user._id.equals(user_id)) {
                invitees.push({
                    sender: user_id,
                    invitee: {
                        userId: user.id,
                        name: user.name,
                        first_name: user.first_name,
                        last_name: user.last_name
                    }
                });
                return true;
            } else {
                me = user;
                return false;
            }
        });

        Invitation.collection.insert(invitees, function(err, invitees) {
            if (err) {
                winston.error('Invitation inserting error', invitees);
                return reply(err);
            }
            invitees.forEach(function(invitee) {

                if (invitee.invitee.email) {
                    members.push({
                        name: invitee.invitee.name,
                        first_name: invitee.invitee.first_name,
                        last_name: invitee.invitee.last_name,
                        inviteId: invitee._id,
                        email: invitee.invitee.email
                    });
                } else if (invitee.invitee.phone) {
                    var phone = invitee.invitee.phone.replace(/[ \-\(\)\.]*/g, '');
                    members.push({
                        name: invitee.invitee.name,
                        first_name: invitee.invitee.first_name,
                        last_name: invitee.invitee.last_name,
                        inviteId: invitee._id,
                        phone: phone
                    });
                } else if (invitee.invitee.userId) {
                    var index = utils.indexof(members, {
                        userId: invitee.invitee.userId
                    });
                    if (~index) {
                        members[index].inviteId = invitee._id;
                    }
                }
            });
            var group = {
                name: payload.name,
                description: payload.description,
                group_icon: payload.group_icon,
                creator: ObjectId(user_id),
                members: members
            };

            group = new Group(group);
            group.addGroup(function(err) {
                if (err) {
                    winston.error('Adding new group error', group);
                    return reply(err);
                }
                reply(group);
                var forms = [];
                var form1 = {};
                group.members.forEach(function(member) {
                    if (member.userId != user_id) {
                        me.name = me.name || "";
                        me.first_name = me.first_name || "";
                        me.profile_image = me.profile_image || "";

                        //var groupPath = constants.default.setGroupPath(group.id);
                        if (member.userId) {
                            var data = '{\"hngre_path\":\"group\"';
                            data += ',\"hngre_groupId\":\"' + group.id + '\"}';
                            forms.push({
                                branch_key: config.branch.key,
                                feature: 'share',
                                channel: 'sms',
                                data: data,
                                inviteId: member.inviteId,
                                user: me
                            });

                        } else {
                            var data = '{\"hngre_invite_id\":\"' + member.inviteId + '\"';
                            data += ',\"hngre_inviting_user_id\":\"' + me._id + '\"';
                            data += ',\"hngre_inviting_user_full_name\":\"' + me.name + '\"';
                            data += ',\"hngre_inviting_user_short_name\":\"' + me.first_name + '\"';
                            data += ',\"hngre_inviting_user_image_url\":\"' + me.profile_image + '\"}';

                            forms.push({
                                branch_key: config.branch.key,
                                feature: 'invite',
                                channel: 'sms',
                                data: data,
                                inviteId: member.inviteId,
                                user: me
                            });
                        }
                    } else {
                        var data = '{\"hngre_path\":\"group\"';
                        data += ',\"hngre_groupId\":\"' + group.id + '\"}';
                        form1 = {
                            branch_key: config.branch.key,
                            feature: 'share',
                            channel: 'sms',
                            data: data
                        };
                    }
                });

                async.map(forms, function(form, callback) {
                        me = form.user;
                        var inviteId = form.inviteId;
                        form = _.omit(form, 'user');
                        form = _.omit(form, 'inviteId');
                        //iterator
                        req.post({
                            url: config.branch.uri,
                            form: form
                        }, function(err, response, body) {
                            if (!err && response.statusCode == 200) {
                                body = JSON.parse(body);
                                var deepLink = body.url;
                                var data = JSON.parse(form.data);
                                data.hngre_invite_id = inviteId;
                                var index = utils.indexof(invitees, {
                                    _id: data.hngre_invite_id
                                });
                                if (~index) {
                                    Group.findOneAndUpdate({
                                        "members.inviteId": data.hngre_invite_id
                                    }, {
                                        $set: {
                                            "members.$.deepLink": deepLink
                                        }
                                    }, function(err) {
                                        if (err) {
                                            winston.error(err);
                                        }
                                    });
                                    var formData = null;
                                    if (invitees[index].invitee.email) {
                                        winston.info('Sending friend invite email');
                                        formData = {};
                                        formData.event = constants.frient_invite.event;
                                        formData.email = {};
                                        formData.email.me = invitees[index].invitee.name,
                                            formData.email.email = invitees[index].invitee.email,
                                            formData.email.name = me.name;
                                        formData.email.first_name = me.first_name;
                                        formData.email.last_name = me.last_name;
                                        formData.email.gender = me.gender;
                                        formData.email.deepLink = deepLink;
                                    } else if (invitees[index].invitee.phone) {
                                        winston.info('Sending friend invite sms');
                                        formData = {};
                                        formData.event = constants.frient_invite.event;
                                        formData.sms = {};
                                        formData.sms.phone = invitees[index].invitee.phone;
                                        formData.sms.body = constants.frient_invite.setSmsBodyForNewUser(invitees[index].invitee.first_name, me.first_name, me.name, me.gender, deepLink);
                                    } else if (invitees[index].invitee.userId) {
                                        winston.info('Sending friend invite push');
                                        formData = {};
                                        formData.event = constants.frient_invite.event;
                                        formData.initiator = {};
                                        formData.initiator.from = constants.frient_invite.initiator.from;
                                        formData.initiator.type = constants.frient_invite.initiator.type;
                                        formData.initiator = {};
                                        formData.title = constants.frient_invite.setTitle();
                                        formData.body = constants.frient_invite.setBody(me.first_name, me.gender, group.name);
                                        formData.push_body = constants.frient_invite.setPushBody(me.first_name, me.gender, group.name);
                                        formData.sms_body = constants.frient_invite.setSmsBodyForExistingUser(me.first_name, me.gender, group.name, deepLink, me.name, invitees[index].invitee.first_name);
                                        formData.path = constants.default.setGroupPath(group._id.toString());
                                        formData.to = constants.frient_invite.to;
                                        formData.icon = group.group_icon || constants.default.group_default_icon;
                                        formData.sender = {};
                                        formData.sender.first_name = me.first_name;
                                        formData.sender.name = me.name;
                                        formData.sender.icon = me.profile_image;
                                        formData.sender.gender = me.gender;
                                        formData.groupName = group.name;
                                        formData.members = group.members.map(function(m) {
                                            m._doc.deepLink = deepLink;
                                            return m._doc;
                                        });

                                        notification.generateNotifications([invitees[index].invitee.userId], formData, function(err) {
                                            return callback(null, body);
                                        });

                                    } else {
                                        return callback(null, body);
                                    }
                                    if (!formData) return callback(null, body);
                                    winston.info(formData);
                                    if (!invitees[index].invitee.userId) {
                                        req.post({
                                            url: config.notifier.url,
                                            form: formData
                                        }, function(err, response, body) {
                                            if (err) {
                                                winston.info(formData);
                                                winston.error('Posting formdata error', err);
                                            }
                                            return callback(null, body);
                                        });
                                    }
                                }

                            } else {
                                callback(err || response.statusCode);
                            }
                        });

                    },
                    function(err, results) {
                        if (!err) {
                            winston.info(results);
                        } else {
                            // handle error here
                        }
                        req.post({
                            url: config.branch.uri,
                            form: form1
                        }, function(err, response, body) {
                            if (!err && response.statusCode == 200) {
                                body = JSON.parse(body);
                                var deepLink = body.url;
                                Group.update({
                                    _id: group._id,
                                    "members.userId": ObjectId(user_id)
                                }, {
                                    $set: {
                                        "members.$.deepLink": deepLink
                                    }
                                }, function(err) {
                                    console.log(err);
                                });
                            } else {
                                console.log(err, response.statusCode);
                            }
                        });
                    });
            });
        });

    });
};

/**
 * remove group by id(for owner)
 *
 * @method     remove
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */
exports.remove = function(request, reply) {
    var groupId = request.params.group_id;
    var user_id = request.auth.credentials.user_id;
    Group.findOne({
            _id: groupId,
            "members.userId": user_id
        },
        function(err, group) {
            if (err) {
                console.log(err);
                return reply(err);
            }
            if (!group) {
                unauthorized.logUnauthorized(request, Boom.unauthorized("User does not belong to group"));
                return reply(Boom.unauthorized('User does not belong to group'));
            }
            group.removeGroup(function(err) {
                if (err) {
                    console.log(err);
                    return reply(err);
                }
                reply({
                    success: true
                });
            });
        });
};

/**
 * update group by id (for owner)
 *
 * @method     update
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */
exports.update = function(request, reply) {
    var groupId = request.params.group_id;
    var user_id = request.auth.credentials.user_id;
    var data = request.payload;
    data = _.pick(data, 'name', 'description');
    Group.findOne({
        _id: groupId,
        "members.userId": user_id
    }, function(err, group) {
        if (err) {
            console.log(err);
            return reply(err);
        }
        if (!group) {
            unauthorized.logUnauthorized(request, Boom.unauthorized("User does not belong to group"));
            return reply(Boom.unauthorized('User does not belong to group'));
        }
        group.updateGroup(data, function(err) {
            if (err) {
                console.log(err);
                return reply(err);
            }
            reply(group);
        });
    });
};

/**
 * join the group
 *
 * @method     joinGroup
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */

exports.joinGroup = function(request, reply) {
    var groupId = request.params.group_id;
    var memberId = request.params.member_id;
    var userId = request.auth.credentials.user_id;
    Group.load(groupId, function(err, group) {
        if (err) {
            console.log(err);
            return reply(err);
        }
        if (!group) return reply(Boom.notFound('Could not find group'));
        group.addMember(userId, memberId, function(err) {
            if (err) {
                console.log(err);
                return reply(err);
            }
            reply(group);
        })
    });
};
/**
 * add members into the group
 *
 * @method     joinGroup
 * @param      {Function}  request  { description }
 * @param      {Function}  reply    { description }
 */
exports.addMembers = function(request, reply) {
    var payload = request.payload;
    var groupId = request.params.group_id;
    var user_id = request.auth.credentials.user_id;

    var userIds = [user_id];
    var emails = []; //to send email
    var phones = []; // to send sms
    var members = [];
    var invitees = [];
    var me = {};

    payload.members.forEach(function(member) {
        if (member.userId) {
            userIds.push(member.userId);
        } else if (member.email) {
            emails.push(member);
            invitees.push({
                sender: user_id,
                invitee: member
            });
        } else if (member.phone) {
            phones.push(member);
            invitees.push({
                sender: user_id,
                invitee: member
            });
        }
    });

    User.find({
        _id: {
            $in: userIds
        }
    }, {
        name: true,
        gender: true,
        birthday: true,
        profile_image: true,
        first_name: true,
        last_name: true,
        devices: true
    }, function(err, users) {
        if (err) {
            console.log(err);
            reply(err);
        }
        users = users.filter(function(user) {
            if (!user._id.equals(user_id)) {
                invitees.push({
                    sender: user_id,
                    invitee: {
                        userId: user.id,
                        name: user.name,
                        first_name: user.first_name,
                        last_name: user.last_name
                    }
                });
                members.push({
                    userId: user.id,
                    name: user.name,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    gender: user.gender,
                    birthday: user.birthday,
                    photo: user.profile_image
                });
                return true;
            } else {
                me = user;
                return false;
            }
        });

        Invitation.collection.insert(invitees, function(err, invitees) {
            if (err) {
                console.log(err);
                return reply(err);
            }
            invitees.forEach(function(invitee) {
                if (invitee.invitee.email) {
                    members.push({
                        name: invitee.invitee.name,
                        first_name: invitee.invitee.first_name,
                        last_name: invitee.invitee.last_name,
                        inviteId: invitee._id,
                        email: invitee.invitee.email
                    });
                } else if (invitee.invitee.phone) {
                    var phone = invitee.invitee.phone.replace(/[ \-\(\)\.]*/g, '');
                    members.push({
                        name: invitee.invitee.name,
                        first_name: invitee.invitee.first_name,
                        last_name: invitee.invitee.last_name,
                        inviteId: invitee._id,
                        phone: phone
                    });
                } else if (invitee.invitee.userId) {
                    var index = utils.indexof(members, {
                        userId: invitee.invitee.userId
                    });
                    if (~index) {
                        members[index].inviteId = invitee._id;
                    }
                }
            });

            Group.load(groupId, function(err, group) {
                if (err) {
                    console.log(err);
                    return reply(err);
                }
                if (!group) return reply(Boom.notFound('Could not find group'));
                var data = JSON.parse(JSON.stringify(group.members));
                members.forEach(function(member) {
                    var isEmail = false,
                        isPhone = false,
                        isUserId = false;
                    data.forEach(function(m) {
                        if (member.email == m.email) {
                            isEmail = true;
                        }
                    });
                    data.forEach(function(m) {
                        if (member.phone == m.phone) {
                            isPhone = true;
                        }
                    });
                    data.forEach(function(m) {
                        if (member.userId == m.userId) {
                            isUserId = true;
                        }
                    });
                    if (isEmail || isPhone || isUserId) {
                        group.members.push(member);
                    }
                });

                group.save(function(err) {
                    if (err) {
                        console.log(err);
                        return reply(err);
                    }
                    reply(group);
                    //TODO
                    var forms = [];
                    members.forEach(function(member) {
                        me.name = me.name || "";
                        me.first_name = me.first_name || "";
                        me.profile_image = me.profile_image || "";

                        //var groupPath = constants.default.setGroupPath(group.id);
                        if (member.userId) {
                            var data = '{\"hngre_path\":\"group\"';
                            data += ',\"hngre_groupId\":\"' + group.id + '\"}';
                            forms.push({
                                branch_key: config.branch.key,
                                feature: 'share',
                                channel: 'sms',
                                data: data,
                                inviteId: member.inviteId,
                                user: me
                            });

                        } else {
                            var data = '{\"hngre_invite_id\":\"' + member.inviteId + '\"';
                            data += ',\"hngre_inviting_user_id\":\"' + me._id + '\"';
                            data += ',\"hngre_inviting_user_full_name\":\"' + me.name + '\"';
                            data += ',\"hngre_inviting_user_short_name\":\"' + me.first_name + '\"';
                            data += ',\"hngre_inviting_user_image_url\":\"' + me.profile_image + '\"}';

                            forms.push({
                                branch_key: config.branch.key,
                                feature: 'invite',
                                channel: 'sms',
                                data: data,
                                inviteId: member.inviteId,
                                user: me
                            });
                        }
                    });
                    async.map(forms, function(form, callback) {
                            me = form.user;
                            var inviteId = form.inviteId;
                            form = _.omit(form, 'user');
                            form = _.omit(form, 'inviteId');
                            //iterator
                            req.post({
                                url: config.branch.uri,
                                form: form
                            }, function(err, response, body) {
                                if (!err && response.statusCode == 200) {
                                    body = JSON.parse(body);
                                    var deepLink = body.url;
                                    var data = JSON.parse(form.data);
                                    data.hngre_invite_id = inviteId;
                                    var index = utils.indexof(invitees, {
                                        _id: data.hngre_invite_id
                                    });
                                    if (~index) {
                                        Group.findOneAndUpdate({
                                            "members.inviteId": data.hngre_invite_id
                                        }, {
                                            $set: {
                                                "members.$.deepLink": deepLink
                                            }
                                        }, function(err) {
                                            if (err) {
                                                winston.error(err);
                                            }
                                        });
                                        var formData = null;
                                        if (invitees[index].invitee.email) {
                                            console.log('Sending friend invite email');
                                            formData = {};
                                            formData.event = constants.frient_invite.event;
                                            formData.email = {};
                                            formData.email.me = invitees[index].invitee.name,
                                                formData.email.email = invitees[index].invitee.email,
                                                formData.email.name = me.name;
                                            formData.email.first_name = me.first_name;
                                            formData.email.last_name = me.last_name;
                                            formData.email.gender = me.gender;
                                            formData.email.deepLink = deepLink;
                                        } else if (invitees[index].invitee.phone) {
                                            console.log('Sending friend invite sms');
                                            formData = {};
                                            formData.event = constants.frient_invite.event;
                                            formData.sms = {};
                                            formData.sms.phone = invitees[index].invitee.phone;
                                            formData.sms.body = constants.frient_invite.setSmsBodyForNewUser(invitees[index].invitee.first_name, me.first_name, me.name, me.gender, deepLink);
                                        } else if (invitees[index].invitee.userId) {
                                            winston.info('Sending friend invite push');
                                            formData = {};
                                            formData.event = constants.frient_invite.event;
                                            formData.initiator = {};
                                            formData.initiator.from = constants.frient_invite.initiator.from;
                                            formData.initiator.type = constants.frient_invite.initiator.type;
                                            formData.initiator = {};
                                            formData.title = constants.frient_invite.setTitle();
                                            formData.body = constants.frient_invite.setBody(me.first_name, me.gender, group.name);
                                            formData.push_body = constants.frient_invite.setPushBody(me.first_name, me.gender, group.name);
                                            formData.sms_body = constants.frient_invite.setSmsBodyForExistingUser(me.first_name, me.gender, group.name, deepLink, me.name, invitees[index].invitee.first_name);
                                            formData.path = constants.default.setGroupPath(group._id.toString());
                                            formData.to = constants.frient_invite.to;
                                            formData.icon = group.group_icon || constants.default.group_default_icon;
                                            formData.sender = {};
                                            formData.sender.first_name = me.first_name;
                                            formData.sender.name = me.name;
                                            formData.sender.icon = me.profile_image;
                                            formData.sender.gender = me.gender;
                                            formData.groupName = group.name;
                                            formData.members = group.members.map(function(m) {
                                                m._doc.deepLink = deepLink;
                                                return m._doc;
                                            });

                                            notification.generateNotifications([invitees[index].invitee.userId], formData, function(err) {
                                                return callback(null, body);
                                            });

                                        } else {
                                            return callback(null, body);
                                        }
                                        if (!formData) return callback(null, body);
                                        winston.info(formData);
                                        if (!invitees[index].invitee.userId) {
                                            req.post({
                                                url: config.notifier.url,
                                                form: formData
                                            }, function(err, response, body) {
                                                if (err) {
                                                    winston.info(formData);
                                                    winston.error('Posting formdata error', err);
                                                }
                                                callback(null, body);
                                            });
                                        }
                                    }

                                } else {
                                    callback(err || response.statusCode);
                                }
                            });

                        },
                        function(err, results) {
                            if (!err) {
                                console.log(results);
                            } else {
                                // handle error here
                            }
                        });
                });
            });
        });

    });
};

/**
 * leave the group
 *
 * @method     leaveGroup
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 * @param      {<type>}  groupId  { description }
 */
exports.leaveGroup = function(request, reply) {

    var type = request.params.type;
    var groupId = request.params.groupId;
    var memberId = request.params.memberId;
    var userId = request.auth.credentials.user_id;
    Group.load(groupId, function(err, group) {
        if (err) {
            console.log(err);
            return reply(err);
        }
        if (!group) return reply(Boom.notFound('Could not find the group'));

        group.removeMember(userId, memberId, function(err) {
            if (err) {
                console.log(err);
                return reply(err);
            }

            if (type === 'invited') { //Delete the invite object as well
                Invitation.remove({
                    _id: memberId
                }, function(err, res) {
                    console.log('Deleted invitation', err, res);
                })
            }

            reply(group);
        })
    });
};

/**
 * Get users by facebook, google, email and phone number
 *
 * @method     getUsers
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 */
exports.getUsers = function(request, reply) {
    var payload = request.payload;
    var userId = request.auth.credentials.user_id;
    User.findOne({
        _id: userId
    }, function(err, user) {
        if (err) {
            console.log(err);
            throw err;
        }
        var facebookIndex = _.findLastIndex(user.login, {
            type: 'facebook'
        });
        winston.info(facebookIndex);
        var isFacebook = ~facebookIndex ? true : false;
        var googleIndex = _.findLastIndex(user.login, {
            type: 'google'
        });
        winston.info(googleIndex);
        var isGoogle = ~googleIndex ? true : false;
        var facebookToken = isFacebook ? user.login[facebookIndex].data.token : null;
        winston.info(facebookToken);
        var googleTokens = isGoogle ? user.login[googleIndex].data : null;
        winston.info(googleTokens);
        var data = {};
        var phones = _.pluck(payload, "phones");
        var emails = _.pluck(payload, "emails");
        phones = _.flatten(phones);
        emails = _.flatten(emails);
        data.emails = emails;
        data.phones = phones;
        if (isFacebook) {
            winston.info('facebook');
            facebook.getConnectedPeople(facebookToken, function(err, friends) {
                if (err) {
                    console.log(err);
                    //throw err;
                    friends = [];
                }
                if (!(friends && friends.length > 0)) {
                     friends = [];
                }
                Contact.findOne({
                    user: userId
                }, function(err, contact) {
                    if (err) {
                        winston.error(err);
                    }
                    if (contact) {
                        contact.payload = payload;
                        contact.facebooks = friends;
                    } else {
                        contact = new Contact({
                            user: userId,
                            contacts: payload,
                            facebooks: friends
                        });
                    }
                    contact.save(function(err, contact) {
                        if (err) {
                            winston.error(err);
                        }
                    });
                });

                data.facebook = friends;
                User.loadUsers(data, function(err, users) {
                    if (err) {
                        console.log(err);
                        return reply(err);
                    }
                    winston.info(_.pluck(users, "email"));
                    //winston.info(users);
                    reply(users);
                });
            });
        } else if (isGoogle) {
            winston.info('google');
            google.getConnectedPeople(googleTokens, function(err, friends) {
                if (err) {
                    console.log(err);
                    //throw err;
                    friends = [];
                }
                if (!(friends && friends.length > 0)) {
                    friends = [];
                }
                Contact.findOne({
                    user: userId
                }, function(err, contact) {
                    if (err) {
                        winston.error(err);
                    }
                    if (contact) {
                        contact.payload = payload;
                        contact.googles = friends;
                    } else {
                        contact = new Contact({
                            user: userId,
                            contacts: payload,
                            facebooks: friends
                        });
                    }
                    contact.save(function(err, contact) {
                        if (err) {
                            winston.error(err);
                        }
                    });
                });

                data.google = friends;
                User.loadUsers(data, function(err, users) {
                    if (err) {
                        console.log(err);
                        return reply(err);
                    }
                    winston.info(_.pluck(users, "email"));
                    //winston.info(users);
                    reply(users);
                });
            });
        } else {
            User.loadUsers(data, function(err, users) {
                if (err) {
                    console.log(err);
                    return reply(err);
                }
                winston.info(_.pluck(users, "email"));
                //winston.info(users);
                reply(users);
            });
        }
    });
};

/**
 * Upload image of the group
 *
 * @method     uploadImage
 * @param      {<type>}  request  { description }
 * @param      {<type>}  reply    { description }
 */
exports.uploadImage = function(request, reply) {
    var groupId = request.query.group_id;
    var userId = request.auth.credentials.user_id;
    var form = new multiparty.Form();
    form.parse(request.payload, function(err, fields, files) {
        //console.log(files);
        if (err || !files) {
            return reply(err);
        }
        var images = files.file && files.file.length > 0 ? [files.file[0]] : undefined;
        imagerConfig.storage.uploadDirectory = constants.cloudfront.group.folder;
        var imager = new Imager(imagerConfig, 'S3');
        imager.upload(images, function(err, cdnUri, _files) {
            if (err) return reply(err);
            var image = cdnUri;
            winston.info(cdnUri, _files);
            if (_files.length) {
                image = constants.cloudfront.host + constants.cloudfront.group.folder + constants.cloudfront.group.prefix + _files[0];
            } else {
                image = constants.cloudfront.host + constants.cloudfront.group.folder + constants.cloudfront.group.prefix + _files;
            }
            if (!groupId) {
                return reply({
                    url: image
                });
            }
            Group.findOne({
                _id: groupId,
                "members.userId": userId
            }, function(err, group) {
                if (err) {
                    console.log(err);
                    return reply(err);
                }
                if (!group) {
                    unauthorized.logUnauthorized(request, Boom.unauthorized("User does not belong to group"));
                    return reply(Boom.unauthorized('User does not belong to group'));
                }
                group.group_icon = image;
                group.save(function(err) {
                    if (err) {
                        console.log(err);
                        return reply(err);
                    }
                    console.log(images[0].path);
                    fs.unlinkSync(images[0].path);
                    reply(group);
                });
            });
        }, 'group');
    });
};
