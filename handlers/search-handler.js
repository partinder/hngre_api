/**
 * Created by hngre on 9/3/15.
 */

var MongoSearch = require('../operations/search/search');
var ElasticSearch = require('../operations/search/search-elastic');
var DishResponse = require('../operations/dish-response');
var MerchantResponse = require('../operations/merchant-response');
var History = require('../operations/user-history');
var Group = require('../models/group');
var unauthorized = require('../operations/unAuthorized');

var ObjectId = require('mongoose').Types.ObjectId;

var Boom = require('boom');
var winston = require('../logger');
var error = require('../error');

function logParams(request, title) {

    var queryLog = {
        params: request.query
    };

    if (request.auth.credentials) {
        _.extend(queryLog, request.auth.credentials);
    }

    winston.info(title, 'with params', queryLog);
}

module.exports = {

    search: function(request, reply) {

        logParams(request, 'Search');

        ElasticSearch.searchFor(request.query, function(err, result) {

            if (err) {
                winston.error('Search error', err);
                return reply(Boom.badRequest(err.message));
            }

            var params = request.query;

            try {
                params.userId = request.auth.credentials.user_id;
            } catch (err) { /*Do nothing*/ };

            DishResponse.prepare(DishResponse.type.SEARCH, result, params, function(err, returnDishes) {

                if (err) return reply(err);

                reply(returnDishes);
            })
        });
    },

    searchForMerchants: function(request, reply) {

        logParams(request, 'Search');
        var params = request.query;       
        var userId = request.auth.credentials.user_id;
        params.userId = userId;
        var userIds = [ObjectId(userId)];
        var groupId = params.group_id;
        Group.findOne({
            _id: groupId,
            "members.userId": userId
        }, function(err, group) {
            if (err) {
                winston.error('Group not found', {
                    groupId: groupId
                });
            }
            if (group && group.members && group.members.length > 0) {
                userIds = _.pluck(group.members, "userId");
                userIds = _.filter(userIds, function(user) {
                    if (user) return true;
                    else return false;
                });
            }

            try {
                params.userId = userId;
                params.userIds = userIds;
            } catch (err) { /*Do nothing*/ };

            params.all = true;            

            History.historyByUserIds(userIds, params.userId, params.current_rec_id, function(err, history) {
                if (err && err.message == 'Unauthorized user') {
                    unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                    return reply(Boom.unauthorized(err.message));
                }

                params.merchantsToExclude = history.prev_recs.merchantsToExclude;

                params.users = history.users;
                params.user = history.user;
                var dishes = [];
                params.dishesToExclude = [];
                if(history.prev_recs && history.prev_recs.prevRecs){                    
                    dishes = history.prev_recs.prevRecs.map(function(d) {
                        return d._dish;
                    });
                    if(dishes.length){
                        params.dishesToExclude = dishes;
                    }
                }

                ElasticSearch.searchForMerchant(params, function(err, result) {

                    if (err) {
                        winston.error('Search error', err);
                        return reply(Boom.badRequest(err.message));
                    }

                    result = result.map(function(m) {
                        m.dishes = m.dishes.filter(function(d){
                            if(~dishes.indexOf(d._id)){
                                return false;
                            }else{
                                dishes.push(d._id);
                                return true;
                            }
                        });
                        return m;                                
                    });

                    MerchantResponse.prepare(MerchantResponse.type.SEARCH, result, params, function(err, returnDishes) {

                        if (err && err.message == 'Unauthorized user') {
                            unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                            return reply(Boom.unauthorized(err.message));
                        }
                        returnDishes.merchants = returnDishes.merchants.map(function(m){
                            winston.info("Merchant Id : ", m._id, " Merchant Name : ", m.name);
                            m.dishes = m.dishes.map(function(d){
                                winston.info("Dish Id : ", d._id, " Dish Name : ", d.name);
                                winston.info("Dish Scores : ", d.scores);
                                if(!d.scores){
                                    d.scores = {};
                                }
                                return d;
                            });
                            return m;
                        });     
                        returnDishes.merchants = returnDishes.merchants.filter(function(m){
                            if(m.dishes && m.dishes.length > 0){
                                return true;
                            }else{
                                return false;
                            }
                        });                     
                        reply(returnDishes);
                    });
                });

            });
        });
    },

    suggest: function(request, reply) {

        logParams(request, 'Suggestion');

        ElasticSearch.typeAhead(request.query, function(err, result) {

            if (err) {
                winston.error('Suggestion error', err);
                reply(Boom.badRequest(err.message));
            } else {
                result.map(function(r) {
                    switch (r.scope) {
                        case 'name':
                            {
                                r.scope = 'dishes.name';
                            };
                            break;
                        case 'merchant.name':
                            {
                                r.scope = 'name';
                            };
                            break;
                        case 'cuisine':
                            {
                                r.scope = 'dishes.cuisine';
                            };
                            break;
                        case 'ingredients':
                            {
                                r.scope = 'dishes.ingredients';
                            };
                            break;
                        case 'tags':
                            {
                                r.scope = 'dishes.tags';
                            };
                            break;
                    }
                    return r;
                });
                reply(result);
            }
        });
    },

    location: function(request, reply) {

        logParams(request, 'Location search');

        ElasticSearch.searchLocation(request.query, function(err, result) {

            if (err) {
                winston.error('Location search error', err);
                reply(Boom.badRequest(err.message));
            } else {
                reply(result);
            }
        })

    },

    locality: function(request, reply) {

        logParams(request, 'Cities');

        MongoSearch.localities(request.query, function(err, result) {

            if (err) {
                winston.error('Locality search error', err);
                reply(Boom.badImplementation(err.message));
            } else {
                reply(result);
            }
        })
    }
};