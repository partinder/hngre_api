(function() {
    'use strict';
    var InviteResolve = require('../operations/invitation/invite-resolve');
    var winston = require('../logger');
    var Boom = require('boom');
    var unauthorized = require('../operations/unAuthorized');

    /**
     * Invitation resolution by invite id
     *
     * @method     resolve
     * @param      {<type>}  request  { description }
     * @param      {<type>}  reply    { description }
     */
    exports.resolve = function(request, reply) {
        var inviteId = request.params.inviteId;
        var userId = request.auth.credentials.user_id;
        if (inviteId) {
            InviteResolve.resolve(userId, inviteId, function(err, user, group) {
                if (err) {
                    winston.error('Invitation resolution error', err);
                    return reply(Boom.notFound(err));
                }
                if (!group) {
                    return reply({
                        success: true,
                        user_id: userId
                    });
                }
                return reply({
                    success: true,
                    group_id: group.id,
                    group_name: group.name,
                    user_id: userId
                });
            });
        } else {
            reply(Boom.notFound(new Error('Invite Id not found')));
        }
    };

}())