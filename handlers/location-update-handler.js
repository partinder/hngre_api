/*
    LOCATION UPDATE HANDLER
    Handles location updates from users.
 */


var winston = require('../logger');
var constants = require('../constants');
var locationUpdate = require('../operations/location-update/location-update');
var unauthorized = require('../operations/unAuthorized');

module.exports = {

    location: function(request, reply) {

        reply({});
        //TODO Complete
        //Check if there is any dish that is wish listed within a range of 250 m.
        // If yes, send a remote notification, 'An item from your wish list is near you'

        var userId = request.auth.credentials.user_id;

        locationUpdate.trackLocation(userId, request.payload, function(err, res) {

        });
    },

    visit: function(request, reply) {
        //FIXME Update
        //Check if there is any dish/restaurant within a range of 25 m.
        //If yes, send a remote notification 'Seems like you are at this restaurant. Here's what we recommend here.'

        winston.info('Received visit to', request.payload, '. Time: ', Date.now());

        var userId = request.auth.credentials.user_id;

        locationUpdate.trackVisit(userId, request.payload, function(err, res) {

            if (err) return reply(err);

            reply(res);
        })

    },
    getLocations: function(request, reply) {
        reply(constants.locations);
    }
}