(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Boom = require("boom");
    var winston = require("../../logger");
    var recommendation = require("../../operations/recommendation/v2");
    var DishSource = require('../../operations/elasticsearch-common');
    var MerchantResponse = require('../../operations/merchant-response');
    var History = require('../../operations/user-history');
    var Rating = require('../../models/rating');
    var Group = require('../../models/group');
    var ObjectId = require('mongoose').Types.ObjectId;
    var unauthorized = require('../../operations/unAuthorized');

    /**
     * dish recommend handler
     *
     * @method     recommend
     * @param      {<type>}    request  { description }
     * @param      {Function}  reply    { description }
     * @return     {<type>}    { description_of_the_return_value }
     */
    exports.recommend = function(request, reply) {

        var userId = '';
        if (request.params.user_id) {
            userId = request.params.user_id;
        } else {
            userId = request.auth.credentials.user_id;
        }

        var query = request.query;
        if (!userId) {
            unauthorized.logUnauthorized(request, Boom.unauthorized("Unauthorized user"));
            return reply(Boom.unauthorized());
        }
        if (!query) return reply(Boom.badRequest(new Error("query suppose to be non-empty")));
        var groupId = query.group_id;
        var userIds = [ObjectId(userId)];
        Group.findOne({
            _id: groupId,
            "members.userId": userId
        }, function(err, group) {
            if (err) {
                winston.error('Group %s not found', groupId);
            }
            if (group && group.members && group.members.length > 0) {
                userIds = _.pluck(group.members, "userId");
                userIds = _.filter(userIds, function(user) {
                    if (user) return true;
                    else return false;
                });
            }
            query.userId = userId;
            winston.info("Fetching recommendation for users", userIds);
            winston.info("Fetching recommendation with query", query);
            recommendation.byUserIds(userIds, query, function(err, merchants) {
                if (err) {
                    if (err.message == 'Unauthorized user') {
                        unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                        return reply(Boom.unauthorized(err.message));
                    }
                    return reply(err);
                }
                merchants.merchants = _.shuffle(merchants.merchants);
                merchants.merchants = merchants.merchants.map(function(m) {
                    winston.info("Merchant Id : ", m._id, " Merchant Name : ", m.name);
                    m.dishes = m.dishes.map(function(d) {
                        winston.info("Dish Id : ", d._id, " Dish Name : ", d.name);
                        winston.info("Dish Scores : ", d.scores);
                        if (!d.scores) {
                            d.scores = {};
                        }
                        return d;
                    });
                    return m;
                });
                reply(merchants);
            });
        });
    };

    //Get merchants on which actions have been taken by a user
    exports.getActions = function(request, reply) {

        var userId;
        try {
            userId = request.auth.credentials.user_id
        } catch (err) {
            userId = request.query.userId;
        }

        if (!userId) {
            return reply(Boom.forbidden('Missing authentication'));
        }

        var queries = [];
        var shouldPopulatePredictions = false;
        var shouldPopulateHistory = false;

        if (request.query.rated) {
            queries.push({
                rating: {
                    $ne: null
                }
            });
            shouldPopulateHistory = true;
            shouldPopulatePredictions = true;
        }

        if (request.query.eat_later) {
            queries.push({
                eat_later: true
            });
            shouldPopulatePredictions = true;
            shouldPopulateHistory = true;
        }

        var query = {
            _user: userId
        };

        if (queries.length > 0) {
            _.extend(query, {
                $or: queries
            });
        }
        winston.info('query', query);

        Rating.find(query, function(err, ratings) {

            if (err) {
                return reply(err);
            } else {
                var merchantIds = []; //List of merchantIds to be fetched from the elasticsearch server.
                var dishIds = [];
                ratings.forEach(function(rating) {
                    var merchantId = rating._merchant.toString();
                    var dishId = rating._dish.toString();
                    dishIds.push(dishId);
                    merchantIds.push(merchantId);
                });

                if (merchantIds.length > 0) {
                    DishSource.fetchDishesFromMerchant(merchantIds, function(err, merchants) {

                        if (err) return reply(err);
                        else {
                            var params = {
                                userId: userId,
                                userIds: [ObjectId(userId)],
                                shouldPopulatePredictions: shouldPopulatePredictions,
                                shouldPopulateHistory: shouldPopulateHistory,
                                history: ratings,
                                all: true
                            };
                            winston.info(dishIds.length);
                            merchants.map(function(merchant) {
                                merchant.dishes = merchant.dishes.filter(function(dish) {
                                    var flag = false;
                                    for (var index = 0, len = dishIds.length; index < len; index++) {
                                        if (dish._id == dishIds[index]) {
                                            flag = true;
                                            break;
                                        }
                                    }
                                    return flag;
                                });
                                winston.info(merchant.dishes.length);
                                return merchant;
                            });
                            History.historyByUserIds(params.userIds, params.userId, params.current_rec_id, function(err, history) {
                                if (err && err.message == 'Unauthorized user') {
                                    unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                                    return reply(Boom.unauthorized(err.message));
                                }
                                params.users = history.users;
                                params.user = history.user;
                                MerchantResponse.prepare(MerchantResponse.type.WISHLIST, merchants, params, function(err, returnData) {

                                    if (err) {
                                        if (err.message == 'Unauthorized user') {
                                            unauthorized.logUnauthorized(request, Boom.unauthorized(err.message));
                                            return reply(Boom.unauthorized(err.message));
                                        }
                                        return reply(err);
                                    }
                                    returnData.merchants = returnData.merchants.filter(function(m) {
                                        if (m.dishes && m.dishes.length > 0) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    });
                                    returnData.merchants = returnData.merchants.map(function(m) {
                                        winston.info("Merchant Id : ", m._id, " Merchant Name : ", m.name);
                                        m.dishes = m.dishes.map(function(d) {
                                            winston.info("Dish Id : ", d._id, " Dish Name : ", d.name);
                                            winston.info("Dish Scores : ", d.scores);
                                            if (!d.scores) {
                                                d.scores = {};
                                            }
                                            return d;
                                        });
                                        return m;
                                    });
                                    reply(returnData.merchants);
                                });
                            })
                        }
                    });
                } else {
                    reply([]);
                }
            }
        });
    };

}());
