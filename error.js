(function() {
    'use strict';
    var request = require("request");
    var config = require('./config');
    var constants = require('./constants');
    var winston = require('./logger');

    exports.report = function(errors, data, email) {
        var option = {
            url: config.notifier.url,
            form: {
                event: constants.hngre_error.event,
                email: {
                    errors: errors,
                    data: data
                }
            }
        }
        if(email){
        	option.form.email.email = email;
        }
        if (process.env.NODE_ENV === 'production') {
            request.post(option, function(err, res, body) {
                if (err) {
                    winston.info(err);
                }
                winston.info(body);
            });
        }else{

        }
    }
}());
