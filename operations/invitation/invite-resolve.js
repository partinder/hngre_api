(function() {

    'use strict';
    var Invitation = require('../../models/invitation');
    var Group = require('../../models/group');
    var User = require('../../models/user');
    var utils = require('../../lib/utils');
    var async = require('async');
    var winston = require('../../logger');
    var constants = require('../../constants');
    var notification = require('../notifications/notifications');

    module.exports = {
        resolve: function(userId, inviteId, cb) {
            async.waterfall([

                    function findByInviteId(callback) {
                        var query = Invitation.findOne({
                            _id: inviteId
                        });
                        query.lean();
                        query.exec(function(err, invitation) {
                            if (err) {
                                winston.error('error', err);
                                return callback(err);
                            }
                            if (invitation) callback(null, invitation);
                            else callback(new Error('Invalid invite id'));
                        });
                    },
                    function verifyIdentity(invitee, callback) {
                        var inviteId = invitee.invitee.phone ? invitee.invitee.phone : invitee.invitee.email ? invitee.invitee.email : '';
                        var query = User.findOne({
                            _id: userId
                        });
                        query.exec(function(err, user) {
                            if (err) {
                                winston.error(err);
                                return callback(err);
                            }
                            if (user) {
                                return callback(null, user, invitee);
                            } else {
                                return callback(new Error('Can not associate this user'));
                            }
                        });
                    },
                    function findGroup(user, invitee, callback) {
                        var set = {
                            "members.$.userId": user._id,
                            "members.$.name": user.name || "",
                            "members.$.first_name": user.first_name || "",
                            "members.$.last_name": user.last_name || "",
                            "members.$.gender": user.gender || "",
                            "members.$.photo": user.profile_image || "",
                        };
                        if (user.birthday) {
                            set["members.$.birthday"] = user.birthday;
                        }
                        Group.findOneAndUpdate({
                                "members.inviteId": invitee._id,
                                "members.userId": {
                                    $ne: userId
                                }
                            }, {
                                $set: set,
                                $unset: {
                                    "members.$.inviteId": ""
                                        /*,
                                        "members.$.deepLink": ""*/
                                }
                            },
                            function(err, group) {
                                if (err) {
                                    winston.error(err);
                                    return callback(err);
                                }
                                if (group && invitee.invitee.phone) {
                                    User.findOneAndUpdate({
                                        _id: user._id
                                    }, {
                                        $set: {
                                            phone: invitee.invitee.phone
                                        }
                                    }, function(err, user) {
                                        if (err && err.errmsg && ~err.errmsg.indexOf('duplicate')) {
                                            winston.error(err);
                                        }
                                        return callback(null, user, group);
                                    });
                                } else {
                                    return callback(null, user, group);
                                }

                            });
                    }
                ],
                function(err, user, group) {
                    if (typeof cb !== 'function') {
                        cb = function() {};
                    }
                    if (err) {
                        winston.error(err);
                        return cb(err);
                    }
                    if (!group) return cb(null, user, group);

                    cb(null, user, group);

                    var userIds = _.pluck(group.members, 'userId');
                    userIds = userIds.filter(function(userId) {
                        if (user._id.equals(userId)) {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    var data = {
                        event: constants.friend_invite_resolve.event,
                        title: constants.friend_invite_resolve.setTitleForMe(),
                        body: constants.friend_invite_resolve.setBodyForMe(group.name),
                        path: constants.default.setGroupPath(group.id),
                        initiator: {
                            from: constants.friend_invite_resolve.initiator.from,
                            type: constants.friend_invite_resolve.initiator.type
                        },
                        to: constants.friend_invite_resolve.toForMe,
                        icon: group.group_icon || constants.default.group_default_icon,
                        sender: {
                            first_name: user.first_name,
                            name: user.name,
                            icon: user.profile_image,
                            gender: user.gender
                        },
                        members : group.members
                    };
                    notification.generateNotifications([user.id], data, function(err) {
                        data.title = constants.friend_invite_resolve.setTitleForUser();
                        data.body = constants.friend_invite_resolve.setBodyForUser(user.first_name, group.name);
                        data.sms_body = constants.friend_invite_resolve.setSmsBodyForUser(user.first_name, group.name, user.name);
                        data.push_body = constants.friend_invite_resolve.setPushBodyForUser(user.first_name, group.name);
                        data.to = constants.friend_invite_resolve.toForUser;
                        data.icon = user.profile_image;                        
                        notification.generateNotifications(userIds, data, cb);
                    });
                });
        }
    };
}())
