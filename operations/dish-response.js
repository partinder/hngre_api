/*
    Formats a list of dish documents fetched from the ElasticSearch data store. Functions include:
    - Merchant selection
    - Rating/Wishlist population
    - Recommendation score population
 */

var Common = require('./api-common');
var Async = require('async');
var Rating = require('../models/rating');
var User = require('../models/user');
var Device = require('../models/device');
var Recommendation = require('../models/recommendation');
var RecEngine = require('./recommendation/rec-engine-connect');
var winston = require('../logger');
var HngreMail = require('./email');
var request = require('request')

/**
 * Formats a given list of dish documents for response by selecting merchants and/or populating predictions/history
 * @param dishDocuments
 * @param params            Contains data such as user id, merchant id, and flags for population
 * @param finalCallback
 */
module.exports = {

    prepare: function(recType, dishDocuments, params, finalCallback) {

        if (!recType) recType = Reommendation.type.RECOMMENDATION;

        _.defaults(params, {
            shouldPopulatePredictions: true,
            shouldPopulateHistory: true,
            shouldCheckOpenStatus: true
        });

        winston.info('DISH RESPONSE! params:', params);

        var merchantParams = _.pick(params, 'lat', 'lon', 'country', 'region', 'locality', 'neighbourhood', 'zip');

        if (params.merchantId && dishDocuments.length === 1) merchantParams.merchantId = params.merchantId;

        winston.info('selecting merchants');
        //Select merchant based on params
        dishDocuments.forEach(function(dish) {

            if (params.history) {
                var dishRating = _.find(params.history, function(rating) {
                    return rating._dish.toString() === dish._id;
                });

                if (dishRating) merchantParams.merchantId = dishRating._merchant.toString();
                else merchantParams.merchantId = undefined;
            }

            Common.selectMerchant(dish, merchantParams);
        });

        //Parallel requests for ratings and recommendations

        if (params.userId && dishDocuments.length > 0) {
            var predictItems = [];
            var findRatings = [];

            dishDocuments.forEach(function(dish) {
                try {
                    predictItems.push({
                        dishId: dish._id,
                        merchantId: dish.merchant._id
                    });

                    findRatings.push({
                        $and: [{
                            _dish: dish._id
                        }, {
                            _merchant: dish.merchant._id
                        }]
                    });
                } catch (err) {
                    winston.error('Error occured pushing into DishData', {
                        err: err,
                        dish: dish,
                        selectedMerchant: selectedMerchant
                    });
                }
            });

            var asyncFunctions = {};

            if (params.shouldPopulatePredictions && predictItems.length > 0) {
                winston.info('Including func for predictions');
                asyncFunctions.predictions = function(callback) {

                    var recCallback = function(user) {

                        RecEngine.predictRatings(user, predictItems, function(err, predictions) {

                            if (err) {
                                if (err.code === 404) {
                                    winston.info('Predictions Not found');
                                } else {
                                    return callback(err);
                                }
                            }

                            callback(null, predictions[params.userId]);

                        });
                    };

                    if (params.user) return recCallback(params.user);

                    User.findOne({
                        _id: params.userId
                    }, function(err, user) {

                        if (err) return callback(err);

                        recCallback(user);
                    })
                };
            }

            if (params.shouldPopulateHistory && findRatings.length > 0) {
                winston.info('Including func for history');

                asyncFunctions.history = function(callback) {

                    if (params.history) return callback(null, params.history);

                    Rating.find({
                        _user: params.userId,
                        $or: findRatings
                    }, function(err, ratings) {

                        if (err) return callback(err);

                        callback(null, ratings);
                    });
                }
            }

            if (_.keys(asyncFunctions).length > 0) {
                Async.parallel(asyncFunctions, function(err, data) {

                    if (err) return finalCallback(err);

                    var ratingMap = {}; //Map of ratings for population.
                    var wishList = []; //List of Ids wish listed for population.

                    if (data.history) {
                        data.history.forEach(function(rating) {

                            var dishId = rating._dish.toString();
                            var merchantId = rating._merchant.toString();

                            if (rating.rating) {
                                ratingMap[dishId] = {};
                                ratingMap[dishId][merchantId] = rating.rating;
                            }

                            if (rating.eat_later) wishList.push(dishId);
                        });
                    }

                    winston.info('Rating map, wish list', {
                        ratingMap: ratingMap,
                        wishList: wishList
                    });

                    var dishDataToSave = []; //Data to save in recommendation database
                    var predictionCount = 0;

                    dishDocuments.forEach(function(dish) {

                        if (data.predictions) {
                            //Populate the ratings and wish list status
                            var prediction;

                            if (data.predictions.hasOwnProperty(dish.merchant._id)) {
                                prediction = data.predictions[dish.merchant._id][dish._id];
                            }

                            if (!prediction) {
                                winston.info('Prediction not found for dish', dish._id);
                            } else {
                                predictionCount++;
                            }

                            dish.score = prediction;
                        }

                        dishDataToSave.push({
                            _dish: dish._id,
                            _merchant: dish.merchant._id,
                            predicted_rating: dish.score
                        });

                        //Check and determine whether the merchants are currently open
                        if (params.shouldCheckOpenStatus && dish.merchant.hours) {
                            dish.merchant.hours.is_open = Common.merchantOpenStatus(dish.merchant);
                        }

                        if (ratingMap.hasOwnProperty(dish._id)) {
                            dish.rated = ratingMap[dish._id][dish.merchant._id];
                        }

                        if (wishList.indexOf(dish._id) > -1) dish.wishlist = true;
                    });

                    //Save or update the recommendation
                    var recCallback = function(err, recommendation) {

                        if (err) return callback(err);

                        winston.info('Rec finalCallback');

                        finalCallback(null, {
                            recId: recommendation._id,
                            dishes: dishDocuments
                        });

                        if (process.env.NODE_ENV === 'production') {
                            if (predictionCount < dishDataToSave.length) {
                                console.log('Sending email for inconsistency in predictions sent...');
                                //Send email
                                var email = new HngreMail('internal');
                                email.subject = '[URGENT] Prediction error/inconsistency';
                                var message = 'The number of recommendations fetched for user ' + params.userId + ' were ' + predictionCount + ' whereas the number of dishes sent was ' + dishDataToSave.length;
                                message += '\nThe recommendation id in question is ' + recommendation._id;
                                email.text = message;
                                email.to = 'akash.gupta@hngre.com, partinder@hngre.com';
                                email.send();
                            }

                            if (predictionCount <= 0) {
                                //Send SMS
                                request.get('http://www.hngre.com/phonesend?access_token=druanal77', function(err, res, body) {

                                });
                            }
                        }
                    };

                    if (dishDataToSave.length > 0) {
                        winston.info('params available', params);

                        var paramsToSave = _.pick(params, 'distance', 'lon', 'lat', 'country', 'region', 'locality', 'neighbourhood', 'zip', 'term', 'scope', 'offset');

                        winston.info('Params to save', paramsToSave);

                        //If rec id exists, update the rec
                        if (params.current_rec_id) {
                            Recommendation.findByIdAndUpdate(
                                params.current_rec_id, {
                                    $push: {
                                        rec_dishes: dishDataToSave,
                                        time_sent: Date.now()
                                    },
                                    $set: {
                                        params: paramsToSave
                                    }
                                }, {
                                    upsert: true
                                },
                                recCallback);
                            //Upsert is set to true for safety
                        } else //Otherwise create a new one
                        {
                            Recommendation.create({
                                _user: params.userId,
                                rec_dishes: [dishDataToSave],
                                params: paramsToSave,
                                type: recType,
                                time_sent: [Date.now()]
                            }, recCallback);
                        }
                    } else {
                        recCallback(null, {});
                    }
                });
            }
        } else {
            finalCallback(null, {
                dishes: dishDocuments
            });
        }
    },

    type: Recommendation.type
};