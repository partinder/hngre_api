(function() {
    'use strict';
    var User = require('../../models/user');
    var Merchant = require('../../models/merchant');
    var Predictions = require('../../models/predictions');
    var PredictionMerchants = require('../../models/prediction_merchants');
    var winston = require('../../logger');
    var constants = require('../../constants');
    var Recommendation = require('../../models/recommendation');
    var elasticsearchCommon = require('../elasticsearch-common');
    var ObjectId = require('mongoose').Types.ObjectId;

    var prediction = {
        getUsersByIds: function(userIds, callback) {
            User.find({
                _id: {
                    $in: userIds
                },
                enabled: true
            }, {
                login: 0,
                full_contacts: 0,
                devices: 0
            }, function(err, users) {

                if (err) return callback(err);
                if (!users) return callback(new Error('no users'));

                callback(null, users);
            });
        },

        getDishesByRangeAndDistance: function(query, callback) {
            var condition = prediction.makeQuery(query);
            if (query.current_rec_id) {
                prediction.getCurrentRecommendation(query.current_rec_id, function(err, recommendation) {
                    if (err) {
                        winston.error(err);
                    }
                    if (recommendation && recommendation.dishesToExclude) {
                        condition.dishId = {
                            $nin: recommendation.dishesToExclude
                        };
                    }
                    prediction.getPredictions(condition, query, function(err, merchants) {
                        if (err) {
                            winston.error(err);
                            return callback(err);
                        }
                        prediction.saveGeneratedRecommendatation(query, merchants, function(err, recommendation) {
                            if (err) {
                                winston.error(err);
                                return callback(err);
                            }
                            callback(null, {
                                merchants: merchants,
                                recId: recommendation._id
                            });
                        });
                    });
                });
            } else {
                prediction.getPredictions(condition, query, function(err, merchants) {
                    if (err) {
                        winston.error(err);
                        return callback(err);
                    }
                    prediction.saveGeneratedRecommendatation(query, merchants, function(err, recommendation) {
                        if (err) {
                            winston.error(err);
                            return callback(err);
                        }
                        callback(null, {
                            merchants: merchants,
                            recId: recommendation._id
                        });
                    });
                });
            }
        },

        makeQuery: function(query) {
            var condition = {
                userId: query.userId,
                score: {
                    $gte: query.range.min,
                    $lt: query.range.max
                },
                veg_type: {
                    $in: query.user.veg_scale
                }
            };
            if (query.lat && query.lon) {
                condition.location = {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [
                                query.lat, query.lon
                            ]
                        },
                        $minDistance: query.distance.min
                    }
                };
            } else if (query.country && query.region && query.locality && (query.neighbourhood || query.zip)) {
                condition["address.country"] = query.country;
                condition["address.region"] = query.region;
                condition["address.locality"] = query.locality;
                condition["$or"] = [{
                    "address.neighbourhood": query.neighbourhood
                }, {
                    "address.zip": query.zip
                }]
            } else {
                winston.error("query not found");
            }
            return condition;
        },

        getPredictions: function(condition, query, callback) {
            var total = constants.merchants.size;
            var merchants = [];

            function fetch() {
            	console.log("condition : ", JSON.stringify(condition));
                Predictions.find(condition).limit(total).exec(function(err, dishes) {
                    if (err) {
                        winston.error(err);
                        return callback(err);
                    }
                    if (!dishes || !dishes.length || !(dishes.length > 0)) {
                        condition.score["$lt"] = condition.score["$gte"];
                        condition.score["$gte"] = condition.score["$lt"] - constants.range.decrease;
                        total = total - merchants.length;
                        return fetch();
                    }

                    var merchantIds = _.pluck(dishes, 'merchantId');
                    merchantIds = _.uniq(merchantIds);
                    var dishIds = _.pluck(dishes, 'dishId');
                    dishIds = _.uniq(dishIds);
                    elasticsearchCommon.fetchDishesFromMerchant(merchantIds, function(err, ms) {
                        if (err) return callback(err)
                        if (!ms) return callback(null, []);
                        ms = prediction.filterDishesFromMerchants(dishIds, ms);
                        ms = prediction.mergeDishesScores(dishes, ms, query);
                        merchants = _.union(merchants, ms);
                        if (merchants.length < constants.merchants.size) {
                            if (condition.score["$gte"] < constants.range.specific) {
                                if (condition.location && condition.location["$near"] && condition.location["$near"]["$minDistance"]) {
                                    if (condition.location["$near"]["$minDistance"] < query.distance.max) {
                                        condition.score["$lt"] = constants.merchants.max;
                                        condition.score["$gte"] = constants.merchants.min;
                                        condition.location["$near"]["$minDistance"] = condition.location["$near"]["$minDistance"] + query.distance.gap;
                                        total = total - merchants.length;
                                        return fetch();
                                    } else {
                                        return callback(null, merchants);
                                    }
                                }
                            } else {
                                condition.score["$lt"] = condition.score["$gte"];
                                condition.score["$gte"] = condition.score["$lt"] - constants.range.decrease;
                                total = total - merchants.length;
                                return fetch();
                            }
                        } else {
                            return callback(null, merchants);
                        }
                    });
                });
            }
            fetch();
        },

        filterDishesFromMerchants: function(dishIds, merchants) {
            merchants = merchants.filter(function(m) {
                m.dishes = m.dishes.filter(function(d) {
                    var flag = false;
                    for (var i in dishIds) {
                        if (dishIds[i] === d._id) {
                            flag = true;
                            break;
                        }
                    }
                    return flag;
                });
                if (m.dishes && m.dishes.length && m.dishes.length > 0) {
                    return true;
                } else {
                    return false;
                }
            });
            return merchants;
        },

        makeQueryForGroup: function(query) {
            var vegScales = prediction.uniqueVegScale(query.users);
            var condition = {
                userId: query.userId,
                available_veg_type: {
                    $all: vegScales
                }
            };
            condition["dishes.score"] = {
                $gte: query.range.min,
                $lt: query.range.max
            };
            if (query.lat && query.lon) {
                condition.location = {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [
                                query.lat, query.lon
                            ]
                        },
                        $minDistance: 100
                    }
                };
                query.minDistance = 100;
            } else if (query.country && query.region && query.locality && (query.neighbourhood || query.zip)) {
                condition["address.country"] = query.country;
                condition["address.region"] = query.region;
                condition["address.locality"] = query.locality;
                condition["$or"] = [{
                    "address.neighbourhood": query.neighbourhood
                }, {
                    "address.zip": query.zip
                }]
            } else {
                winston.error("query not found");
            }
            return condition;
        },
        getMerchantsByRangeAndDistance: function(query, callback) {
            var condition = prediction.makeQueryForGroup(query);
            if (query.current_rec_id) {
                prediction.getCurrentRecommendation(query.current_rec_id, function(err, recommendation) {
                    if (err) {
                        winston.error(err);
                    }
                    if (recommendation && recommendation.dishesToExclude) {
                        condition.dishId = {
                            $nin: recommendation.dishesToExclude
                        };
                    }
                    prediction.getMerchantPredictions(condition, query, function(err, merchants) {
                        if (err) {
                            winston.error(err);
                            return callback(err);
                        }
                        prediction.saveGeneratedRecommendatation(query, merchants, function(err, recommendation) {
                            if (err) {
                                winston.error(err);
                                return callback(err);
                            }
                            callback(null, {
                                merchants: merchants,
                                recId: recommendation._id
                            });
                        });
                    });
                });
            } else {
                prediction.getMerchantPredictions(condition, query, function(err, merchants) {
                    if (err) {
                        winston.error(err);
                        return callback(err);
                    }
                    prediction.saveGeneratedRecommendatation(query, merchants, function(err, recommendation) {
                        if (err) {
                            winston.error(err);
                            return callback(err);
                        }
                        callback(null, {
                            merchants: merchants,
                            recId: recommendation._id
                        });
                    });
                });
            }
        },

        getMerchantPredictions: function(condition, query, callback) {
            var total = constants.merchants.size;
            var merchants = [];

            function fetch() {
                console.log("condition : ", JSON.stringify(condition));
                PredictionMerchants.find(condition).limit(total).exec(function(err, _prediction) {
                    if (err) {
                        winston.error(err);
                        return callback(err);
                    }
                    if (!_prediction || !_prediction.length || !(_prediction.length > 0)) {
                        condition["dishes.score"]["$lt"] = condition["dishes.score"]["$gte"];
                        condition["dishes.score"]["$gte"] = condition["dishes.score"]["$lt"] - constants.range.decrease;
                        total = total - merchants.length;
                        return fetch();
                    }

                    var merchantIds = _.pluck(_prediction, 'merchantId');
                    var merchantIds = _.uniq(merchantIds);
                    var pred = prediction.filterDishesForGroups(_prediction, condition);
                    _prediction = pred._prediction;
                    var dishes = pred.dishes;
                    var dishIds = _.pluck(dishes, 'dishId');
                    dishIds = _.uniq(dishIds);
                    elasticsearchCommon.fetchDishesFromMerchant(merchantIds, function(err, ms) {
                        if (err) return callback(err)
                        if (!ms) return callback(null, []);
                        ms = prediction.filterDishesFromMerchants(dishIds, ms);
                        ms = prediction.mergeDishesScores(dishes, ms, query);
                        merchants = _.union(merchants, ms);
                        if (merchants.length < constants.merchants.size) {
                            if (condition["dishes.score"]["$gte"] < constants.range.specific) {
                                if (condition.location && condition.location["$near"] && condition.location["$near"]["$minDistance"]) {
                                    if (condition.location["$near"]["$minDistance"] < query.distance.max) {
                                        condition["dishes.score"]["$lt"] = constants.merchants.max;
                                        condition["dishes.score"]["$gte"] = constants.merchants.min;
                                        condition.location["$near"]["$minDistance"] = condition.location["$near"]["$minDistance"] + query.distance.gap;
                                        total = total - merchants.length;
                                        return fetch();
                                    } else {
                                        return callback(null, merchants);
                                    }
                                } else {
                                    prediction.getOthersPredictions(query, merchants, function(err, data) {
                                        merchants = prediction.merseScoresOfUsers(data, merchants, query);
                                        return callback(null, merchants);
                                    });
                                }
                            } else {
                                condition["dishes.score"]["$lt"] = condition["dishes.score"]["$gte"];
                                condition["dishes.score"]["$gte"] = condition["dishes.score"]["$lt"] - constants.range.decrease;
                                total = total - merchants.length;
                                return fetch();
                            }
                        } else {
                            prediction.getOthersPredictions(query, merchants, function(err, data) {
                                merchants = prediction.merseScoresOfUsers(data, merchants, query);
                                return callback(null, merchants);
                            });
                        }
                    });
                });
            }
            fetch();
        },

        getOthersPredictions: function(query, merchants, callback) {
            var users = query.users.filter(function(user) {
                if (user._id.toString() === query.userId) {
                    return false;
                } else {
                    return true;
                }
            });
            var data = {};
            var total = users.length;
            var merchantIds = _.pluck(merchants, "_id");
            merchantIds = _.uniq(merchantIds);
            function fetch() {
                var condition = {
                    userId: users[total - 1]._id.toString(),
                    merchantId: {
                        $in: merchantIds
                    }
                };
                PredictionMerchants.find(condition).exec(function(err, _prediction) {
                    if (err) {
                        console.log(err);
                        return callback(err)
                    }
                    if (_prediction) {
                        _prediction = prediction.filterDishesVegScale(_prediction, users[total - 1].veg_scale);
                        data[users[total - 1]._id.toString()] = _prediction;
                    }
                    if (--total) {
                        fetch();
                    } else {
                        return callback(null, data);
                    }
                });
            }
            if (total) {
                fetch();
            } else {
                callback(null, data);
            }
        },

        filterDishesVegScale: function(merchants, vegScale) {
            merchants = merchants.map(function(pd) {
                pd.dishes = pd.dishes.filter(function(d) {
                    var veg_type = d.veg_type ? d.veg_type : d._doc.veg_type;
                    if (~vegScale.indexOf(veg_type)) {
                        return true;
                    } else {
                        return false;
                    }
                });
                return pd;
            });
            return merchants;
        },

        merseScoresOfUsers: function(data, merchants, query) {
            merchants = merchants.map(function(merchant) {
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        var user = null;
                        query.users.forEach(function(u) {
                            if (u._id.toString() === key) {
                                user = u;
                            }
                        });
                        var vegScale = [1, 2, 3];
                        if (user) {
                            vegScale = user.veg_scale;
                            console.log("filtering dishes", vegScale);
                        } else {
                            console.log("not filtering dishes");
                        }
                        for (var m in data[key]) {
                            if (data[key][m].merchantId === merchant._id) {
                                merchant.dishes = merchant.dishes.map(function(d) {
                                    for (var i in data[key][m].dishes) {
                                        if (d._id === data[key][m].dishes[i].dishId) {
                                            var vegtype = data[key][m].dishes[i].veg_type ? data[key][m].dishes[i].veg_type : data[key][m].dishes[i]._doc.veg_type;
                                            if (~vegScale.indexOf(vegtype)) {
                                                d.scores[key] = data[key][m].dishes[i].score;
                                                //break;
                                            }
                                            console.log(vegScale.indexOf(vegtype), vegtype);
                                        }
                                    }
                                    return d;
                                });
                            }
                        }
                    }
                }
                return merchant;
            });
            return merchants;
        },

        filterDishesForGroups: function(_prediction, condition) {
            var dishes = [];
            _prediction = _prediction.map(function(pd) {
                pd.dishes = pd.dishes.filter(function(d) {
                    var veg_type = d.veg_type ? d.veg_type : d._doc.veg_type;
                    if (~condition.available_veg_type["$all"].indexOf(veg_type)) {
                        return true;
                    } else {
                        return false;
                    }
                });
                dishes = _.union(pd.dishes, dishes);
                return pd;
            });
            return {
                dishes: dishes,
                _prediction: _prediction
            };
        },

        mergeDishesScores: function(dishes, merchants, query) {
            merchants = merchants.map(function(m) {
                m.dishes = m.dishes.map(function(d) {
                    var score, scores;
                    for (var i in dishes) {
                        if (dishes[i].dishId === d._id) {
                            score = dishes[i].score;
                            scores = dishes[i].scores;
                            break;
                        }
                    }
                    if (scores) {
                        d.scores = scores;
                    } else if (score) {
                        d.scores = {};
                        d.scores[query.userId] = score;
                    }
                    return d;
                });
                return m;
            });
            return merchants;
        },

        getCurrentRecommendation: function(currentRecId, callback) {
            Recommendation.findOne({
                _id: currentRecId
            }, function(err, recommendation) {
                if (err) return callback(null, [])
                if (!recommendation) return callback(null, []);
                var _recommendation = {
                    dishesToExclude: [],
                    merchantsToExclude: []
                };

                recommendation.rec_merchants.forEach(function(rec_merchant) {
                    if (_.isArray(rec_merchant)) {
                        rec_merchant.forEach(function(m) {
                            _recommendation.dishesToExclude = _.uniq(_recommendation.dishesToExclude, _.pluck(m._dishes, '_dish'));
                            _recommendation.merchantsToExclude.push(m._merchant);
                        });
                    } else {
                        _recommendation.merchantsToExclude.push(rec_merchant._merchant);
                        _recommendation.dishesToExclude = _.uniq(_recommendation.dishesToExclude, _.pluck(rec_merchant._dishes, '_dish'));
                    }

                });

                if (!_.isArray(_recommendation.merchantsToExclude)) return callback(null, []);
                if (!_.isArray(_recommendation.dishesToExclude)) return callback(null, []);

                winston.info('Total previous recommended dishes is %d', _recommendation.dishesToExclude.length);
                winston.info('Total previous recommended merchcants is %d', _recommendation.merchantsToExclude.length);

                callback(null, _recommendation);

            });
        },

        saveGeneratedRecommendatation: function(query, merchants, callback) {
            var merchantDataToSave = [];
            merchants.forEach(function(m) {
                var dishes = [];
                m.dishes.forEach(function(d) {
                    dishes.push({
                        scores: d.scores,
                        _dish: d._id
                    });
                });
                merchantDataToSave.push({
                    _merchant: m._id,
                    _dishes: dishes
                });
            });
            var paramsToSave = _.pick(query, "lat", "lon", "distance", "country", "region", "locality", "neighbourhood", "zip");
            //If rec id exists, update the rec
            if (query.current_rec_id) {
                Recommendation.findByIdAndUpdate(
                    query.current_rec_id, {
                        $push: {
                            rec_merchants: merchantDataToSave,
                            time_sent: Date.now()
                        },
                        $set: {
                            params: paramsToSave
                        }
                    }, {
                        upsert: true
                    },
                    callback);
                //Upsert is set to true for safety
            } else {
                //Otherwise create a new one
                Recommendation.create({
                    _user: query.userId,
                    rec_merchants: [merchantDataToSave],
                    params: paramsToSave,
                    type: Recommendation.type.RECOMMENDATION,
                    time_sent: [Date.now()]
                }, callback);
            }
        },

        uniqueVegScale: function(users) {
            var veg_scale = [];
            users.forEach(function(user) {
                switch (user.veg_scale.length) {
                    case 1:
                        {
                            veg_scale.push(user.veg_scale[0]);
                        };
                        break;
                    case 2:
                        {
                            veg_scale.push(user.veg_scale[0]);
                            veg_scale.push(user.veg_scale[1]);
                        };
                        break;
                }
            });
            veg_scale = _.uniq(veg_scale);
            if (veg_scale.length && veg_scale.length < 1) {
                veg_scale = [1, 2, 3];
            }
            return veg_scale;
        }
    };
    module.exports = prediction;
}())