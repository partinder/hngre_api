/*
    SEARCH
    Methods and algorithms for searching from the Hngre MongoDB datastore.
 */
"use strict";

var Merchant = require('../../models/merchant');
var Dish = require('../../models/dish');
var Async = require('async');
var winston = require('../../logger');
var searchHelper = require('./search-helper');

var desiredStatus = 'completed';

function checkAndBuildGeoQuery (params, queryType) {

    var locationFields = _.pick(params, 'lon', 'lat', 'neighbourhood', 'locality', 'city', 'country');

    var geoQuery = {};

    if (locationFields.lon && locationFields.lat) {

        if (queryType === 'aggregation')
        {

        }
        else
        {
            geoQuery.location = {
                $nearSphere: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [parseFloat(params.lon), parseFloat(params.lat)]
                    },
                    $maxDistance: 2000 //Needs to be adjusted
                }
            }
        }
    }
    else if (locationFields.neighborhood || locationFields.locality || locationFields.city || locationFields.country)
    {
        for (var key in locationFields)
        {
            var newKey = "address." + key;
            locationFields[newKey] = locationFields[key];
            delete locationFields[key];
        }

        _.extend(geoQuery, locationFields);
    }

    return geoQuery;
}

/**
  * Queries for unique names, addresses, dishes, cuisines and ingredients (based on an incomplete entry) to provide the user with suggestions
  *
  *
  */
var TypeAhead = function (params, finalCallback) {

    //Get search term and search for existence in merchant.name, merchant.address

    var termQuery = {};

    var matchFromBeginning = new RegExp('\\b' + params.term, 'i');

    if (params.term) {
        termQuery.name = matchFromBeginning;
        termQuery.status = desiredStatus;
    } else {
        return finalCallback(new Error('Term for type ahead is missing'));
    }

    if (!params.scope) params.scope = searchHelper.completeScope;
    else params.scope = params.scope.split(',');

    var geoQuery = checkAndBuildGeoQuery(params);

    _.extend(termQuery, geoQuery);

    var parallelQueries = {};

    //If scope includes merchant
    if (params.scope.indexOf('merchant') > -1) {

        //Build query for searching merchants
        parallelQueries.outlet = function (callback) {

            Merchant.distinct('name', termQuery, function (err, results) {
                if (err) {

                    winston.error('Error getting merchant names for type ahead', {term: params.term});
                    return callback(err);
                }
                else {

                    if (results.length > 0) callback(null, results);
                    else callback (null, undefined);
                }
            });
        }
    }

    //If scope includes address
    if (params.scope.indexOf('address') > -1)
    {

        //Build query for searching neighborhoods
        parallelQueries.neighborhood = function (callback)
        {
            Merchant.aggregate([
                {$match: {status: desiredStatus}},
                {$project:{_id:0, 'neighbourhood': '$address.neighbourhood'}},
                {$unwind:'$neighbourhood'},
                {$match:{'neighbourhood': matchFromBeginning}},
                {$group:{_id:null, neighbourhoods:{$addToSet:'$neighbourhood'}}}

            ], function (err, result) {

                if (err) {

                    winston.error('Error aggregating neighbourhood for type ahead', {term: params.term});
                    return callback(err);

                } else {

                    var neighbourhoods = [];
                    if (result.length > 0) neighbourhoods = result[0].neighbourhoods;
                    if (neighbourhoods.length <= 0) neighbourhoods = undefined;
                    callback(null, neighbourhoods);
                }
            });
        };

        //Build query for searching for zip codes
        parallelQueries.zip = function (callback) {

            Merchant.aggregate([
                {$match: {status: desiredStatus}},
                {$match:{'address.zip':matchFromBeginning}},
                {$project:{'zip': '$address.zip', _id:0}},
                {$group:{_id:null,'zipCodes':{$addToSet:'$zip'}}}

            ], function (err, result) {

                if (err) {

                    winston.error('Error aggregating zip codes for type ahead', {term: params.term});
                    return callback(err);
                }

                var zipCodes = [];
                if (result.length > 0) zipCodes = result[0].zipCodes;
                if (zipCodes.length <= 0) zipCodes = undefined;
                callback(null, zipCodes);
            });
        };
    }

    //If scope includes cuisine
    if (params.scope.indexOf('cuisine') > -1) {

        //Build query for searching cuisines
        parallelQueries.cuisine =  function (callback) {

            Merchant.aggregate([
                {$match: {status: desiredStatus}},
                {$project:{_id: 0, cuisine: 1}},
                {$unwind:'$cuisine'},
                {$match:{'cuisine': matchFromBeginning}},
                {$group:{_id:null, cuisines:{$addToSet:{$toLower: '$cuisine'}}}}

            ], function (err, result)
            {

                if (err)
                {
                    winston.error('Error aggregating cuisines for type ahead', {term: params.term});
                    return callback(err);
                }
                else
                {
                    var cuisines = [];
                    if (result.length > 0) cuisines = result[0].cuisines;
                    if (cuisines.length <= 0) cuisines = undefined;
                    callback(null, cuisines);

                }
            });
        }
    }


    if (params.scope.indexOf('dish') > -1)
    {
        //Search for dishes who either belong to matching merchants or whose name match the search term.

        parallelQueries.dish = function (callback) {
            Merchant.aggregate ([
                {$match: {status: desiredStatus}},
                {$project:{_id: 0, 'dishes.name': 1, 'dishes.approve_status': 1}},
                {$unwind:'$dishes'},
                {$match:{'dishes.name': matchFromBeginning, 'dishes.approve_status': true}},
                {$group:{_id:null, dishes:{$push:'$dishes.name'}}}

            ], function (err, result) {

                if (err) {
                    winston.error('Error getting dish names for type ahead', {term: params.term});
                    return finalCallback(err);
                }

                var dishes = [];
                if (result.length > 0) dishes = result[0].dishes;
                if (dishes.length <= 0) dishes = undefined;
                callback(null, dishes);

            })
        }
    }

    if (params.scope.indexOf('ingredients') > -1)
    {
        //Search for dishes who either belong to matching merchants or whose name match the search term.

        parallelQueries.ingredients = function (callback) {
            Merchant.aggregate ([
                {$match: {status: desiredStatus}},
                {$project:{_id: 0, 'ingredients': '$dishes.ingredients'}},
                {$unwind:'$ingredients'},
                {$unwind: '$ingredients'},
                {$match:{'ingredients': matchFromBeginning}},
                {$group:{_id:null, ingredients:{$addToSet: {$toLower: '$ingredients'}}}}

            ], function (err, result) {

                if (err) {
                    winston.error('Error getting dish names for type ahead', {term: params.term});
                    return finalCallback(err);
                }

                var ingredients = [];
                if (result.length > 0) ingredients = result[0].ingredients;
                if (ingredients.length <= 0) ingredients = undefined;
                callback(null, ingredients);

            })
        }
    }

    if (params.scope.indexOf('tag') > -1)
    {
        parallelQueries.dishTag = function (callback) {

            Merchant.aggregate ([
                {$match: {status: desiredStatus}},
                {$project: {_id: 0, tags: 1}},
                {$unwind: '$tags'},
                {$match: {'tags': matchFromBeginning}},
                {$group: {_id: null, tags: {$addToSet:{$toLower: '$tags'}}}}
            ], function (err, result) {

                if (err) {
                    winston.error('Error getting dish tags for type ahead', {term: params.term});
                    return callback(err);
                }

                var tags = [];
                if (result.length > 0) tags = result[0].tags;
                if (tags.length <= 0) tags = undefined;
                callback (null, tags);
            })
        }

        parallelQueries.merchantTag = function (callback) {

            Merchant.aggregate ([
                {$match: {status: desiredStatus}},
                {$project: {_id: 0, tags: '$dishes.tags'}},
                {$unwind: '$tags'},
                {$unwind: '$tags'},
                {$match: {'tags': matchFromBeginning}},
                {$group: {_id: null, tags: {$addToSet: { $toLower : '$tags'}}}}
            ], function (err, result) {

                if (err) {
                    winston.error('Error getting dish tags for type ahead', {term: params.term});
                    return callback(err);
                }

                var tags = [];
                if (result.length > 0) tags = result[0].tags;
                if (tags.length <= 0) tags = undefined;
                callback (null, tags);
            })
        }
    }

    Async.parallel(parallelQueries, function (err, results) {

        if (err) {

            return finalCallback(err);

        } else {

            if (params.scope.indexOf('address') > -1) {

                results.location = _.union(results.neighborhood, results.zip);
                if (results.location.length <= 0) results.address = undefined;
            }

            delete results.neighborhood;
            delete results.zip;

            var keys = _.keys(results);
            
            var finalResult = [];

            keys.forEach( function (key) {

                var arrTerms = results[key];

                if (arrTerms !== undefined)
                {
                    arrTerms.forEach(function (term) {

                        var scope = searchHelper.scopeMap[key];
                        if (!scope) scope = key;

                        var category = searchHelper.keyMap[key];
                        if (!category) category = key;

                        finalResult.push({
                            term: term,
                            type: category,
                            scope: scope
                        });
                    });
                }
            });

            //Sort the array on basis of relevance

            var termLower = params.term.toLowerCase();

            finalResult.sort(
                function (first, second) {

                    var comparisonResult;

                    var idxFirst = first.term.toLowerCase().indexOf(termLower);
                    var idxSecond = second.term.toLowerCase().indexOf(termLower);

                    comparisonResult = idxFirst - idxSecond;

                    if (comparisonResult === 0)
                    {
                        comparisonResult = searchHelper.scopePreferences[first.scope] - searchHelper.scopePreferences[second.scope];
                    }

                    return comparisonResult;
                }
            );

            if (finalResult.length > 20) finalResult.length = 20;

            finalCallback(null, finalResult);

        }
    });
    //db.sentences.find({$text:{$search:"some string"}}, {score:{$meta: 'textScore'}}.sort({score:{$meta:'textScore'}}))
}


/**
  * Performs a search of a term based on the scope. 
  *
  *
  */
var Search = function (params, callback) {

    //Keywords in search should be searched for in restaurant, dish, ingredient, address, etc (everything possible)
    //The particular field in which the keyword is found will be shown as an icon in the results. Alternatively, it can be highlighted.
    //Get scope to add respective queries to the flow.

    if (!params.term) return callback(new Error('Search term is missing'));

    if (!params.scope) params.scope = searchHelper.completeScope;
    else params.scope = params.scope.split(',');

    performMerchantSearch(params, callback);
}

/*
 INTERNAL USE
 */
function performMerchantSearch (params, callback) {

    //Create a regular expression with the search term

    var termRegex = new RegExp('\\b'+params.term, 'i'); //Case insensitive, beginning of each word

    var merchantGeoQuery = checkAndBuildGeoQuery(params);

    var arrMerchantQuery = [];
    var arrDishQuery = [];

    if (params.scope.indexOf('merchant') > -1) {

        arrMerchantQuery.push({name: termRegex});
        arrDishQuery.push({'merchant.name': termRegex});
    }

    if (params.scope.indexOf('address') > -1) {

        arrMerchantQuery.push({'address.zip' : termRegex}, {'address.neighbourhood' : termRegex});
        arrDishQuery.push({'merchant.address': termRegex});
    }

    if (params.scope.indexOf('cuisine') > -1) {

        arrMerchantQuery.push({cuisine: termRegex});
        arrDishQuery.push({'dish.cuisine': termRegex});
    }

    if (params.scope.indexOf('dish') > -1) {

        arrMerchantQuery.push({'dishes.name': termRegex});
        arrDishQuery.push({'dish.name': termRegex});
    }

    if (params.scope.indexOf('ingredients') > -1) {
        arrMerchantQuery.push({'dishes.ingredients': termRegex});
        arrDishQuery.push({'dish.ingredients': termRegex});
    }

    if (params.scope.indexOf('tag') > -1)
    {
        arrMerchantQuery.push({'tags': termRegex}, {'dishes.tags': termRegex});
        arrDishQuery.push({'dish.tags': termRegex});
    }

    var merchantQuery = {status: "completed"};

    if (arrMerchantQuery.length > 0) {
        merchantQuery.$or = arrMerchantQuery;
    }

    _.extend(merchantQuery, merchantGeoQuery);

    console.log(merchantQuery);

    var aggregate = [];

    if (_.keys(merchantGeoQuery).length > 0)
    {
        aggregate.push({$geoNear: merchantGeoQuery});
    }

    aggregate.push (
        {$match : merchantQuery},
        {$project: {_id: 0, 'dish': '$dishes', 'merchant': '$$CURRENT'}},
        {$unwind: '$dish'},
        {$match: {"dish.approve_status": true}}
    );

    console.log(aggregate);

    if (arrDishQuery.length > 0)
    {
        aggregate.push({$match: {$or: arrDishQuery}});
    }

    aggregate.push({$limit: 50});

    winston.info('Merchant search query', JSON.stringify(aggregate));

    if (Object.keys(merchantQuery).length > 0) {

        var query = Merchant.aggregate(aggregate);
        query.exec(function (err, results) {

            if (err) {
                winston.error('Error in search', {query: merchantQuery});
                return callback(err);
            }

            var returnResult = [];

            results.forEach(function (result) {

                var dish = result.dish;
                delete result.merchant.dishes;
                dish._merchant = result.merchant;
                returnResult.push(dish);
            });

            callback (null, returnResult);
        });

    } else {

        callback (new Error('No scope selected for search'));
    }
}

function getLocalities (params, callback)
{
    var matchObject = {status: 'completed'};

    if (params.country) matchObject['address.country'] = params.country;
    if (params.region) matchObject['address.region'] = params.region;

    Merchant.aggregate([
        {$match: matchObject},
        {$group: {_id: '$address.country', localities: {$addToSet: {locality: '$address.locality', region: '$address.region'}}}},
        {$project: {_id: 0, country: '$_id', localities: 1}}

    ], function (err, result) {

        if (err)
        {
            return callback(err);
        }
        else
        {
            return callback(null, result);
        }
    });
}

/*
    EXPORT
 */
module.exports = {
    searchFor: Search,
    typeahead: TypeAhead,
    localities: getLocalities
};
