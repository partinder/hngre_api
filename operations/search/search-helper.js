/*
 SEARCH HELPER
 Methods and objects common to all types of search.
 */

module.exports = {
    completeScope : ['merchant', 'address', 'dish', 'cuisine', 'ingredient', 'tag'],
    scopeMap : {
        outlet: 'merchant',
        restaurant: 'merchant',
        zip: 'address',
        neighborhood: 'address',
        location: 'address',
        dishTag: 'tag',
        merchantTag: 'tag'
    },
    keyMap : {
        dishTag: 'dishes',
        merchantTag: 'outlets'
    },
    scopePreferences : {
        dish: 3,
        cuisine: 1,
        merchant: 4,
        address: 2,
        ingredient: 5,
        tag: 6
    }
};
