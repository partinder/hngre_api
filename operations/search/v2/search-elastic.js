(function() {
    'use strict';
    /**
     * Module dependencies;
     */
    var ElasticSearch = require('../../elasticsearch-common');
    var winston = require('../../../logger');
    var constants = require('../../../constants');
    var error = require('../../../error');

    /**
     * Get unique veg scale preferences
     *
     * @method     uniqueVegScale
     * @param      {<type>}  users   { description }
     * @return     {Array}   { description_of_the_return_value }
     */
    function uniqueVegScale(users) {
        var veg_scale = [];
        users.forEach(function(user) {
            switch (user.veg_scale.length) {
                case 1:
                    {
                        veg_scale.push(user.veg_scale[0]);
                    };
                    break;
                case 2:
                    {
                        veg_scale.push(user.veg_scale[0]);
                        veg_scale.push(user.veg_scale[1]);
                    };
                    break;
            }
        });
        veg_scale = _.uniq(veg_scale);
        if (!veg_scale.length || veg_scale.length == 0) {
            veg_scale = [1, 2, 3];
        }
        return veg_scale;
    }

    var search = {};

    /**
     * Fetch merchants
     *
     * @method     fetchIncremental
     * @param      {<type>}    merchantArray  { description }
     * @param      {number}    history    { description }
     * @param      {<type>}    query      { description }
     * @param      {Function}  callback   { description }
     */
    search.fetchIncremental = function(merchantArray, history, query, callback) {
        if (query.lat && query.lon) {
            try {
                if (query.distance) {
                    query.distance += constants.distance.gap;
                } else if (history.prev_recs && history.prev_recs.params && history.prev_recs.params.distance) {
                    query.distance = history.prev_recs.params.distance;
                } else {
                    throw "No distance in query";
                }
            } catch (err) {
                query.distance = constants.distance.gap
            }

            winston.info('Fetching recs at distance', query.distance);
        }

        var filters = ElasticSearch.createFilterFromParamsForMerchant(query, 'search');

        if (!filters) return callback(new Error('Filter is missing'));
        var merchantsToExclude = _.pluck(merchantArray, '_id');
        merchantsToExclude = _.union(merchantsToExclude, history.prev_recs.merchantsToExclude);
        merchantsToExclude = _.uniq(merchantsToExclude);
        console.log('merchantsToExclude merchants : ', merchantsToExclude);
        if (!filters.bool) {
            var geoDistance = filters.geo_distance;
            delete filters.geo_distance;

            filters.bool = {
                must: [{
                    geo_distance: geoDistance
                }],
                must_not: [{
                    ids: {
                        values: merchantsToExclude
                    }
                }]
            };
        } else {
            filters.bool.must_not = [{
                ids: {
                    values: merchantsToExclude
                }
            }];
        }

        //Apply user's veg preferences
        var veg_scale = uniqueVegScale(history.users);
        if (veg_scale.length > 0) {
            if (!filters.bool.must) filters.bool.must = [];

            filters.bool.must = filters.bool.must.concat({
                query: {
                    filtered: {
                        filter: {
                            terms: {
                                available_veg_type: veg_scale,
                                execution: "or",
                                _cache: true
                            }
                        }
                    }
                }
            });
        }

        var searchBody = ElasticSearch.createSearchBodyForMerchant(filters, {
            _script: {
                script_file: 'random',
                type: 'number',
                params: {},
                order: 'asc'
            }
        }, query.size - merchantArray.length);
        if (!searchBody.query) {
            searchBody.query = {
                nested: {
                    path: "dishes",
                    query: {
                        bool: {
                            must: [{
                                filtered: {
                                    filter: {
                                        terms: {
                                            "dishes.veg_type": veg_scale,
                                            execution: "or",
                                            _cache: true
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    inner_hits: {
                        "size": constants.dishes.innerDishes
                    }
                }
            }
        }
        winston.info('Elastic search query body is', searchBody);
        ElasticSearch.client().search(searchBody, function(err, result) {

            if (err) {
                callback(err);
                return error.report(err,searchBody);                
            } else {
                var recMerchants = [];

                if (result.hits.hits.length > 0) {
                    recMerchants = ElasticSearch.parseDishDocument(result.hits.hits);

                    merchantArray = merchantArray.concat(recMerchants);

                    if (merchantArray.length < constants.merchants.size && query.distance && query.distance < constants.distance.max) {
                        if (!history.prev_recs) {
                            history.prev_recs = {};
                            history.prev_recs.prevRecs = []
                        }

                        recMerchants.forEach(function(merchant) {
                            history.prev_recs.prevRecs.push(merchant._id);
                        });
                    }
                }

                callback(null, recMerchants);
            }
        });
    }

    module.exports = search;
}())
