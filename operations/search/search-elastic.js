/*
 SEARCH - ELASTIC
 Methods and algorithms for searching from the Hngre ElasticSearch server.
 */

var request = require('request');
var ElasticSearch = require('../elasticsearch-common');
var winston = require('../../logger');
var constants = require('../../constants');
var error = require('../../error');

var kTypeSuggest = 'suggest',
    kTypeSearch = 'search';

/**
 * Get unique veg scale preferences
 *
 * @method     uniqueVegScale
 * @param      {<type>}  users   { description }
 * @return     {Array}   { description_of_the_return_value }
 */
function uniqueVegScale(users) {
    var veg_scale = [];
    users.forEach(function(user) {
        switch (user.veg_scale.length) {
            case 1:
                {
                    veg_scale.push(user.veg_scale[0]);
                };
                break;
            case 2:
                {
                    veg_scale.push(user.veg_scale[0]);
                    veg_scale.push(user.veg_scale[1]);
                };
                break;
        }
    });
    veg_scale = _.uniq(veg_scale);
    if (!veg_scale.length || veg_scale.length == 0) {
        veg_scale = [1, 2, 3];
    }
    return veg_scale;
}

module.exports = {

    searchFor: function(params, callback) {
        var searchBody = {
            index: "hngre",
            type: "dishes",
            body: {
                query: {
                    filtered: {
                        query: {
                            match: {

                            }
                        }
                    }
                },
                from: params.offset
            }
        };

        var scopeTerm = {};
        scopeTerm[params.scope] = {
            query: params.term,
            operator: 'and'
        };

        searchBody.body.query.filtered.query.match = scopeTerm;

        var filter = ElasticSearch.createFilterFromParams(params, kTypeSearch);

        if (!filter) return callback(new Error('Filter is missing'));

        searchBody.body.query.filtered.filter = filter;

        var client = ElasticSearch.client();

        client.search(searchBody, function(err, result) {
            if (err) {
                callback(err);
                return error.report(err, searchBody);
            } else {
                var reply = [];

                if (result.hits.hits.length > 0) {
                    reply = ElasticSearch.parseDishDocument(result.hits.hits);
                } else {
                    winston.info(result);
                }

                winston.info(reply);

                callback(null, reply);
            }
        });
    },

    searchForMerchant: function(params, callback) {
        var searchBody = {
            index: "hngre",
            type: "merchants",
            body: {
                size: constants.merchants.size,
                sort: {
                    _script: {
                        script_file: 'random',
                        type: 'number',
                        params: {},
                        order: 'asc'
                    }
                },
                query: {
                    filtered: {
                        query: {}
                    }
                },
                from: params.offset
            }
        };

        var scopeTerm = {};
        var flag = false;
        console.log(params.scope);
        var veg_scale = [1, 2, 3];
        if (params.user) {
            veg_scale = params.user.veg_scale;
        }
        if (params.users) {
            veg_scale = uniqueVegScale(params.users);
        }

        params.dishesToExclude = params.dishesToExclude.filter(function(d) {
            if (d) {
                return true;
            } else {
                return false;
            }
        });

        switch (params.scope) {
            case 'dishes.name':
                {
                    scopeTerm.nested = {
                        "path": "dishes",
                        "query": {
                            "bool": {
                                "must": [{
                                    "query_string": {
                                        "default_field": "dishes.name",
                                        "query": params.term,
                                        "default_operator": "AND"
                                    }
                                }],
                                "must_not": [{
                                    "terms": {
                                        "dishes._id": params.dishesToExclude
                                    }
                                }]
                            }
                        },
                        "inner_hits": {
                            "size": constants.dishes.innerDishes
                        }
                    };
                    flag = true;
                };
                break;
            case 'dishes.cuisine':
                {
                    scopeTerm.nested = {
                        "path": "dishes",
                        "query": {
                            "bool": {
                                "must": [{
                                    "query_string": {
                                        "default_field": "dishes.cuisine",
                                        "query": params.term,
                                        "default_operator": "AND"
                                    }
                                }],
                                "must_not": [{
                                    "terms": {
                                        "dishes._id": params.dishesToExclude
                                    }
                                }]
                            }
                        },
                        "inner_hits": {
                            "size": constants.dishes.innerDishes
                        }
                    };
                    flag = true;
                };
                break;
            case 'dishes.ingredients':
                {
                    scopeTerm.nested = {
                        "path": "dishes",
                        "query": {
                            "bool": {
                                "must": [{
                                    "query_string": {
                                        "default_field": "dishes.ingredients",
                                        "query": params.term,
                                        "default_operator": "AND"
                                    }
                                }],
                                "must_not": [{
                                    "terms": {
                                        "dishes._id": params.dishesToExclude
                                    }
                                }]
                            }
                        },
                        "inner_hits": {
                            "size": constants.dishes.innerDishes
                        }
                    };
                    flag = true;
                };
                break;
            case 'dishes.tags':
                {
                    scopeTerm.nested = {
                        "path": "dishes",
                        "query": {
                            "bool": {
                                "must": [{
                                    "query_string": {
                                        "default_field": "dishes.tags",
                                        "query": params.term,
                                        "default_operator": "AND"
                                    }
                                }],
                                "must_not": [{
                                    "terms": {
                                        "dishes._id": params.dishesToExclude
                                    }
                                }]
                            }
                        },
                        "inner_hits": {
                            "size": constants.dishes.innerDishes
                        }
                    };
                    flag = true;
                };
                break;
            default:
                {
                    scopeTerm.match = {};
                    scopeTerm.match[params.scope] = {
                        query: params.term,
                        operator: 'and'
                    };
                }
        }

        searchBody.body.query.filtered.query = scopeTerm;

        var filter = ElasticSearch.createFilterFromParamsForMerchant(params, kTypeSearch);

        if (!filter) return callback(new Error('Filter is missing'));

        /*        if (params.merchantsToExclude) {
                    if (!filter.bool.must_not) filter.bool.must_not = [];

                    filter.bool.must_not = filter.bool.must_not.concat({
                        ids: {
                            values: params.merchantsToExclude
                        }
                    });
                }*/

        //Apply user's veg preferences
        /*        if (veg_scale && veg_scale.length <= 3 && veg_scale.length > 0) {
                    if (!filter.bool) filter.bool = {};
                    if (!filter.bool.must) filter.bool.must = [];

                    filter.bool.must = filter.bool.must.concat({
                        terms: {
                            available_veg_type: veg_scale
                        }
                    });
                }*/

        searchBody.body.query.filtered.filter = filter;

        var client = ElasticSearch.client();

        client.search(searchBody, function(err, result) {
            if (err) {
                callback(err);
                return error.report(err, searchBody);
            } else {
                var reply = [];

                if (result.hits.hits.length > 0) {
                    reply = ElasticSearch.parseDishDocument(result.hits.hits);
                } else {
                    winston.info(result);
                }

                winston.info(reply);

                callback(null, reply);
            }
        });
    },

    suggestForMerchant: function(params, callback) {

        var veg_scale = [1, 2, 3];
        if (params.user) {
            veg_scale = params.user.veg_scale;
        }
        if (params.users && params.users.length > 0) {
            veg_scale = uniqueVegScale(params.users);
        }

        var searchBody = {
            index: "hngre",
            type: "merchants",
            body: {
                size: constants.fetchmerchants.size,
                query: {
                    filtered: {
                        query: {
                            nested: {
                                path: "dishes",
                                query: {
                                    bool: {
                                        must: []
                                    }
                                },
                                inner_hits: {
                                    size: constants.dishes.innerDishes
                                }
                            }
                        },
                        filter: {
                            bool: {
                                must_not: [{
                                    ids: {
                                        values: params.merchantIds
                                    }
                                }]
                            }

                        }
                    }
                }
            }
        };

        if (params.lat && params.lon) {
            searchBody.body.sort = [{
                _geo_distance: {
                    location: {
                        lat: params.lat,
                        lon: params.lon
                    },
                    order: "asc",
                    unit: "km",
                    mode: "min",
                    distance_type: "plane"
                }
            }];
            searchBody.body.query.filtered.filter.bool.must = [{
                geo_distance: {
                    distance: constants.distance.max,
                    location: {
                        lat: params.lat,
                        lon: params.lon
                    }
                }
            }];
            searchBody.body.fields = ["_source"];
            searchBody.body.script_fields = {
                distance: {
                    lang: "groovy",
                    params: {
                        lat: params.lat,
                        lon: params.lon
                    },
                    script: "doc['location'].distanceInKm(lat,lon)"
                }
            };
        } else if (params.country && params.region && params.locality) {
            searchBody.body.query.filtered.filter.bool.must = [{
                query: {
                    query_string: {
                        default_field: "address.country",
                        query: params.country,
                        default_operator: "AND"
                    }
                }
            }, {
                query: {
                    query_string: {
                        default_field: "address.region",
                        query: params.region,
                        default_operator: "AND"
                    }
                }
            }];

            if (params.locality != "null" && params.locality != null && params.locality != "undefined" && params.locality != undefined && params.locality != "Delhi NCR") {
                searchBody.body.query.filtered.filter.bool.should = [{
                    query: {
                        query_string: {
                            default_field: "address.locality",
                            query: params.locality,
                            default_operator: "AND"
                        }
                    }
                }];
            }

            if (params.zip) {
                searchBody.query.filtered.filter.bool.must.push({
                    query: {
                        query_string: {
                            default_field: "address.zip",
                            query: params.zip,
                            default_operator: "AND"
                        }
                    }
                });
            } else if (params.neighbourhood) {
                searchBody.body.query.filtered.filter.bool.must.push({
                    query: {
                        query_string: {
                            default_field: "address.neighbourhood",
                            query: params.neighbourhood,
                            default_operator: "AND"
                        }
                    }
                });
            }
        }

        switch (params.scope) {
            case 'dishes.name':
                {
                    var must = [{
                        terms: {
                            "dishes.veg_type": veg_scale
                        }
                    }];
                    if (params.suggestedParams.dishes.cuisine && params.suggestedParams.dishes.cuisine.length > 0) {
                        must.push({
                            terms: {
                                "dishes.cuisine": params.suggestedParams.dishes.cuisine
                            }
                        });
                    }
                    if (params.suggestedParams.dishes.ingredients && params.suggestedParams.dishes.ingredients.length > 0) {
                        must.push({
                            terms: {
                                "dishes.ingredients": params.suggestedParams.dishes.ingredients
                            }
                        });
                    }

                    searchBody.body.query.filtered.query.nested.query.bool.must = must;
                };
                break;
            case 'dishes.tags':
                {
                    var must = [{
                        terms: {
                            "dishes.veg_type": veg_scale
                        }
                    }];
                    if (params.suggestedParams.dishes.cuisine && params.suggestedParams.dishes.cuisine.length > 0) {
                        must.push({
                            terms: {
                                "dishes.cuisine": params.suggestedParams.dishes.cuisine
                            }
                        });
                    }
                    if (params.suggestedParams.dishes.ingredients && params.suggestedParams.dishes.ingredients.length > 0) {
                        must.push({
                            terms: {
                                "dishes.ingredients": params.suggestedParams.dishes.ingredients
                            }
                        });
                    }

                    searchBody.body.query.filtered.query.nested.query.bool.must = must;
                };
                break;
            case 'dishes.ingredients':
                {
                    var must = [{
                        terms: {
                            "dishes.veg_type": veg_scale
                        }
                    }];

                    searchBody.body.query.filtered.query.nested.query.bool.must = must;
                };
                break;
            case 'dishes.cuisine':
                {
                    var must = [{
                        terms: {
                            "dishes.veg_type": veg_scale
                        }
                    }];

                    searchBody.body.query.filtered.query.nested.query.bool.must = must;
                };
                break;
            case 'name':
                {
                    var must = [{
                        terms: {
                            "dishes.veg_type": veg_scale
                        }
                    }];
                    if (params.suggestedParams.dishes.cuisine && params.suggestedParams.dishes.cuisine.length > 0) {
                        must.push({
                            terms: {
                                "dishes.cuisine": params.suggestedParams.dishes.cuisine
                            }
                        });
                    }

                    searchBody.body.query.filtered.query.nested.query.bool.must = must;
                };
                break;
            default:
                {

                }
        }

        var client = ElasticSearch.client();
        winston.info("Query ", searchBody);
        winston.info("Query ", JSON.stringify(searchBody));
        client.search(searchBody, function(err, result) {
            if (err) {
                callback(err);
                return error.report(err, searchBody);
            } else {
                var reply = [];

                if (result.hits.hits.length > 0) {
                    reply = ElasticSearch.parseDishDocument(result.hits.hits);
                } else {
                    winston.info(result);
                }

                winston.info(reply);

                callback(null, reply);
            }
        });
    },

    typeAhead: function(params, callback) {

        var searchBody = {
            index: "autocomplete",
            type: "suggest",
            body: {
                size: 15,
                track_scores: true,
                sort: [{
                    _score: {
                        order: 'desc'
                    }
                }, {
                    count: {
                        order: 'desc'
                    }
                }],
                query: {
                    filtered: {
                        query: {
                            match: {
                                phrase: {
                                    query: params.term //,
                                        //analyzer: 'standard'
                                }
                            }
                        }
                    }
                }
            }
        };

        var filter = ElasticSearch.createFilterFromParams(params, kTypeSuggest);

        if (!filter) return callback(new Error('Filter is missing'));

        searchBody.body.query.filtered.filter = filter;

        ElasticSearch.client().search(searchBody, function(err, result) {

            if (err) {
                callback(err);
                return error.report(err, searchBody);
            } else {
                var reply = [];

                result.hits.hits.forEach(function(hit) {

                    reply.push({
                        term: hit._source.phrase,
                        scope: hit._source.scope,
                        type: hit._source.type
                    })

                });
                winston.info(reply);

                callback(null, reply);
            }
        });
    },

    searchLocation: function(params, callback) {

        var searchBody = {
            index: "autocomplete",
            type: "lsuggest",
            body: {
                track_scores: true,
                sort: [{
                    _score: {
                        'order': 'desc'
                    }
                }, {
                    scope: {
                        "order": "desc"
                    }
                }, {
                    count: {
                        "order": "desc"
                    }
                }],
                query: {
                    filtered: {
                        query: {
                            match: {
                                phrase: {
                                    query: params.term //,
                                        //"analyzer": "standard"
                                }
                            }
                        }
                    }
                }
            }
        };

        //var filter = DishSource.createFilterFromParams(params, kTypeSuggest);
        //
        //if (!filter)
        //{
        //    return callback(new Error('Filter is missing'));
        //}

        //searchBody.body.query.filtered.filter = filter;

        var client = ElasticSearch.client();

        client.search(searchBody, function(err, result) {

            if (err) {
                callback(err);
                return error.report(err, searchBody);

            } else {
                var reply = [];

                var prefix = new RegExp('^merchant.');

                result.hits.hits.forEach(function(hit) {

                    var source = hit._source;
                    source.scope = source.scope.replace(prefix, '');

                    reply.push(source);
                });

                winston.info(reply);

                callback(null, reply);
            }
        });
    },

    merchantAtLocation: function(params, callback) {

        //GET /autocomplete/suggest/_search

        if (params.lon && params.lat) {
            var searchBody = {
                index: "autocomplete",
                type: "suggest",
                body: {
                    query: {
                        filtered: {
                            query: {
                                match: {
                                    scope: "merchant.name"
                                }
                            }
                        }
                    }
                }
            };

            params.distance = 20; //Search for 20 meters around the location

            var filter = ElasticSearch.createFilterFromParams(params, kTypeSuggest);

            if (!filter) return callback(new Error('Filter is missing'));

            searchBody.body.query.filtered.filter = filter;

            ElasticSearch.client().search(searchBody, function(err, result) {

                if (err) {
                    callback(err);
                    return error.report(err, searchBody);
                } else {
                    var reply = [];

                    result.hits.hits.forEach(function(hit) {

                        reply.push({
                            term: hit._source.phrase,
                            scope: hit._source.scope,
                            type: hit._source.type
                        })

                    });
                    winston.info(reply);

                    callback(null, reply);
                }
            });
        } else {
            callback(null, null)
        }
    }
};
