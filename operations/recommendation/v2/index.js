(function() {
    'user strict';
    /**
     *  Module dependencies
     */
    var async = require('async');
    var history = require('../../user-history');
    var merchantSearch = require('../../search/v2/search-elastic');
    var ElasticSearch = require('../../elasticsearch-common');
    var merchantResponse = require('../../merchant-response');
    var winston = require('../../../logger');
    var constants = require('../../../constants');

    var recommendation = {};
    /**
     * Get new recommendations by user Id
     *
     * @method     byUserId
     * @param      {<type>}    userId    { description }
     * @param      {<type>}    query     { description }
     * @param      {Function}  callback  { description }
     */
    recommendation.byUserIds = function(userIds, query, callback) {
        history.historyByUserIds(userIds, query.userId, query.current_rec_id, function(err, result) {
            if (err) return callback(err);
            if (result.prev_recs && result.prev_recs.message) {
                winston.info(result.prev_recs.message);
                return callback(null, {
                    merchants: [],
                    message: result.prev_recs.message
                });
            }
            var merchantArray;
            query.userIds = userIds;
            query.user = result.user;
            query.users = result.users;
            var currentCount = 0;
            async.whilst(function() {
                //Perform test for population

                if (!merchantArray) {
                    merchantArray = [];
                    return true;
                }

                winston.info('Total merchants are being fetched %d still', merchantArray.length);

                if (query.distance) {
                    if (query.distance < constants.distance.max) {
                        return merchantArray.length < constants.merchants.size;
                    } else {
                        return false;
                    }
                } else {
                    if (currentCount < constants.incremental.max) {
                        currentCount++;
                        return merchantArray.length < constants.merchants.size;
                    } else {
                        return false
                    }
                }

            }, function(callback) {
                merchantSearch.fetchIncremental(merchantArray, result, query, function(err, merchants) {
                    var length = merchants && merchants.length ? merchants.length : 0;
                    winston.info('Found more merchants are %d', length);
                    merchantArray = merchantArray.concat(merchants);

                    var dishes = [];
                    if (merchantArray && merchantArray.length && result && result.prev_recs && result.prev_recs && result.prev_recs.prevRecs && result.prev_recs.prevRecs.length && result.prev_recs.prevRecs.length > 0) {
                        var prevRecs = result.prev_recs.prevRecs;
                        dishes = prevRecs.map(function(p) {
                            return p._dish;
                        });
                    }

                    merchantArray = merchantArray.map(function(m) {
                        if (m && m.dishes && m.dishes.length > 0) {
                            m.dishes = m.dishes.filter(function(d) {
                                var index = dishes.indexOf(d._id);
                                if (~index) {
                                    return false;
                                } else {
                                    dishes.push(d._id);
                                    return true;
                                }
                            });
                        }
                        return m;
                    });

                    merchantArray = merchantArray.filter(function(m) {
                        if (m && m.dishes && m.dishes.length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    });

                    callback(err);
                });
            }, function(err) {

                //Call callback with result
                if (err) return callback(err);

                winston.info('Finished incremental fetching for merchants, found merchants ', merchantArray.length);
                /*                if (merchantArray.length === 0) {
                                    return callback(null, {
                                        merchants: [],
                                        message: "No dishes found at location"
                                    });
                                }*/

                merchantResponse.prepare(merchantResponse.type.RECOMMENDATION, merchantArray, query, function(err, recommendation) {

                    if (err) return callback(err);

                    if (recommendation.merchants.length > 0) {
                        winston.info('Total recommendations we got ', recommendation.merchants.length);
                        callback(null, recommendation);
                    } else {
                        var filters = ElasticSearch.createFilterFromParams(query, 'search');

                        var searchBody = ElasticSearch.createSearchBody(null, null, null);

                        searchBody.body = {
                            query: {
                                filtered: {
                                    query: {
                                        match_all: {}
                                    },
                                    filter: filters
                                }
                            }
                        };

                        var client = ElasticSearch.client();

                        client.count(searchBody, function(err, result) {

                            if (err) return callback(err);

                            var message = '';

                            if (result.count > 0) {
                                message = 'No dishes found for profile';
                            } else {
                                message = 'No dishes found at location';
                            }
                            winston.info('We did not get any recommendation due to %s', message);
                            callback(null, {
                                merchants: [],
                                message: message
                            });
                        });
                    }
                });
            });
        });
    };

    recommendation.byContributorId = function(contributorId, userIds, query, callback) {
        history.historyByUserIds(userIds, query.userId, query.current_rec_id, function(err, result) {
            if (err) return callback(err);
            query.merchantsToExclude = [];
            if (result && result.prev_recs && result.prev_recs.merchantsToExclude && result.prev_recs.merchantsToExclude.length > 0) {
                query.merchantsToExclude = result.prev_recs.merchantsToExclude;
            }
            ElasticSearch.fetchDishesFromContributor(contributorId, query, function(err, merchants) {
                if (err) return callback(err);
                query.all = true;
                query.shouldDiscoveries = true;
                merchantResponse.prepare(merchantResponse.type.CONTRIBUTOR, merchants, query, function(err, returnDishes) {

                    if (err) {
                        return callback(err);
                    }
                    returnDishes.merchants = returnDishes.merchants.map(function(m) {
                        winston.info("Merchant Id : ", m._id, " Merchant Name : ", m.name);
                        m.dishes = m.dishes.map(function(d) {
                            winston.info("Dish Id : ", d._id, " Dish Name : ", d.name);
                            winston.info("Dish Scores : ", d.scores);
                            if (!d.scores) {
                                d.scores = {};
                            }
                            return d;
                        });
                        return m;
                    });
                    returnDishes.merchants = returnDishes.merchants.filter(function(m) {
                        if (m.dishes && m.dishes.length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                    callback(returnDishes);
                });
            });
        });
    };

    module.exports = recommendation;

}())
