/*
    REC-ADD-USER
    Methods for folding in new users into the recommendation engine.
 */

var RecEngine = require('./rec-engine-connect');
var User = require('../../models/user');
var Rating = require('../../models/rating');

var Async = require('async');
var winston = require('../../logger');

module.exports = function checkAndFoldInUser (userId, callback)
{

    Async.parallel({

        userProfile: function (callback) {

            User.findById(userId, function (err, user) {

                if (err) return callback (err);

                callback(null, user);
            });
        },

        ratings: function (callback) {

            Rating.ratingsForUser(userId, function(err, ratings) {

                if (err) return callback (err);

                callback (null, ratings);
            })
        }

    }, function (err, data) {

        if (err)
        {
            winston.error('Could not find rating for user', userId, 'while folding in. Error:', err);
            if (callback) callback(err);
        }
        else
        {
            if (data.ratings.length > 0)
            {
                var foldInRatings = [];

                data.ratings.forEach(function (rating) {

                    foldInRatings.push({
                        dishId: rating._dish.toString(),
                        merchantId: rating._merchant.toString(),
                        rating: rating.rating
                    })
                });

                RecEngine.addNewUser(data.userProfile, foldInRatings, function (err, success) {

                    winston.info('Folding in user',{err:err,success:success});

                    if (success)
                    {
                        winston.info('Successfully folded user', userId, 'into the recEngine');
                    }
                    else
                    {
                        if (err) winston.err('Error folding in user to recEngine', err);
                    }

                    if (callback) callback (err, success);
                });
            }
        }
    });

};

