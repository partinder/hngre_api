/*
	RECOMMENDATION
	Methods for finding dishes based on user preferences and fetching predictions for them.
 */
"use strict";

var Async = require('async');
var winston = require('../../logger');
var error = require('../../error');

var Common = require('../api-common');

var User = require('../../models/user');
var Rating = require('../../models/rating');
var Dish = require('../../models/dish');
var Merchant = require('../../models/merchant');
var Recommendation = require('../../models/recommendation');

var ElasticSearch = require('../elasticsearch-common');
var DishResponse = require('../dish-response');


var getHistory = function (userId, currentRecId, callback) {

    var async_queries = {

        user: function (callback) {

            User.findOne({_id: userId}, {login: 0}, function (err, user) {

                if (err) return callback (err);
                if (!user || user.enabled == false) return callback (new Error('Unauthorized user'));

                callback (null, user);
            })
        },

        rated_dishes: Common.wrapAsyncQuery(Rating.distinct('_dish',{_user : userId}))
    };

    if (currentRecId)
    {
        async_queries.prev_recs = function (callback) {

            Recommendation.find({_id: currentRecId}, function (err, recommendation) {

                var prevRecs = recommendation[0].rec_dishes;

                if (err || !_.isArray(prevRecs)) return callback(null, []);

                var merged = [];
                merged = merged.concat.apply(merged, prevRecs);

                winston.info('Previously recommended', merged) ;

                callback(null, merged);

            });
        }
    }


    Async.parallel(async_queries , function userHistory (err, results) {

        if (err) return callback(err);
        callback(null, results);

    });
};


/**
 * Recommendations generated after being fetched from the ElasticSearch server.
 * @param userId
 * @param params
 * @param callback
 */
var getElasticRecommendations = function (userId, params, callback) {

    getHistory(userId, params.current_rec_id, function (err, prelimData) {

        if (err) return callback(err);

        winston.info('Starting incremental fetching for dishes');

        var dishArray;

        Async.whilst(function () {

            //Perform test for population

            if (!dishArray)
            {
                dishArray = [];
                return true;
            }

            winston.info('Disharray length', dishArray.length);

            return (dishArray.length < 10 && params.distance && params.distance < 10000);

        }, function (callback) {
            //Perform elasticsearch query

            fetchIncremental(dishArray, prelimData, params, function (err, dishes) {
                winston.info('Found dishes', dishes);
                dishArray = dishArray.concat(dishes);
                callback (err);
            })

        }, function (err) {

            //Call callback with result
            if (err) return callback (err);

            winston.info('Finished incremental fetching for dishes, found dishes', dishArray.length);

            params.userId = userId;
            params.user = prelimData.user;
            params.shouldPopulateHistory = false;

            DishResponse.prepare(DishResponse.type.RECOMMENDATION, dishArray, params, function (err, recommendation) {

                if (err) return callback(err);

                winston.info(recommendation);

                if (recommendation.dishes.length > 0)
                {
                    callback(null, recommendation);
                }
                else
                {
                    var filters = ElasticSearch.createFilterFromParams(params, 'search');

                    var searchBody = ElasticSearch.createSearchBody(null, null, null);

                    searchBody.body = {
                        query: {
                            filtered: {
                                query:{
                                    match_all: {}
                                },
                                filter: filters
                            }
                        }
                    };

                    var client = ElasticSearch.client();

                    client.count(searchBody, function (err, result) {

                        if (err) return callback (err);

                        var message = '';

                        if (result.count > 0)
                        {
                            message = 'No dishes found for profile';
                        }
                        else
                        {
                            message = 'No dishes found at location';
                        }

                        callback (null, {
                            dishes: recommendation.dishes,
                            message: message
                        });
                    });
                }
            });
        });
    });
}

function fetchIncremental(array, prelimData, params, finalCallback)
{
    if (params.lat && params.lon)
    {
        try
        {
            if (params.distance)
            {
                params.distance += 1000;
            }
            else if (prelimData.prev_recs.params.distance)
            {
                params.distance = prelimData.prev_recs.params.distance;
            }
            else
            {
                throw "No distance in params";
            }
        }
        catch (err)
        {
            params.distance = 1000
        }

        winston.info('Fetching recs at distance', params.distance);
    }

    var filters = ElasticSearch.createFilterFromParams(params, 'search');

    if (!filters) return finalCallback(new Error('Filter is missing'));

    if (!filters.bool)
    {
        var geoDistance = filters.geo_distance;
        delete  filters.geo_distance;

        filters.bool = {
            must: [{geo_distance: geoDistance}]
        }
    }

    //Exclude dishes which have been rated or wishlisted
    if (prelimData.rated_dishes.length > 0 || prelimData.prev_recs) {

        var dishesToExclude =  _.union(prelimData.rated_dishes, prelimData.prev_recs);

        if (!filters.bool.must_not) filters.bool.must_not = [];

        filters.bool.must_not = filters.bool.must_not.concat({
            ids: {
                values: dishesToExclude
            }
        });
    }

    //Apply user's veg preferences
    if (prelimData.user.veg_scale)
    {
        if (!filters.bool.must) filters.bool.must = [];

        filters.bool.must = filters.bool.must.concat({
            terms: {
                veg_type:  prelimData.user.veg_scale
            }
        });
    }

    var searchBody = ElasticSearch.createSearchBody(filters,
        {
            _script : {
                script_file : 'random',
                type : 'number',
                params : {},
                order : 'asc'
            }
        }, params.size - array.length);

    ElasticSearch.client().search(searchBody, function (err, result) {

        if(err)
        {
            finalCallback(err);
            return error.report(err, searchBody);
        }
        else
        {
            var recDishes = [];

            if (result.hits.hits.length > 0)
            {
                recDishes = ElasticSearch.parseDishDocument(result.hits.hits);

                array = array.concat(recDishes);

                if (array.length < 10 && params.distance && params.distance < 10000)
                {
                    if (!prelimData.prev_recs) prelimData.prev_recs = [];

                    recDishes.forEach(function (dish) {
                        prelimData.prev_recs.push(dish._id);
                    });
                }
            }

            finalCallback(null, recDishes);
        }
    });
}


/**
 * Recommendations generated after fetching data from the MongoDB datastore
 * @param userId
 * @param params
 * @param finalCallback
 * @returns {*}
 */
var getMongoRecommendation = function (userId, params, finalCallback) {

	var startTime = Date.now();

	if (params.long && params.lat) {

		params.coordinates = {
			type: "Point",
			coordinates : [parseFloat(params.long), parseFloat(params.lat)]
		};

		delete params.long;
		delete params.lat;

	} else {

		return finalCallback(new Error('Location data is required'));
	}

    getHistory(userId, params.current_rec_id, function aggregateMerchants (err, prelimData) {

		if (err) {

			return finalCallback(err);

		} else {

			//Confirm whether user exists

			if (!prelimData.user) {
				return finalCallback(new Error('User does not exist!'));
			}

			//Find merchants nearest to the user's location.

			Merchant.aggregate(
				[
                    {
                        $geoNear: {
                            near: params.coordinates,
                            spherical: true,
                            distanceField: 'location.distance',
                            maxDistance: 2000  //Needs to be adjusted
                        }
                    },
                    {$match: {status: "completed"}},
                    {$project: {_id: 0, 'dish': '$dishes', 'merchant': '$$CURRENT'}},
                    {$unwind: "$dish"},
                    {$match: {"dish._id": {$nin:prelimData.rated_dishes}, "dish.veg_type":{$in: prelimData.user.veg_scale}, "dish.approve_status": true}}
                    //{$limit: 50}
                ],

				//Sample query modifier for checking if merchant is currently open
				//{$match:{'info.hours.mon':{$elemMatch: {'0': {$lte: 1535}, '1':{$gte: 1535}}}}},
				//{$project: {dist: 1}}

			function (err, results) {


				if (err) return finalCallback(err);

                var dishes = [];

                results.forEach(function (result) {

                    var dish = result.dish;
                    dish._merchant = result.merchant;
                    dishes.push(dish);
                });

                //Find how many times the dish has been recommended before and sort on that basis

                for (var i = 0; i < dishes.length; i++)
                {
                    dishes[i]['rec_count'] = 0;
                }

                if (prelimData.prev_recs.length > 0)
                {
                    //console.log('checking prev recs');recs

                    for (var i = 0; i < dishes.length; i++)
                    {
                        prelimData.prev_recs.some(function (prev_rec) {

                            if (prev_rec._id.equals(dishes[i]._id))
                            {
                                dishes[i].rec_count = prev_rec.count;
                                return true;
                            }
                        });
                    }

                    //console.log('Dishes before:', dishes);

                    dishes.sort(function (a, b) {
                        return a.rec_count - b.rec_count;
                    });

                    //console.log('Dishes after:', dishes);
                }

                //Compile final rec from sorted list
                //Add dish distance from merchant
                //Populate rec list for saving

                var finalRec = dishes.slice(0, 50);
                var recList = [];

                //console.log('Merchants', merchantIDs);
                //console.log('Merchants', merchants);

                finalRec.forEach(function (dish) {
                    dish.merchant = Common.setDataForMerchantProfile(dish.merchant);
                    recList.push({_dish: dish._id, predicted_rating: 0});
                });

                //Save recommendation in DB

                params.veg_scale = prelimData.user.veg_scale;

                var newRecommendation = new Recommendation ({
                    _user : prelimData.user._id,
                    rec_dishes : recList,
                    params : params,
                    time_taken : Date.now() - startTime
                });

                newRecommendation.save(function (err) {

                    if (err) winston.error('ERROR while saving recommendation:', err);
                    else winston.info('Recommendation saved', {recId: newRecommendation._id.toString(), user: userId})
                });

                finalCallback (null, finalRec);

			});
		}
	});
}


module.exports = getElasticRecommendations;
