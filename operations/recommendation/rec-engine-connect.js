/*
    REC-ENGINE-CONNECT
    Methods for interacting with the Hngre recommendation engine.
 */

var request = require('request');
var moment = require('moment');
var winston = require('../../logger');
var config = require('../../config');
var error = require('../../error');

var baseURI = config.recEngine.uri; //URI at which the rec RecEngine is accepting requests.

function status (callback) {

    var path = baseURI + '/status';

    request({uri: path, timeout: 2000}, function (err, response, body) {

        if (!err && response.statusCode == 200)
        {
            winston.info('Rec engine status', body);
            var statusBody = JSON.parse(body);
            callback(statusBody.success);
        }
        else
        {
            callback(false);
            error.report(err || body,{uri: path, timeout: 2000, body: body });            
        }
    });
}

module.exports = {

    status: status,

    predictRatings: function (users, items, callback, country) {

        status(function (isAvailable) {

            if (isAvailable)
            {
                winston.info('Getting predictions', {users:_.pluck(users,'_id'), items:items});
                country = country || "";
                winston.info(country);
                var path = baseURI + '/recommend';//?country='+country;

                if (!_.isArray(users)) users = [users];

                var userData = [];
                
                users.forEach(function (user) {
                    userData.push({
                        userId: user._id,
                        age: moment().diff(user.birthday, 'years'),
                        gender: user.gender
                    });
                });

                var options = {
                    url: path,
                    method: 'POST',
                    json: true,
                    body: JSON.stringify({
                        users: userData,
                        dishes: items
                    })
                };
                winston.info(path);
                winston.info(JSON.stringify(options));
                request(options, function (err, response, body) {

                    if (!err && response.statusCode == 200)
                    {
                        winston.info('predictions are', body);
                        callback (null, body);
                    }
                    else
                    {
                        winston.error(err);
                        winston.error(body);
                        callback (err, {});
                        options.response = body;
                        error.report(err || body, options);                        
                    }
                });
            }
            else
            {
                winston.info('Rec engine was available:', isAvailable);
		        callback (null, {});
            }

        })
    },

    addNewUser: function (user, ratings, callback) {

        status(function (isAvailable) {

            if (isAvailable)
            {
                var path = baseURI + '/update/' + user._id.toString();

                var options = {
                    url: path,
                    method: 'POST',
                    json: true,
                    body: JSON.stringify({
                        ratings: ratings,
                        age: moment().diff(user.birthday, 'years'),
                        gender: user.gender
                    })
                };

                winston.info(options);

                request(options, function (err, response, body) {

                    winston.info(body);

                    if (!err && response.statusCode == 200)
                    {
                        callback(null, body.success);
                    }
                    else
                    {                        
                        callback(err, false);
                        error.report(err || response.statusCode, options);
                    }
                });
            }
            else
            {
                callback (null, false);
            }
        })
    }
};

