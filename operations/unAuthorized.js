(function() {
    'use strict';
    var Unauthorized = require('../models/unAuthorized');
    exports.logUnauthorized = function(request, response) {
        var ip = null;
        try {
            ip = request.headers['X-Forwarded-For'] || request.headers['x-forwarded-for'] || request.info.remoteAddress;
        } catch (err) {
            console.log(err);
        }
        var logme = {
            response: response,
            url: request.url,
            params: request.params,
            payload: request.payload,
            headers: request.headers,
            ip_address: ip
        };
        var unAuthorized = new Unauthorized(logme);
        if (process.env.NODE_ENV === 'production') unAuthorized.save();
    };
}());
