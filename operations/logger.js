/*
    LOGGER
    Sets up logging modules and transports
 */

var winston = require('../logger');

//Setup logentries as a winston transporter
var logentries = require('le_node');

var log = logentries.logger({
    token: '56dee81e-36e3-4ba4-96da-c2b3832de187'
});

log.winston(winston, {level: 'info', levels: {info: 1, error: 2, fatal: 3}});
log.winston.handleExceptions = true;

module.exports = winston;