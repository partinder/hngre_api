(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var Rating = require('../../models/rating');
    var User = require('../../models/user');
    var Recommendation = require('../../models/recommendation');
    var async = require('async');
    var winston = require('../../logger');
    var constants = require('../../constants');

    /**
     * All previous recommended dishes by user id and current rec id
     *
     * @method     historyByUserId
     * @param      {<type>}    userId        { description }
     * @param      {<type>}    currentRecId  { description }
     * @param      {Function}  cb            { description }
     */
    exports.historyByUserIds = function(userIds, userId, currentRecId, cb) {
        var async_queries = {
            users: function(callback) {
                User.find({
                    _id: {
                        $in: userIds
                    },
                    enabled: true
                }, {
                    login: 0,
                    full_contacts: 0,
                    devices: 0
                }, function(err, users) {

                    if (err) return callback(err);
                    if (!users) return callback(new Error('no users'));

                    callback(null, users);
                });
            },
            user: function(callback) {
                User.findOne({
                    _id: userId,
                    enabled: true
                }, {
                    login: 0,
                    full_contacts: 0,
                    devices: 0
                }, function(err, user) {

                    if (err) return callback(err);
                    if (!user) return callback(new Error('Unauthorized user'));

                    callback(null, user);
                });
            },
            rated_dishes: function(callback) {
                Rating.find({
                    _user: {
                        $in: userIds
                    }
                }).distinct('_dish', function(err, ratings) {
                    if (err) return callback(err);
                    if (!ratings) return callback(null, []);
                    ratings = ratings.map(function(rate) {
                        return {
                            _dish: rate.toString()
                        };
                    })
                    return callback(null, ratings);
                });
            }
        };
        if (currentRecId) {
            async_queries.prev_recs = function(callback) {

                Recommendation.findOne({
                    _id: currentRecId
                }, function(err, recommendation) {
                    if (err) return callback(null, [])
                    if (!recommendation) return callback(null, []);
                    var prevRec = {
                        params: recommendation.params,
                        prevRecs: [],
                        merchantsToExclude: []
                    };
                    if (recommendation.type !== "search" && recommendation.rec_merchants && recommendation.rec_merchants.length === constants.merchants.set) {
                        prevRec.message = "no more dishes"
                        return callback(null, prevRec);
                    }
                    recommendation.rec_merchants.forEach(function(rec_merchant) {
                        if (_.isArray(rec_merchant)) {
                            rec_merchant.forEach(function(m) {
                                if (m._dishes && m._dishes.length > 0) {
                                    prevRec.prevRecs = prevRec.prevRecs.concat(m._dishes);
                                }
                                prevRec.merchantsToExclude.push(m._merchant);
                            });
                        } else {
                            prevRec.merchantsToExclude.push(rec_merchant._merchant);
                            if (rec_merchant._dishes && rec_merchant._dishes.length > 0) {
                                prevRec.prevRecs = prevRec.prevRecs.concat(rec_merchant._dishes);
                            }
                        }
                    });

                    if (err || !_.isArray(prevRec.prevRecs)) return callback(null, []);

                    winston.info('Total previous recommended is %d', prevRec.prevRecs.length);

                    callback(null, prevRec);

                });
            }
        }
        async.parallel(async_queries, function(err, results) {
            if (err) return cb(err);
            if (!results.prev_recs) {
                results.prev_recs = {};
                results.prev_recs.prevRecs = [];
            }
            cb(null, results);
        });
    };

}());
