/*
    LOCATION-UPDATE
 */

var Async = require('async');

var User = require('../../models/user');
var Device = require('../../models/device');
var Rating = require('../../models/rating');
var ElasticSearch = require('../../operations/elasticsearch-common');
var Common = require('../../operations/api-common');
var pushIos = require('../../Push/push-ios');
var winston = require('../../logger');
var error = require('../../error');

var HngreMail = require('../../operations/email');


module.exports = {

    trackLocation: function (userId, params, callback) {

        Async.waterfall([

            function (callback) {

                User.findById(userId, function (err, user) {

                    if (err) return callback (err);

                    callback(null, user);
                })
            },

            function (user, callback) {

                if (user.notificationToken)
                {
                    Rating.wishListForUser(userId, function (err, wishList) {

                        if (err) return callback(err);

                        if (wishList.length <= 0) return callback (new Error('No items in wish list'));

                        callback (null, wishList, user.notificationToken);
                    });
                }
                else callback(new Error('User cannot receive notifications'));
            },

            function (wishList, notificationToken, callback) {

                //Check if any wish list item is within 250 meters
                params.distance = 250;

                var filters = ElasticSearch.createFilterFromParams(params, 'search');

                if (!filters) return callback(new Error('ElasticSearch Filter is missing'));

                if (!filters.bool)
                {
                    var geoDistance = filters.geo_distance;
                    delete  filters.geo_distance;

                    filters.bool = {
                        must: [{geo_distance: geoDistance}]
                    }
                }

                //Include dishes which have been wish listed
                if (!filters.bool.must) filters.bool.must = [];

                filters.bool.must = filters.bool.must.concat({
                    ids: {
                        values: wishList
                    }
                });

                //Create search body
                var searchBody = {
                    index:"hngre",
                    type:"dishes",
                    body:{
                        filter : filters,
                    }
                };

                var client = ElasticSearch.client();

                client.search(searchBody, function (err, result) {

                    if (err) { callback (err); return error.report(err,searchBody); };

                    var returnMessage = {


                    }

                });
            }

        ], function (err, result) {

            if (result)
            {
                //
            }

            //TODO complete


        });
    },

    trackVisit: function (userId, params, callback) {

        Async.waterfall([

            function (callback) {

                Device.findOne({user: userId}, function (err, device) {

                    if (device)
                    {
                        callback (null, device);
                    }
                    else
                    {
                        callback(new Error('No device found for user'));
                    }
                });
                //User.findById(userId, function (err, user) {
                //
                //    if (err) return callback (err);
                //
                //    callback(null, user);
                //})
            },
            function (device, callback) {

                params.distance = 100;

                var filters = ElasticSearch.createFilterFromParams(params, 'search');

                if (!filters) return callback(new Error('ElasticSearch Filter is missing'));

                if (!filters.bool)
                {
                    var geoDistance = filters.geo_distance;
                    delete  filters.geo_distance;

                    filters.bool = {
                        must: [{geo_distance: geoDistance}]
                    }
                }

                //Create search body
                var searchBody = ElasticSearch.createSearchBody(
                    filters,
                    {
                        "_geo_distance": {
                            "merchant.location": {
                                "lat":  params.lat,
                                "lon":  params.lon
                            },
                            "order":         "asc",
                            "unit":          "m",
                            "distance_type": "plane"
                        }
                    },
                    1
                );

                var client = ElasticSearch.client();

                client.search(searchBody, function (err, result) {

                     if (err) { callback (err); return error.report(err,searchBody); };

                    var returnMessage;
                    var userInfo;

                    //EMAIL for DEBUG
                    var locUpdate = {
                        user: userId,
                        device: device.token,
                        params: params
                    };

                    //Remove above after testing
                    if (result.hits.hits.length > 0)
                    {
                        var dish = ElasticSearch.parseDishDocument(result.hits.hits[0]);

                        Common.selectMerchant(dish, params);

                        returnMessage = "Looks like you're at: " + dish.merchant.name + ". Have a look at our recommendations!";
                        userInfo = {merchantId: dish.merchant._id};

                        locUpdate.result = {
                            success: true,
                            found: dish.merchant
                        }
                    }
                    else
                    {
                        returnMessage = "Received a visit to " + params.lat + "," + params.lon + ". Could not recognize.";

                        locUpdate.result = {
                            success: false
                        }
                    }

                    var mailer = new HngreMail('internal');
                    mailer.subject = 'Location update';
                    mailer.text = JSON.stringify(locUpdate, null, 4);
                    mailer.send();

                    callback(null, {token: device.token, message: returnMessage, userInfo: userInfo});
                });
            }
        ], function (err, data) {

            if (err) winston.error(error.message);

            callback(null, {success: true});

            if (data.message)
            {
                pushIos.notify(data.token, data.message, data.userInfo);
            }
        });
    }
}