/*
    API-COMMON
    General methods for API required throughout the application.
 */

var User    = require('./../models/user');
var Rating  = require('./../models/rating');
var bcrypt  = require('bcrypt');
var moment  = require('moment-timezone');
var winston = require('../logger');

/**
 * Picks the user data to return for an API request.
 * @param {Object} user
 * @param {function} [callback]
 */
module.exports.setDataForUserProfile = function (user, callback) {

    var returnUser = _.pick(user, '_id', 'first_name', 'last_name', 'name', 'birthday', 'gender', 'veg_scale', 'mixpanel_id', 'profile_image','phone_verified', 'role');

    if (_.isArray(user.login))
    {
        returnUser.login_count = user.login.length;
    }

    if (returnUser.birthday)
    {
        returnUser.birthday = moment(returnUser.birthday).format('MM/DD/YYYY');
    }

    if (callback)
    {
        Rating.count({_user: user._id, rating: {$exists: true}}, function (err, count) {

            if (err)
            {
                winston.error('Error picking up rate count for user', {id: user._id, error: err});
                returnUser.rate_count = 0;
            }
            else returnUser.rate_count = count;

            callback(null, returnUser);
        });
    }
    else
    {
        return returnUser;
    }
}


/**
 * Wraps a MongoDB query for use as an Async function.
 * @param query
 * @returns {Function}
 */
module.exports.wrapAsyncQuery = function (query) {

    return function (callback) {

        if (typeof query.exec === 'function')
        {
            query.exec(function (err, result) {
                if (err) return callback(err);
                return callback (null, result);
            });
        }
        else
        {
            callback(new Error('Invalid query'));
        }
    }

}

/**
 * Calculates the distance between two sets of coordinates in km
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @returns {number}
 */
module.exports.getDistanceFromLatLonInKm = function (lat1,lon1,lat2,lon2) {

    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
};

/**
 * Converts a value in degrees to radians
 * @param deg
 * @returns {number}
 */
function deg2rad(deg) {
    return deg * (Math.PI/180)
}

/**
 * Determines whether the given merchant is open currently
 * @param merchant
 * @param {currentTime} [currentTime]
 * @returns {boolean}
 */
module.exports.merchantOpenStatus = function (merchant, currentTime) {

    var daysArray = [
        'mon',
        'tue',
        'wed',
        'thu',
        'fri',
        'sat',
        'sun'
    ];

    var hours = merchant.hours;
    //winston.info("Hours : ", hours);
    if (hours)
    {
        if (!merchant.timezone) merchant.timezone = 'America/New_York';
        //winston.info("Timezone : ", merchant.timezone);
        if (!currentTime) currentTime = moment().tz(merchant.timezone);

        var day = currentTime.format('e') - 1;
        //winston.info("Day : ", day);
        var timeAtMerchant = parseInt(currentTime.format('Hmm'));
        //winston.info("timeAtMerchant : ", timeAtMerchant);
        var currentDay = daysArray[day];
        //winston.info("Current Date : ", currentDay);
        var currentDaySlots = hours[currentDay];
        //winston.info("Current Day Slots : ", currentDaySlots);
        var isOpen = false;

        if (_.isArray(currentDaySlots))
        {
            //winston.info("Start cheching ");
            currentDaySlots.every(function (slot) {
                winston.info("Slot", slot);
                var from = slot[0];
                var to = slot[1];
                winston.info("To:", to, " From:",from);
                if (to <= from) to += 2400;
                //winston.info("To:", to, " From:",from);
                //winston.info("Condition : ",(timeAtMerchant > from && timeAtMerchant < to) || (timeAtMerchant === from || timeAtMerchant === to))
                if ((timeAtMerchant > from && timeAtMerchant < to) || (timeAtMerchant === from || timeAtMerchant === to))
                {
                    isOpen = true;
                    return false;
                }

                return true;
            });
        }
        //winston.info("Isopen : ",isOpen);
        return isOpen;
    }

    return false;
};


/**
 * Selects the merchant in a dish document based on the parameters
 * @param dish
 * @param params
 */
module.exports.selectMerchant = function (dish, params)
{
    var selectedMerchant;

    if (_.isArray(dish.merchant))
    {
        selectedMerchant = dish.merchant[0];

        if (dish.merchant.length > 1)
        {
            //If merchantId is given, select merchant directly
            if (params.merchantId)
            {
                for (var i = 0; i < dish.merchant.length; i++)
                {
                    var currentMerchant = dish.merchant[i];

                    if (currentMerchant._id === params.merchantId)
                    {
                        selectedMerchant = currentMerchant;
                        break;
                    }
                }

                //Alternatively, can use _.find
            }
            // Or if longitude and latitude are given, get nearest merchant
            else if (params.lon && params.lat)
            {
                var closestDistance = 10000;

                dish.merchant.forEach(function (merchant) {

                    var distance = module.exports.getDistanceFromLatLonInKm(
                        params.lat,
                        params.lon,
                        merchant.location.lat,
                        merchant.location.lon
                    );

                    if (distance < closestDistance)
                    {
                        selectedMerchant = merchant;
                        closestDistance = distance;
                    }
                });
            }
            //Or if search address is given, select the merchant with the closest match
            else if (params.country && params.region && params.locality)
            {
                var greatestMatchCount = 0;

                var baseProperties = ['country', 'region', 'locality'];

                dish.merchant.forEach( function (merchant) {

                    var matchCount = 0;

                    baseProperties.forEach(function (property) {

                        if (params[property].toLowerCase() === merchant['address'][property].toLowerCase()) matchCount++;

                    });

                    if (params.neighbourhood)
                    {
                        merchant.address.neighbourhood.forEach(function (neighborhood) {
                            if (params.neighbourhood.toLowerCase() === neighborhood.toLowerCase()) matchCount++;
                        });
                    }

                    if (params.zip && merchant.zip === params.zip) matchCount++;

                    if (matchCount > greatestMatchCount)
                    {
                        greatestMatchCount = matchCount;
                        selectedMerchant = merchant;
                    }
                });
            }
        }
    }
    else
    {
        selectedMerchant = dish.merchant;
    }

    dish.merchant = selectedMerchant;
};