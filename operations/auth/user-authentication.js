/*
    USER-AUTH
    Methods for user authentication, token generation and validation.
*/

var jwt = require('jsonwebtoken');
var User = require('../../models/user');
var Notification = require('../../models/notification');
var common = require('./../api-common');
var winston = require('../../logger');
var config = require('../../config');
var constants = require('../../constants');

var EmailUser = require('../User-emails/user-event-email');

var authFacebook = require('./facebook-auth');
var authGoogle = require('./google-auth');
var authTemp = require('./temp-auth');
var authDigits = require('./digits-auth');

var request = require('request');

var jwtSecret = "FRscg9TaXm5NdvPlkFU4"; //Private key for signing.       

//Allowed login mechanisms for production are only google and facebook
var allowedLoginMechanisms = ['facebook', 'google', 'digits'];
if (process.env.NODE_ENV !== 'production') allowedLoginMechanisms.push('temp', 'device');

/**
 * Mechanism for connecting existing account without email
 * @param accountData
 */
function connectAccount(accountData) {

}

/**
 * Wraps the callback function for result from user verification
 * @param loginParams
 * @param callback
 * @returns {Function} verification handler
 */
function wrapCallbackForVerification(loginParams, callback) {
    return function(err, userObj, loginData) {

        if (err) return callback(err);

        else {
            if (loginParams._id) {
                userObj._id = loginParams._id;
            }

            if (loginParams.device) {
                userObj.device = {
                    identifier: loginParams.device,
                    agent: loginParams.agent
                };
            } else {
                loginParams.device = "";
            }

            User.findOne({
                "devices.identifier": loginParams.device
            }, function(err, user1) {
                if (err) {
                    winston.error(err);
                }
                if (user1 && user1.devices) {
                    winston.info("Devices count ", user1.devices.length);
                    user1.devices = user1.devices.filter(function(d) {
                        if (d.identifier == loginParams.device) {
                            winston.info("Matched Device Id1 ", loginParams.device)
                            return false;
                        } else {
                            return true;
                        }
                    });
                }
                if (user1) {
                    winston.info("Matched Device Id2 ", loginParams.device)
                    user1.save(function(err) {
                        winston.error(err);
                        User.createOrUpdate(userObj, function(err, user) {

                            if (err) return callback(err);

                            else {
                                var returnBlock = function(err) {

                                    if (err) {
                                        return callback(err);
                                    } else {
                                        var token = createTokenForUser(user);
                                        console.log(token);
                                        common.setDataForUserProfile(user, function(err, returnUser) {

                                            if (err) return callback(err);
                                            else {
                                                callback(null, token, returnUser);

                                                if (returnUser.login_count <= 1) {
                                                    winston.info('Sending Welcome email');
                                                    var form = {
                                                        event: constants.user_registered.event,
                                                        email: {
                                                            name: user.first_name,
                                                            email: user.email
                                                        }
                                                    };
                                                    request.post({
                                                        url: config.notifier.url,
                                                        form: form
                                                    }, function(err, response, body) {
                                                        if (err) {
                                                            winston.error('Error sending mail', {
                                                                err: err,
                                                                email: user.email
                                                            });
                                                        }
                                                    });
                                                    var notification = new Notification({
                                                        user: user._id,
                                                        title: constants.user_registered.setTitle(),
                                                        body: constants.user_registered.setBody(user.first_name),
                                                        path: constants.default.hngre_path,
                                                        initiator: {
                                                            from: constants.user_registered.initiator.from,
                                                            type: constants.user_registered.initiator.type
                                                        },
                                                        icon: constants.default.hngre_icon
                                                    });
                                                    notification.save(function(err) {
                                                        if (err) {
                                                            winston.error('welcome notification creating error ', err);
                                                        }
                                                    })
                                                } else {
                                                    winston.info('No need to send user email');
                                                }
                                            }
                                        });
                                    }
                                };

                                if (loginData) {
                                    loginData = {
                                        type: loginParams.loginType,
                                        date: Date.now(),
                                        data: loginData
                                    };
                                    user.login.push(loginData);
                                    user.save(returnBlock);
                                } else {
                                    returnBlock(null);
                                }
                            }
                        });
                    });
                } else {
                    winston.info("Not Matched Device Id ", loginParams.device);
                    User.createOrUpdate(userObj, function(err, user) {

                        if (err) return callback(err);

                        else {
                            var returnBlock = function(err) {

                                if (err) {
                                    return callback(err);
                                } else {
                                    var token = createTokenForUser(user);
                                    console.log(token);
                                    common.setDataForUserProfile(user, function(err, returnUser) {

                                        if (err) return callback(err);
                                        else {
                                            callback(null, token, returnUser);

                                            if (returnUser.login_count <= 1) {
                                                winston.info('Sending Welcome email');
                                                var form = {
                                                    event: constants.user_registered.event,
                                                    email: {
                                                        name: user.first_name,
                                                        email: user.email
                                                    }
                                                };
                                                request.post({
                                                    url: config.notifier.url,
                                                    form: form
                                                }, function(err, response, body) {
                                                    if (err) {
                                                        winston.error('Error sending mail', {
                                                            err: err,
                                                            email: user.email
                                                        });
                                                    }
                                                });
                                                var notification = new Notification({
                                                    user: user._id,
                                                    title: constants.user_registered.setTitle(),
                                                    body: constants.user_registered.setBody(user.first_name),
                                                    path: constants.default.hngre_path,
                                                    initiator: {
                                                        from: constants.user_registered.initiator.from,
                                                        type: constants.user_registered.initiator.type
                                                    },
                                                    icon: constants.default.hngre_icon
                                                });
                                                notification.save(function(err) {
                                                    if (err) {
                                                        winston.error('welcome notification creating error ', err);
                                                    }
                                                })
                                            } else {
                                                winston.info('No need to send user email');
                                            }
                                        }
                                    });
                                }
                            };

                            if (loginData) {
                                loginData = {
                                    type: loginParams.loginType,
                                    date: Date.now(),
                                    data: loginData
                                };
                                user.login.push(loginData);
                                user.save(returnBlock);
                            } else {
                                returnBlock(null);
                            }
                        }
                    });
                }

            });
        }
    }
}

/**
 * Returns the verification function for the given login type.
 * Function must have the signature (loginData, verificationCallback)
 * @param type  Type of login
 * @returns {*}
 */
function getFunctionForAuthentication(type) {

    //Check if login mechanism is supported
    if (allowedLoginMechanisms.indexOf(type) < 0) return null;

    switch (type) {
        case 'facebook':
            return authFacebook.verifyToken;

        case 'google':
            return authGoogle.verifyToken;

        case 'temp':
            return authTemp.parseLoginData;

        case 'digits':
            return authDigits.digitsAuth;

        case 'device':
            return function(loginData, callback) {

                if (loginData.device) {
                    callback(null, {}, {
                        device: loginData.device
                    })
                } else {
                    callback(new Error('No device identifier found'));
                }
            };

        default:
            return null;
    }
}

/**
 * Creates a JWT token for a given user
 * @param user
 * @returns {*}
 */
function createTokenForUser(user) {
    var date = new Date();
    return jwt.sign({
        user_id: user._id,
        iat: date.toISOString()
    }, jwtSecret);
}

//Validator method for requests
function validateToken(decodedToken, callback) {

}

/**
 * Authenticates user and assigns a JSON Web Token
 * @param loginData Holds either a Facebook/Google token, or email/password, or temp login email and name, with a loginType string
 * @param callback
 * @returns {err, token, user} Either an auth error, or a token with the user profile
 */
module.exports.authenticateUser = function(loginData, callback) {
    //Check for facebook/google token and verify
    //Otherwise check for username/password and verify
    //If correct, create the token and send it back
    //Otherwise, send 401 error.

    winston.info('Authenticating user', {
        loginData: loginData
    });

    var verificationCallback = wrapCallbackForVerification(loginData, callback);

    var functionForAuthentication = getFunctionForAuthentication(loginData.loginType);

    if (functionForAuthentication && functionForAuthentication.length === 2) //Function should have signature with two parameters
    {
        functionForAuthentication(loginData, verificationCallback);
    } else {
        verificationCallback(new Error('Invalid login details'));
    }
};

module.exports.authOptions = {
    key: jwtSecret //,
        //validateFunc: validateToken
};
