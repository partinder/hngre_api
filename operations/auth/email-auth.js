/*
 * Contains the methods for implementation of Basic auth
 */

//TODO FIXME

var basic = require('hapi-auth-basic')
var bcrypt  = require('bcrypt');
var winston = require('../../logger');


//Setting up Basic Authentication
var validate = function (username, password, callback) {
    winston.info('Authenticating with username and password: ', {username:username, password:password});

    var user = {
        username: 'akash',
        password: '$2a$10$0IkDxCqPVYNvgaAV8HvEROGGXUseEv3GQIhSZxPhNzTQzfKMVOgRi',
        id:'someid',
        name:'Akash Gupta'
    };

    if (!(username === user.username)) {
        return callback (new error ('Invalid username'), false);
    }

    Bcrypt.compare(password, user.password, function (err, isValid) {
        callback(err, isValid, { id: user.id, name: user.name });
    });
}


function createBasicProfile (profileData, callback) {

    if (!profileData.email) return callback(new Error('Email is missing'));
    if (!profileData.password) return callback(new Error('Password is missing'));

    var newUser = new User({email: loginData.email});

    bcrypt.hash(profileData.password, null, null, function (err, hash) {

        if (err) return callback(err);
        else
        {

            newUser.save(function (err, user) {
                if (err)
                {
                    callback(err);
                }
                else
                {
                    callback (null, user);
                }
            });
        }
    });
}


/**
 * Verifies the password for a user.
 * @param loginData
 * @param callback
 * @returns {*}
 */
module.exports.verifyUser = function (loginData, callback) {

    if (!loginData.email) return callback(new Error('Email is missing'));
    if (!loginData.password) return callback(new Error('Password is missing'));

    User.findOne({email: loginData.email}, function (err, user) {
        if (err) return callback (err);

        else {
            var existingLoginData = user.loginData;

            if (existingLoginData.password == null) {
                return callback(new Error('Could not find password to compare. Please try another login method'));
            } else {
                bcrypt.compare(loginData.password, existingLoginData.password, function (err, res) {
                    if (res === true) return callback (null, user);
                    else return callback (new Error('Incorrect email/password'));
                });
            }
        }
    });
}

/**
 *
 * @param email
 * @param password
 * @param callback
 */
module.exports.createUserProfileForEmail = function (email, password, callback) {

    var passwordToSave = bcrypt.hash(password, null, null, function (err, hash) {
        if (err) {
            winston.error('password hashing error',err);
            return callback (err);
        } else {

        }
    });
};