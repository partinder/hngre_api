/*
    DIGITS-AUTH
 */

//TODO Verify tokens

function processDigitsAuth (loginData, finalCallback)
{
    if (!loginData.phone || !loginData.digitsToken || !loginData.digitsTokenSecret)
        return finalCallback (new Error('Invalid Digits Login data'));

    var userData = _.pick(loginData, 'phone');
    userData.phone_verified = true;

    var appendLoginData = _.pick(loginData, 'digitsToken', 'digitsTokenSecret');

    finalCallback(null, userData, appendLoginData);

}

module.exports = {

    digitsAuth: processDigitsAuth
};