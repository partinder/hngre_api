/*
    TEMP-AUTH
    Just for quick creation of users based on name and email
    Only available in production
 */

function processTempUser (loginData, callback)
{
    console.log('Processing temp user', loginData);

    if (!loginData.email) return callback(new Error('Email is required'));

    var userObj = _.pick(loginData, 'email', 'name');

    if (userObj.email)
    {
        userObj.email = userObj.email;
        userObj.email_verified = false;
    }
    else
    {
        callback(new Error('Email not found'));
    }

    callback(null, userObj, {});
}

module.exports = {
    parseLoginData: processTempUser
};