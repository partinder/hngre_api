/*

 */

var google = require('googleapis');
var winston = require('../../logger');
var url = require('url');

//Use authorization code to get access token, then get email from Google.
var OAuth2 = google.auth.OAuth2;
var clientID = '161504962992-bosclgismo1h24vn2f58u1e2hm2hkot9.apps.googleusercontent.com';
var clientSecret = 'SnmYxuYGR--XDEPvVzXQF0gp';

/**
 *
 * @returns {OAuth2}
 */
function getOAuthClient() {

    return new OAuth2(clientID, clientSecret);
}

/**
 * Verifies a google token and returns the user.
 * @param loginData
 * @param callback
 * @returns {*}
 */
function verifyGoogleToken(loginData, callback) {

    if (!loginData.authCode) return callback(new Error('Google authorization code is missing'));

    var oAuthClient = getOAuthClient();

    oAuthClient.getToken(loginData.authCode, function(err, tokens) {

        if (err) {
            winston.error('Error getting Google token', {
                authCode: loginData.authCode,
                email: loginData.email,
                error: err
            });
            return callback(new Error('Google authorization error'))
        } else {
            winston.info('Received tokens', tokens);

            getUserProfile(tokens, function(err, userObj) {

                if (err) return callback(err);

                var login = {
                    access_token: tokens.access_token,
                    refresh_token: tokens.refresh_token
                };

                callback(null, userObj, login);
            });
        }
    });
}

/**
 * Gets the user object for a given set of tokens.
 * @param tokens
 * @param callback
 */
function getUserProfile(tokens, callback) {

    var oAuthClient = getOAuthClient();
    oAuthClient.setCredentials(tokens);
    var plus = google.plus('v1');

    plus.people.get({
        userId: 'me',
        auth: oAuthClient
    }, function(err, googleUser) {
        //Handle response, send callback

        if (err) {
            winston.err('Error getting Google+ profile', {
                email: loginData.email,
                error: err
            });
            return callback('Google+ profile error');
        }

        winston.info('Google user\n', googleUser);

        //Extract data from profile.
        var userObj = {};

        googleUser.emails.some(function(email) {

            if (email.type == 'account') {
                userObj.email = email.value;
                userObj.email_verified = true;
                return true;
            }
            return false;
        });

        //Check if email is same as sent from app, if exists
        if (userObj.email.address) {
            if (loginData.email !== userObj.email.address) {
                return callback(new Error('Account mismatch'));
            }
        }

        userObj.google = googleUser.id;

        if (googleUser.displayName) userObj.name = googleUser.displayName;
        if (googleUser.name) {
            if (googleUser.name.givenName) userObj.first_name = googleUser.name.givenName;
            if (googleUser.name.familyName) userObj.last_name = googleUser.name.familyName;
        }
        if (googleUser.birthday) {
            userObj.birthday = Date.parse(googleUser.birthday);
            userObj.brithday_source = 'google';
        }
        if (googleUser.gender) userObj.gender = googleUser.gender.toLowerCase();

        if (googleUser.image) {
            if (!googleUser.image.isDefault) {
                var imageUrlObj = url.parse(googleUser.image.url);

                //Modify the size parameter to 300
                imageUrlObj.search = 'sz=300';

                userObj.profile_image = url.format(imageUrlObj);
            } else {
                userObj.profile_image = null;
            }
        }

        callback(null, userObj);
    });
}

/**
 *
 */
function getConnectedPeople(tokens, callback) {

    var oAuthClient = getOAuthClient();
    var plus = google.plus('v1');
    oAuthClient.setCredentials(tokens);


    plus.people.list({
        userId: 'me',
        collection: 'connected',
        auth: oAuthClient
    }, function(err, response) {

        if (err) return callback(err);

        callback(null, response.items);
    });
}

module.exports = {
    verifyToken: verifyGoogleToken,
    getUserProfile: getUserProfile,
    getConnectedPeople: getConnectedPeople
};
