/*

 */

var request = require('request');
var winston = require('../../logger');
var moment  = require('moment');
var Async   = require('async');

var facebookGraphBaseUrl = 'https://graph.facebook.com';

/**
 * Verifies a facebook token and returns the user.
 * @param loginData
 * @param finalCallback
 * @returns {*}
 */
function verifyFacebookToken (loginData, finalCallback) {

    //Verify token, get email from Facebook
    if (loginData.token == null) return callback(new Error('Facebook token is missing'));

    else
    {
        Async.parallel({

            userObj: function (callback) {

                //Verify token from facebook
                var facebookVerificationURL = facebookGraphBaseUrl + '/me?access_token=' + loginData.token;

                request(facebookVerificationURL, function (err, response) {

                    if (err)
                    {
                        winston.error ('FB verification error:', err);

                        return callback(new Error('Facebook verification failed'));
                    }
                    else
                    {
                        if (response.error)
                        {
                            winston.error('FB verification error: %s', response.error.message);
                            return callback(new Error('Facebook verification failed'));
                        }
                        else
                        {
                            var fbResponse = JSON.parse(response.body);

                            winston.info(fbResponse);

                            if (fbResponse.error)
                            {
                                callback(new Error(fbResponse.error.message));
                            }
                            else
                            {
                                var userObj = _.pick(fbResponse, 'first_name', 'last_name', 'gender', 'birthday', 'name', 'email');

                                if (fbResponse.email)
                                {
                                    userObj.email_verified = true;
                                }

                                userObj.facebook = fbResponse.id;

                                if (userObj.birthday)
                                {
                                    userObj.birthday = moment(new Date(userObj.birthday));
                                    userObj.brithday_source = 'facebook';
                                }

                                if (userObj.gender)
                                {
                                    userObj.gender = userObj.gender.toLowerCase();
                                }

                                winston.info('User from FB', {user: userObj});

                                callback(null, userObj);
                            }
                        }
                    }
                });

            },

            imageUrl: function (callback) {

                getImageUrl(loginData.token, function (imageUrl) {

                    callback(null, imageUrl);

                });
            }
        }, function (err, result) {

            if (err) return finalCallback(err);

            result.userObj.profile_image = result.imageUrl;

            var saveLoginData = {
                token: loginData.token
            };

            finalCallback(null, result.userObj, saveLoginData);
        });


    }
}

/**
 * Gets the image url for a given facebook user Id or access token
 * @param facebookIdOrAccessToken
 * @param callback
 * @returns {*}
 */
function getImageUrl (facebookIdOrAccessToken, callback) {

    winston.info('Getting facebook image for', facebookIdOrAccessToken);

    if (!facebookIdOrAccessToken) return callback();

    var facebookId;
    var accessToken;

    if (facebookIdOrAccessToken.match(/[0-9a-z]*/i))    //Is access token
    {
        facebookId = 'me';
        accessToken = facebookIdOrAccessToken;
    }
    else if (facebookIdOrAccessToken.match(/\d*/))     //Is facebookId
    {
        facebookId = facebookIdOrAccessToken;
    }
    else
    {
        return callback()
    }

    var url = facebookGraphBaseUrl + '/' + facebookId + '/picture?type=square&width=300&height=300&redirect=false';

    if (accessToken) url = url + '&access_token=' + accessToken;

    request(url, function (err, response) {

        if (response.err) return callback();

        var facebookImageObject = JSON.parse(response.body);

        if (facebookImageObject.data && !facebookImageObject.data.is_silhouette) //isSilhouette means image is placeholder
        {
            callback(facebookImageObject.data.url);
        }
        else
        {
            callback();
        }
    });
}

function getLoggedInFriends (token, callback) {

    var url = facebookGraphBaseUrl + '/me/friends?access_token=' + token;

    var foundFriends = [];

    function getFriendsFromUrl(url, cb) {

        request(url, function (err, response) {

            if (err) return cb(err, foundFriends);
            if (response.error) return cb(new Error('Failed to get friend list'), foundFriends);
            if(response && response.statusCode !== 200){
                return cb(response.statusCode, foundFriends);
            }
            response = JSON.parse(response.body);

            if (response.data && response.data.length > 0) {

                foundFriends = foundFriends.concat(response.data);

                if (response.paging && response.paging.next) {
                    getFriendsFromUrl(response.paging.next, cb);
                }else{
                    cb(null, foundFriends);
                }

            } else {
                cb (null, foundFriends);
            }
        });
    }

    getFriendsFromUrl(url, callback);
}

module.exports = {
    verifyToken : verifyFacebookToken,
    getImageUrl: getImageUrl,
    getConnectedPeople: getLoggedInFriends
};