(function() {
    'use strict';

    var Common = require('./api-common');
    var Async = require('async');
    var Rating = require('../models/rating');
    var User = require('../models/user');
    var Device = require('../models/device');
    var Recommendation = require('../models/recommendation');
    var RecEngine = require('./recommendation/rec-engine-connect');
    var winston = require('../logger');
    var constants = require('../constants');
    var ElasticSearch = require('../operations/search/search-elastic');
    var Object = {};

    Object.size = function(obj) {
        var size = 0,
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    exports.suggested = function(recType, merchantDocuments, params, callback) {
        var doLowerCase = function(item) {
            if (item) {
                return item.toLowerCase();
            } else {
                return item;
            }
        }
        merchantDocuments[0].dishes[0].tags = merchantDocuments[0].dishes[0].tags.map(doLowerCase);
        merchantDocuments[0].dishes[0].ingredients = merchantDocuments[0].dishes[0].ingredients.map(doLowerCase);
        merchantDocuments[0].dishes[0].cuisine = merchantDocuments[0].dishes[0].cuisine.map(doLowerCase);
        params.suggestedParams = {
            name: merchantDocuments[0].name,
            dishes: {
                name: merchantDocuments[0].dishes[0].name,
                tags: merchantDocuments[0].dishes[0].tags,
                ingredients: merchantDocuments[0].dishes[0].ingredients,
                cuisine: merchantDocuments[0].dishes[0].cuisine
            }
        };
        var merchantIds = _.pluck(merchantDocuments, "_id");
        params.merchantIds = merchantIds || [];
        ElasticSearch.suggestForMerchant(params, function(err, merchants) {
            if (err) {
                winston.error(err);
                return callback(err);
            }
            winston.info("Merchants ", merchants);
            winston.info("Total length : ", merchants.length);
            if (merchants && merchants.length > 0) {
                prepare(recType, merchants, params, function(err, m) {
                    if (err) {
                        return callback(err);
                    }
                    var suggestedMerchants = [];
                    var processMerchants = [];
                    var done = 0,
                        remains = 3;
                    for (var i = 1; i < constants.fetchmerchants.maxKm; i++) {
                        processMerchants = _.filter(m, function(m1) {
                            var d;
                            if (m1.fields && m1.fields.distance) {
                                d = m1.fields.distance;
                            }
                            if (d && d[0] < i) {
                                return m1;
                            } else {
                                return false;
                            }
                        });
                        winston.info("Before shuffle ",_.pluck(processMerchants,'_id'));
                        m = _.reject(m, function(m1) {
                            var d;
                            if (m1.fields && m1.fields.distance) {
                                d = m1.fields.distance;
                            }
                            if (d && d[0] < i) {
                                return m1;
                            } else {
                                return false;
                            }
                        });
                        winston.info(_.pluck(m,'_id'));
                        winston.info("Suggested Merchants total ", suggestedMerchants.length);
                        if (suggestedMerchants.length >= 3) {
                            break;
                        }
                        if (processMerchants && processMerchants.length > 0) {
                            processMerchants = _.shuffle(processMerchants);
                            winston.info("After shuffle ",_.pluck(processMerchants,'_id'));
                            processMerchants = _.first(processMerchants, remains);
                            winston.info("Total Processes Merchants ", processMerchants.length);
                            if(suggestedMerchants.length>0){
                            	processMerchants.forEach(function(pm){
                            		suggestedMerchants.push(pm);
                            	});
                            }else{
                            	suggestedMerchants = processMerchants;
                            }
                            winston.info("Before Done ", done, " Remains ", remains);                            
                            done = done + processMerchants.length;
                            remains = remains - processMerchants.length;
                            winston.info("After Done ", done, " Remains ", remains);                                                       
                        }
                    }
                    callback(null, suggestedMerchants);
                });
            } else {
                callback(new Error("No merchants found"), []);
            }
        });
    };

    function prepare(recType, merchantDocuments, params, finalCallback) {

        if (!recType) recType = Reommendation.type.RECOMMENDATION;

        _.defaults(params, {
            shouldPopulatePredictions: true,
            shouldPopulateHistory: true,
            shouldCheckOpenStatus: true
        });

        var merchantParams = _.pick(params, 'lat', 'lon', 'country', 'region', 'locality', 'neighbourhood', 'zip');

        //Parallel requests for ratings and recommendations

        if (params.userIds && merchantDocuments.length > 0) {
            var predictItems = [];
            var findRatings = [];
            var countryName = merchantDocuments[0].address.country;
            merchantDocuments.forEach(function(merchant) {
                var dishDocuments = merchant.dishes;
                dishDocuments.forEach(function(dish) {
                    try {
                        predictItems.push({
                            dishId: dish._id,
                            merchantId: merchant._id
                        });

                        findRatings.push({
                            $and: [{
                                _dish: dish._id
                            }, {
                                _merchant: merchant._id
                            }]
                        });
                    } catch (err) {
                        winston.error('Error occured pushing into DishData', {
                            err: err,
                            dishId: dish._id,
                            totalSelectedMerchants: selectedMerchant.length
                        });
                    }
                });
            });

            var asyncFunctions = {};

            if (params.shouldPopulatePredictions && predictItems.length > 0) {
                winston.info('Including func for predictions');
                asyncFunctions.predictions = function(callback) {

                    var recCallback = function(users) {
                        winston.info('Predictions for users', _.pluck(users, 'id'));
                        winston.info('Predictions for users', _.pluck(users, 'birthday'));
                        winston.info('Predictions for users', _.pluck(users, 'gender'));
                        RecEngine.predictRatings(users, predictItems, function(err, predictions) {

                            if (err) {
                                if (err.code === 404) {
                                    winston.error('Predictions Not found for suggested');
                                } else {
                                    return callback(err);
                                }
                            }
                            if (_.keys(predictions).length > 0) {
                                callback(null, predictions);
                            } else {
                                callback(null, null);
                            }

                        }, countryName);
                    };

                    if (params.users) return recCallback(params.users);

                    User.find({
                        _id: {
                            $in: params.userIds
                        }
                    }, function(err, users) {

                        if (err) return callback(err);

                        if (!users || !users.length || users.length === 0) return callback(new Error('Unauthorized user'));
                        if (users.length === 1) {
                            params.user = users[0];
                        }
                        recCallback(users);
                    })
                };
            }

            if (params.shouldPopulateHistory && findRatings.length > 0) {
                winston.info('Including func for history');

                asyncFunctions.history = function(callback) {

                    if (params.history) return callback(null, params.history);

                    Rating.find({
                        _user: params.userId,
                        $or: findRatings
                    }, function(err, ratings) {

                        if (err) return callback(err);

                        callback(null, ratings);
                    });
                }
            }

            if (_.keys(asyncFunctions).length > 0) {
                Async.parallel(asyncFunctions, function(err, data) {

                    if (err) return finalCallback(err);

                    var ratingMap = {}; //Map of ratings for population.
                    var wishList = []; //List of Ids wish listed for population.

                    if (data.history) {

                        data.history.forEach(function(rating) {

                            var dishId = rating._dish.toString();
                            var merchantId = rating._merchant.toString();
                            var userId = rating._user.toString();

                            if (rating.rating && userId == params.userId) {
                                ratingMap[dishId] = rating.rating;
                            }

                            if (rating.eat_later && userId == params.userId) wishList.push(dishId);
                        });
                    }

                    winston.info('Rating map, wish list', {
                        ratingMap: ratingMap,
                        wishList: wishList
                    });

                    var merchantDataToSave = []; //Data to save in recommendation database
                    merchantDocuments = merchantDocuments.map(function(merchant) {
                        var dishes = [];
                        var selectedDishes = [];
                        if (!(merchant.dishes && merchant.dishes.length > 0)) return merchant;
                        var ifNot = JSON.parse(JSON.stringify(merchant.dishes));
                        var dishesScores = {};
                        merchant.dishes = merchant.dishes.map(function(dish) {
                            if (data.predictions) {

                                if (params.users && params.users.length > 0) {
                                    params.users.forEach(function(user) {
                                        if (data.predictions[user.id] && ~user.veg_scale.indexOf(dish.veg_type)) {
                                            //Populate the ratings and wish list status
                                            var prediction;

                                            if (data.predictions[user.id][merchant._id]) {
                                                if (data.predictions[user.id][merchant._id][dish._id]) {
                                                    prediction = data.predictions[user.id][merchant._id][dish._id];
                                                }
                                            }

                                            if (!prediction) {
                                                winston.info('Prediction not found for dish', dish._id);
                                            }

                                            if (!dishesScores[user.id]) {
                                                dishesScores[user.id] = [];
                                            }
                                            dishesScores[user.id].push({
                                                dish: dish._id,
                                                score: prediction
                                            });
                                            //dish.score = prediction;
                                        }
                                    });
                                } else {
                                    if (data.predictions[params.userId] && (data.history || ~params.user.veg_scale.indexOf(dish.veg_type))) {
                                        //Populate the ratings and wish list status
                                        var prediction;

                                        if (data.predictions[params.userId][merchant._id]) {
                                            if (data.predictions[params.userId][merchant._id][dish._id]) {
                                                prediction = data.predictions[params.userId][merchant._id][dish._id];
                                            }
                                        }

                                        if (!prediction) {
                                            winston.info('Prediction not found for dish', dish._id);
                                        }

                                        if (!dishesScores[params.userId]) {
                                            dishesScores[params.userId] = [];
                                        }
                                        dishesScores[params.userId].push({
                                            dish: dish._id,
                                            score: prediction
                                        });
                                        //dish.score = prediction;
                                    }
                                }
                            }

                            if (ratingMap && ratingMap[dish._id]) {
                                dish.rated = ratingMap[dish._id];
                            }
                            if (~wishList.indexOf(dish._id)) {
                                dish.wishlist = true;
                            }
                            return dish;
                        });
                        if (data.predictions) {

                            var dishScores = [];
                            var size = Object.size(dishesScores);
                            var size1 = params.users ? params.users.length : 1;
                            console.log(size, size1);
                            if (size === size1) {
                                for (var property in dishesScores) {
                                    if (dishesScores.hasOwnProperty(property)) {
                                        var dishes_ = _.sortBy(dishesScores[property], 'score')
                                        if (params.all && (size1 === 1 || !size1)) {
                                            var max = dishes_.length;
                                            //dishes_.forEach(function(dish_) {
                                            dishScores.push({
                                                userId: property,
                                                dish: dishes_[max - 1].dish,
                                                scores: dishes_[max - 1].score
                                            });
                                            //});
                                        } else {
                                            var dishes_ = _.sortBy(dishesScores[property], 'score');
                                            var max = dishes_.length;
                                            var dish_ = dishes_[max - 1];
                                            var index = dishes_[max - 1];
                                            while (--max >= 0 && index) {
                                                dish_ = dishes_[max];
                                                index = _.findWhere(dishScores, {
                                                    dish: dish_.dish
                                                });
                                                if (max > 0) {
                                                    if (dishes_[max - 1].score < constants.groupRecommendation.assignAt) {
                                                        break;
                                                    }
                                                }
                                            }
                                            dishScores.push({
                                                userId: property,
                                                dish: dish_.dish,
                                                scores: dish_.score
                                            });
                                        }
                                    }
                                }
                            } else if (params.all) {
                                for (var property in dishesScores) {
                                    if (dishesScores.hasOwnProperty(property)) {
                                        var dishes_ = _.sortBy(dishesScores[property], 'score');
                                        if (params.all && (size1 === 1 || !size1)) {
                                            var max = dishes_.length;
                                            //dishes_.forEach(function(dish_) {
                                            dishScores.push({
                                                userId: property,
                                                dish: dishes_[max - 1].dish,
                                                scores: dishes_[max - 1].score
                                            });
                                            //});
                                        } else {
                                            var dishes_ = _.sortBy(dishesScores[property], 'score');
                                            var max = dishes_.length;
                                            var dish_ = dishes_[max - 1];
                                            var index = dishes_[max - 1];
                                            while (--max >= 0 && index) {
                                                dish_ = dishes_[max];
                                                index = _.findWhere(dishScores, {
                                                    dish: dish_.dish
                                                });
                                                if (max > 0) {
                                                    if (dishes_[max - 1].score < constants.groupRecommendation.assignAt) {
                                                        break;
                                                    }
                                                }
                                            }
                                            dishScores.push({
                                                userId: property,
                                                dish: dish_.dish,
                                                scores: dish_.score
                                            });
                                        }
                                    }
                                }
                            }

                            dishScores = _.sortBy(dishScores, 'dish');
                            var finalDishes = [];
                            var finalDishes0 = {};
                            for (var dishIndex = 0, len = dishScores.length; dishIndex < len; dishIndex++) {
                                if (finalDishes0[dishScores[dishIndex].dish] && finalDishes0[dishScores[dishIndex].dish].scores) {
                                    finalDishes0[dishScores[dishIndex].dish].scores[dishScores[dishIndex].userId] = dishScores[dishIndex].scores;
                                } else {
                                    finalDishes0[dishScores[dishIndex].dish] = {};
                                    finalDishes0[dishScores[dishIndex].dish].scores = {};
                                    finalDishes0[dishScores[dishIndex].dish].scores[dishScores[dishIndex].userId] = dishScores[dishIndex].scores;
                                }
                            }

                            for (var key in finalDishes0) {
                                if (finalDishes0.hasOwnProperty(key)) {
                                    finalDishes.push({
                                        dish: key,
                                        scores: finalDishes0[key].scores
                                    });
                                }
                            }

                            merchant.dishes = merchant.dishes.map(function(dish) {
                                var scores_;
                                for (var i = 0, len = finalDishes.length; i < len; i++) {
                                    if (dish._id == finalDishes[i].dish) {
                                        scores_ = finalDishes[i].scores;
                                        dishes.push({
                                            _dish: dish._id,
                                            scores: scores_
                                        });
                                        break;
                                    }
                                }
                                dish.scores = scores_;
                                if (scores_) {
                                    return dish;
                                } else {
                                    if (params.merchantId) {
                                        dishes.push({
                                            _dish: dish._id,
                                            scores: dish.scores_
                                        });
                                        return dish;
                                    } else {
                                        return undefined;
                                    }
                                }
                            });
                            merchant.dishes = merchant.dishes.filter(function(d) {
                                if (d) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        } else {
                            dishes = merchant.dishes;
                        }

                        //Check and determine whether the merchants are currently open
                        if (params.shouldCheckOpenStatus) {
                            merchant.hours.open_currently = Common.merchantOpenStatus(merchant);
                            winston.info("Merchant : ", merchant.name, " is opened ", merchant.hours.open_currently);
                        }

                        if (merchant.dishes.length < 1) {
                            return undefined;
                        } else {
                            return merchant;
                        }
                    });
                    merchantDocuments = merchantDocuments.filter(function(merchant) {
                        if (merchant) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                    finalCallback(null, merchantDocuments);
                });
            }
        } else {
            finalCallback(null, merchantDocuments);
        }
    }
}());
