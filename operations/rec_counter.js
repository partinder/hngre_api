(function() {
    'use strict';
    /**
     * Module dependencies
     */
    var redis = require("redis"),
        config = require('../config'),
        client = redis.createClient(config.redis.uri),
        User = require('../models/user'),
        async = require('async'),
        error = require('../error');

    client.on("error", function(err) {
        console.log("Error " + err);
        error.report(err, {
            error: 'Redis throwing an error'
        });
    });

    module.exports = function(merchants, params, callback) {
        async.map(merchants, function(merchant, cb1) {
            if (merchant.dishes && merchant.dishes.length && merchant.dishes.length > 0) {
                async.map(merchant.dishes, function(dish, cb2) {
                    client.get(dish._id, function(err, data) {
                        if (err) {
                            console.log(err);
                            return cb2(null, dish);
                        }
                        if (!params.shouldDiscoveries) {
                            if (!data) {
                                dish.rec_dish_count = 1;
                                client.set(dish._id, 1, redis.print);
                            } else {
                                data = parseInt(data) + 1;
                                dish.rec_dish_count = data;
                                client.set(dish._id, data, redis.print);
                            }
                        }

                        if (dish && dish.contributor && dish.contributor._id) {
                            User.findOne({
                                _id: dish.contributor._id
                            }, {
                                name: 1,
                                role: 1,
                                profile_image: 1,
                                website: 1,
                                website_display_name: 1,
                                bio: 1,

                            }, function(err, user) {
                                if (err) {
                                    console.log(err);
                                    return cb2(null, dish);
                                }
                                if (!user) {
                                    return cb2(null, dish);
                                }
                                dish.contributor.name = user.name;
                                dish.contributor.profile_image = user.profile_image;
                                dish.contributor.website = user.website;
                                dish.contributor.website_display_name = user.website_display_name;
                                dish.contributor.bio = user.bio;
                                dish.contributor.role = user.role;
                                client.get(dish.contributor._id, function(err, ds) {
                                    if (err) {
                                        console.log(err);
                                        return cb2(null, dish);
                                    }
                                    if (params.shouldDiscoveries) {
                                        return cb2(null, dish);
                                    } else {
                                        if (!ds) {
                                            dish.contributor.rec_count = 1;
                                            client.set(dish.contributor._id, 1, redis.print);
                                            return cb2(null, dish);
                                        } else {
                                            ds = parseInt(ds) + 1;
                                            dish.contributor.rec_count = ds;
                                            client.set(dish.contributor._id, ds, redis.print);
                                            return cb2(null, dish);
                                        }
                                    }
                                });
                            });
                        } else {
                            return cb2(null, dish);
                        }
                    });
                }, function(err, dishes) {
                    if (err) {
                        console.log(err);
                    }
                    merchant.dishes = dishes;
                    return cb1(null, merchant);
                });
            } else {
                return cb1(null, merchant);
            }
        }, function(err, ms) {
            if (err) {
                console.log(err);
            }
            if (!ms) return callback(null, merchants);
            return callback(null, ms);
        });
    };
}());
