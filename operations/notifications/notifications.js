(function() {
    'use strict';
    var Notification = require('../../models/notification');
    var User = require('../../models/user');
    var winston = require('../../logger');
    var config = require('../../config');
    var request = require('request');
    var async = require('async');

    exports.generateNotifications = function(userIds, data, cb) {
        var members = data.members;
        var groupName = data.groupName;
        winston.info(userIds, data);
        User.find({
            _id: {
                $in: userIds
            }
        }, function(err, users) {
            if (err) {
                winston.error(err);
            }

            var count = 0,
                total = users && users.length > 0 ? users.length : -1;
            async.whilst(function() {
                return count < total
            }, function(callback) {
                var user = users[count];
                count++;
                var notification = new Notification(_.extend({
                    user: user._id
                }, data));
                notification.save(function(err) {
                    if (err) {
                        winston.error('Notification creating error ', err);
                        return callback(new Error('failed to save notification'));
                    }
                    var options = {
                        criteria: {
                            user: user._id
                        }
                    };
                    Notification.loadCount(options, function(err, count) {
                        if (err && count == 0) {
                            winston.error('Notification count loading error', err);
                            count = 1;
                        }
                        var deviceIds = [];
                        var form = null;
                        winston.info('device id ', user.devices);
                        if (user.devices && user.devices.length && user.devices.length > 0) {
                            user.devices.forEach(function(device) {
                                if (device.identifier && device.active && device.gcm_notification && device.gcm_notification.active && device.gcm_notification.token) {
                                    deviceIds.push(device.gcm_notification.token);
                                }
                            });
                            winston.info('Sending Notifications');
                            if (deviceIds && deviceIds.length > 0) {
                                form = {
                                    event: notification.event,
                                    push: {
                                        deviceIds: deviceIds,
                                        title: notification.title,
                                        body: notification.push_body,
                                        icon: notification.icon,
                                        badge: count,
                                        data: {
                                            notification_id: notification._id.toString(),
                                            hngre_api_uri: notification.path
                                        }
                                    }
                                };
                            }
                        }
                        if (!form && user.phone && user.phone_verified) {
                            winston.info('Sending SMS');
                            if (notification.sms_body && ~notification.sms_body.indexOf("me_first_name")) {
                                notification.sms_body = notification.sms_body.replace(/\{\{me_first_name\}\}/g, user.first_name);
                                var member = null;
                                for (var m in members) {
                                    if (members[m].userId.toString() == user._id.toString()) {
                                        member = members[m];
                                        break;
                                    }
                                }
                                if (member) {
                                    notification.sms_body = notification.sms_body.replace(/\{\{deepLink\}\}/g, member.deepLink);
                                }
                            }
                            form = {
                                event: notification.event,
                                sms: {
                                    phone: user.phone,
                                    body: notification.sms_body
                                }
                            };
                        }
                        if (!form && user.email && user.email_verified) {
                            winston.info('Sending Email');
                            var member = null;
                            for (var m in members) {
                                console.log("Matching : ",members[m].userId.toString() == user._id.toString(),members[m].userId.toString(), user._id.toString());
                                if (members[m].userId.toString() == user._id.toString()) {
                                    member = members[m];
                                    break;
                                }
                            }
                            form = {
                                event: notification.event,
                                email: {
                                    groupName: groupName,
                                    me_first_name: data.sender.first_name || "",
                                    me_name: data.sender.name,
                                    me_gender: data.sender.gender,
                                    me_icon: data.sender.icon,
                                    first_name: user.first_name || "",
                                    email: user.email,
                                    deepLink: member.deepLink
                                }
                            };
                        }

                        if (!form) return callback();
                        winston.info(form);
                        request.post({
                            url: config.notifier.url,
                            form: form
                        }, function(err, response, body) {
                            if (err) {
                                winston.error('Error sending notification', err, body);
                                return callback(err);
                            }
                            callback();
                        });
                    })
                });
            }, function(err) {
                if (err) {
                    winston.error(err);
                    return cb(err);
                }
                return cb();
            });
        });
    };

    exports.generateSMS = function(phones, data, cb) {
        winston.info(phones, data);
        var count = 0,
            total = phones && phones.length > 0 ? phones.length : -1;
        async.whilst(function() {
            return count < total
        }, function(callback) {
            var phone = phones[count];
            count++;
            winston.info('Sending SMS');
            var form = {
                event: data.event,
                sms: {
                    phone: phone,
                    body: data.sms_body
                }
            };
            winston.info(form);
            request.post({
                url: config.notifier.url,
                form: form
            }, function(err, response, body) {
                if (err) {
                    winston.error('Error sending sms', err);
                    return callback(err);
                }
                callback();
            });
        }, function(err) {
            if (err) {
                winston.error(err);
                return cb(err);
            }
            return cb();
        });
    };
}())
