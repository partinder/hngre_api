(function() {
    'use strict';
    var async = require('async');
    var User = require('../models/user');
    var config = require('../config');
    var request = require('request');
    var redis = require('redis');
    var client = redis.createClient(config.redis.uri);
    var error = require('../error');

    client.on("error", function(err) {
        console.log("Error " + err);
        error.report(err, {
            error: 'Redis throwing an error'
        });
    });

    exports.merge = function(hngre_generated_id, payload, finalCallback) {

        var updateUser = function(callback) {
            var data = _.pick(payload, 'user_id', 'first_name', 'last_name', 'role');
            User.findOneAndUpdate({
                    _id: data.user_id
                }, {
                    $set: {
                        first_name: data.first_name,
                        last_name: data.last_name,
                        role: data.role,
                    }
                }, {
                    new: true
                },
                function(err, user) {
                    if (err) {
                        console.log(err);
                        return callback(err);
                    }
                    return callback(null, user);
                }
            );
        };

        var updateUserIdInDishes = function(user, callback) {
            request.post({
                url: config.admin.merchants.update.uri,
                form: {
                    hngre_generated_id: hngre_generated_id,
                    user_id: user._id,
                    token: config.admin.server_token
                }
            }, function(err, res, body) {
                if (err || !res || !res.statusCode || res.statusCode !== 200) {
                    console.log(err, body);
                    callback(err);
                } else {
                    console.log(body);
                    callback(null, user);
                }
            });
        };

        var updateUserIdInRedis = function(user, callback) {
            client.get(hngre_generated_id, function(err, data) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                if (!data) {
                    client.set(user._id.toString(), 1, redis.print);
                    return callback();
                } else {
                    client.set(user.id, data, redis.print);
                    return callback(null, user);
                }
            });
        };
        async.waterfall([
                updateUser,
                updateUserIdInDishes,
                updateUserIdInRedis
            ],
            function(err, results) {
                if (err) {
                    console.log(err);
                    return finalCallback(null, {
                        success: false,
                        error: err
                    });
                }
                console.log(results);
                return finalCallback(null, {
                    success: true
                });
            });
    };

}());
