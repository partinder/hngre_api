/**
 * Created by Akash on 07/05/15.
 */

/**
 ELASTICSEARCH-COMMON
 Commonly used methods associated with the Hngre ElasticSearch data store.
 */

var elasticSearch = require('elasticsearch');

var elasticSearchConfig = require('../config').elasticsearch;
var winston = require('../logger');
var constants = require('../constants');

/**
 * Creates and returns an instance of the elasticsearch client on which search queries can be performed
 * @returns {es.Client}
 */
function elasticSearchClient() {
    var clientConfig = {
        host: elasticSearchConfig.uri
    };

    if (elasticSearchConfig.log) clientConfig.log = 'trace';

    return new elasticSearch.Client(clientConfig);
}

/**
 * Fetches the relevant dish document from within the given elasticSearch document, if it exists.
 * @param elasticSearchDocument
 * @returns {*}
 */
function parseDish(elasticSearchDocument) {
    if (elasticSearchDocument._source) {
        var source = elasticSearchDocument._source;
        if (elasticSearchDocument.inner_hits && elasticSearchDocument.inner_hits.dishes && elasticSearchDocument.inner_hits.dishes.hits.hits && elasticSearchDocument.inner_hits.dishes.hits.hits.length > 0) {
            var ds = elasticSearchDocument.inner_hits.dishes.hits.hits;
            source.dishes = source.dishes.filter(function(dish) {
                var flag = false;
                for (var key in ds) {
                    if (ds[key]._source && (dish._id == ds[key]._source._id)) {
                        flag = true;
                        break;
                    }
                }
                console.log("parseDish Id :", dish._id, flag);
                return flag;
            });
        }
        source._id = elasticSearchDocument._id;
        source.fields = elasticSearchDocument.fields;
        source.sort = elasticSearchDocument.sort;
        winston.info(elasticSearchDocument.fields);
        winston.info(elasticSearchDocument.sort);
        return source;
    } else {
        winston.info('Dish with ID not found', elasticSearchDocument._id);
    }
}

/**
 * Gets the dish documents from a given array of documents.
 * @param dishIds
 * @returns {*}
 */
function getDishDocument(dishIds) {
    if (dishIds instanceof Array) {
        var returnData = [];

        dishIds.forEach(function(doc) {

            var dishWithSource = parseDish(doc);
            if (dishWithSource) returnData.push(dishWithSource);
        });

        return returnData;
    } else {
        return parseDish(dishIds);
    }
}

module.exports = {

    client: elasticSearchClient,

    parseDishDocument: getDishDocument,

    /**
     * Fetches the dish documents for the given dish ID(s) from the ElasticSearch server.
     * @param dishIDs
     * @param callback
     */
    fetchDishes: function(dishIDs, callback) {
        var dishes = [];

        dishIDs.forEach(function(dishId) {
            dishes.push({
                _id: dishId
            });
        });

        winston.info('Fetching ', dishes.length);

        var elasticClient = elasticSearchClient();

        elasticClient.mget({
            index: "hngre",
            type: "dishes",
            body: {
                docs: dishes
            }

        }, function(err, result) {
            //winston.info('result', result);

            if (err) return callback(err);
            else {
                var dishDocs = getDishDocument(result.docs);

                callback(null, dishDocs);
            }
        });
    },

    /**
     * Fetches the merchants documents for the given merchant Ids(s) from the ElasticSearch server.
     * @param dishIDs
     * @param callback
     */
    fetchDishesFromMerchant: function(merchantIds, callback) {
        var merchants = [];
        merchantIds = _.uniq(merchantIds);
        merchantIds.forEach(function(merchantId) {
            merchants.push({
                _id: merchantId
            });
        });

        winston.info('Fetching ', merchants.length);

        var elasticClient = elasticSearchClient();

        elasticClient.mget({
            index: "hngre",
            type: "merchants",
            body: {
                docs: merchants
            }

        }, function(err, result) {
            //winston.info('result', result);

            if (err) return callback(err);
            else {
                var dishDocs = getDishDocument(result.docs);

                callback(null, dishDocs);
            }
        });
    },

    fetchDishesFromContributor: function(contributorId, params, callback) {

        var elasticClient = elasticSearchClient();
        var searchBody = {
            index: "hngre",
            type: "merchants",
            body: {
                size: constants.merchants.size,
                from: params.offset,
                query: {
                    filtered: {
                        query: {
                            nested: {
                                path: "dishes",
                                query: {
                                    match: {
                                        "dishes.contributor._id": {
                                            query: contributorId
                                        }
                                    }
                                },
                                inner_hits: {
                                    size: constants.dishes.innerDishes
                                }
                            }
                        },
                        filter: {
                            bool: {
                                must_not: [{
                                    ids: {
                                        values: [] //params.merchantsToExclude
                                    }
                                }]
                            }

                        }
                    }
                }
            }

        };
        if (params.lat && params.lon) {
            searchBody.body.sort = [{
                _geo_distance: {
                    location: {
                        lat: params.lat,
                        lon: params.lon
                    },
                    order: "asc",
                    unit: "km",
                    mode: "min",
                    distance_type: "plane"
                }
            }];
            searchBody.body.query.filtered.filter.bool.must = [{
                geo_distance: {
                    distance: 10000000,
                    location: {
                        lat: params.lat,
                        lon: params.lon
                    }
                }
            }];
            searchBody.body.fields = ["_source"];
            searchBody.body.script_fields = {
                distance: {
                    lang: "groovy",
                    params: {
                        lat: params.lat,
                        lon: params.lon
                    },
                    script: "doc['location'].distanceInKm(lat,lon)"
                }
            };
        } else if (params.country && params.region && params.locality) {
            searchBody.body.query.filtered.filter.bool.must = [{
                query: {
                    query_string: {
                        default_field: "address.country",
                        query: params.country,
                        default_operator: "AND"
                    }
                }
            }, {
                query: {
                    query_string: {
                        default_field: "address.region",
                        query: params.region,
                        default_operator: "AND"
                    }
                }
            }];
            if (params.locality != "null" && params.locality != null && params.locality != "undefined" && params.locality != undefined && params.locality != "Delhi NCR") {
                searchBody.body.query.filtered.filter.bool.should = [{
                    query: {
                        query_string: {
                            default_field: "address.locality",
                            query: params.locality,
                            default_operator: "AND"
                        }
                    }
                }];
            }
        }

        elasticClient.search(searchBody, function(err, result) {
            //winston.info('result', result);        
            if (err) return callback(err);
            else {
                var dishDocs = getDishDocument(result.hits.hits);
                callback(null, dishDocs);
            }
        });
    },

    /**
     * Creates a filter for the type of search query based on the params.
     * @param params
     * @param type
     * @returns {*}
     */
    createFilterFromParams: function filterFromParams(params, type) {

        if (!(type === 'suggest' || type === 'search')) return;

        var filters;

        if (params.lat && params.lon) {
            var distance = params.distance;

            if (!distance) distance = 10000;

            distance = distance.toString() + 'm';

            filters = {
                "geo_distance": {
                    "distance": distance //,
                        //"distance_unit": "mi"
                }
            };

            var coordinates = {
                "lat": params.lat,
                "lon": params.lon
            };

            switch (type) {
                case 'suggest':
                    {
                        filters.geo_distance.location = coordinates;
                    }
                    break;

                case 'search':
                    {
                        filters.geo_distance['merchant.location'] = coordinates;
                    }
                    break;
            }
        } else {

            var address = _.pick(params, 'country', 'region', 'locality', 'neighbourhood', 'zip');

            var availableKeys = _.keys(address);

            if (availableKeys.length > 0) {
                filters = {
                    bool: {
                        must: [],
                        should: []
                    }
                };

                availableKeys.forEach(function(key) {
                    var filter;

                    switch (type) {
                        case 'suggest':
                            {
                                filter = {
                                    term: {}
                                };
                                filter.term[key] = address[key];
                            }
                            break;

                        case 'search':
                            {
                                filter = {
                                    query: {
                                        query_string: {
                                            default_field: 'merchant.address.' + key,
                                            query: address[key],
                                            default_operator: 'AND'
                                        }
                                    }
                                }
                            }
                            break;
                    }

                    if (filter) {
                        switch (key) {
                            case 'locality':
                                {
                                    if (address[key] != "null" && address[key] != null && address[key] != "undefined" && address[key] != undefined && address[key] != "Delhi NCR") {
                                        var splitted = address[key].split(',');
                                        for (var index = 0, len = splitted.length; index < len; index++) {
                                            filters.bool.should.push(filter);
                                        }
                                    }
                                };
                                break;
                            default:
                                {
                                    filters.bool.must.push(filter);
                                }
                        }
                    }
                });
            }
        }

        return filters;
    },
    /**
     * Creates a filter for the type of search query based on the params.
     * @param params
     * @param type
     * @returns {*}
     */
    createFilterFromParamsForMerchant: function filterFromParams(params, type) {

        if (!(type === 'suggest' || type === 'search')) return;

        var filters;

        if (params.lat && params.lon) {
            var distance = params.distance;

            if (!distance) distance = 10000;

            distance = distance.toString() + 'm';


            var coordinates = {
                "lat": params.lat,
                "lon": params.lon
            };

            switch (type) {
                case 'suggest':
                    {
                        filters = {
                            "geo_distance": {
                                "distance": distance,
                                "location": coordinates
                            }
                        };
                        //filters.geo_distance.location = coordinates;
                    }
                    break;

                case 'search':
                    {
                        filters = {
                            bool: {
                                must: [{
                                    "geo_distance": {
                                        "distance": distance,
                                        "location": coordinates
                                    }
                                }]
                            }
                        };
                    }
                    break;
            }
        } else {

            var address = _.pick(params, 'country', 'region', 'locality', 'neighbourhood', 'zip');

            var availableKeys = _.keys(address);

            if (availableKeys.length > 0) {
                filters = {
                    bool: {
                        must: [],
                        should: []
                    }
                };

                availableKeys.forEach(function(key) {
                    var filter;

                    switch (type) {
                        case 'suggest':
                            {
                                filter = {
                                    term: {}
                                };
                                filter.term[key] = address[key];
                            }
                            break;

                        case 'search':
                            {
                                filter = {
                                    query: {
                                        query_string: {
                                            default_field: 'address.' + key,
                                            query: address[key],
                                            default_operator: 'AND'
                                        }
                                    }
                                }
                            }
                            break;
                    }

                    if (filter) {
                        switch (key) {
                            case 'locality':
                                {
                                    if (address[key] != "null" && address[key] != null && address[key] != "undefined" && address[key] != undefined && address[key] != "Delhi NCR") {
                                        var splitted = address[key].split(',');
                                        for (var index = 0, len = splitted.length; index < len; index++) {
                                            filters.bool.should.push({
                                                query: {
                                                    query_string: {
                                                        default_field: 'address.' + key,
                                                        query: splitted[index],
                                                        default_operator: 'AND'
                                                    }
                                                }
                                            });
                                        }
                                    }
                                };
                                break;
                            default:
                                {
                                    filters.bool.must.push(filter);
                                }
                        }
                    }
                });
            }
        }

        return filters;
    },
    createSearchBody: function(filters, sort, size) {

        var body = {};

        if (size) body.size = size;

        if (sort) body.sort = sort;

        if (filters) body.filter = filters;

        return {
            index: "hngre",
            type: "dishes",
            body: body
        };
    },
    createSearchBodyForMerchant: function(filters, sort, size) {

        var body = {};

        if (size) body.size = size;

        if (sort) body.sort = sort;

        if (filters) body.filter = filters;

        return {
            index: "hngre",
            type: "merchants",
            body: body
        };
    }
};
