/*
    EMAIL
    Methods for sending emails
 */

var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');
var winston = require('../logger');


var internalTransporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'hngreapp@gmail.com',
        pass: 'rohithanda1234'
    }
});

var externalTransporter = nodemailer.createTransport(mg({
    auth: {
        api_key: 'key-a184af3ea4a06a679240732867d0435d',
        domain: 'hngremail.com'
    }
}));

function HngreMailer (target)
{
    if (!(target === 'internal' || target === 'external')) throw new Error('Invalid options for creating mailer');

    this.from = '';
    this.to = '';
    this.subject = '';
    this.attachments = [];
    this.target = target;

    this.addAttachment = function (attachment) {

        if (!_.isArray(attachment)) attachment = [attachment];
        var thees = this;
        attachment.forEach(function (att) {
            thees.attachments.push(att);
        });
    };

    this.send = function (callback) {

        var mailOptions = {
            from: this.from || '', // sender address
            to: this.to || '', // list of receivers
            subject: this.subject || '', // Subject line
            attachments: this.attachments || []
        };

        if (this.html) mailOptions.html = this.html;
        else mailOptions.text = this.text;

        switch (this.target)
        {
            case 'internal':
            {
                if (mailOptions.to === '') mailOptions.to = 'akash.gupta@hngre.com';
                if (mailOptions.from === '') mailOptions.from = 'hngreapp@gmail.com';

                internalTransporter.sendMail(mailOptions, callback);
            }
                break;

            case 'external':
            {
                mailOptions.from = 'Rohit H <rohit@hngre.com>';
                externalTransporter.sendMail(mailOptions, callback);

                winston.info('Sending mail with options', mailOptions);

                //mailgun.messages().send(mailOptions, callback);
            }
                break;
        }
    }
}

//var email = new HngreMailer('external');
//email.subject = 'Mailgun test';
//email.text = 'TEST TEST';
//email.to = 'akash.gupta@hngre.com';
//email.send(function (err, info) {
//    console.log('Mailgun test', err, info);
//});

module.exports = HngreMailer;
