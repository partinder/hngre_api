(function() {
    'use strict';
    /**
     * Module dependencies
     */
    /*
    Formats a list of dish documents fetched from the ElasticSearch data store. Functions include:
    - Merchant selection
    - Rating/Wishlist population
    - Recommendation score population
 */

    var Common = require('./api-common');
    var Async = require('async');
    var Rating = require('../models/rating');
    var User = require('../models/user');
    var Device = require('../models/device');
    var Recommendation = require('../models/recommendation');
    var RecEngine = require('./recommendation/rec-engine-connect');
    var winston = require('../logger');
    var constants = require('../constants');
    var suggestions = require('./suggestion');
    var getAndUpdateRec = require('./rec_counter');

    var Object = {};

    Object.size = function(obj) {
        var size = 0,
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    /**
     * Formats a given list of dish documents for response by selecting merchants and/or populating predictions/history
     * @param dishDocuments
     * @param params            Contains data such as user id, merchant id, and flags for population
     * @param finalCallback
     */
    module.exports = {

        prepare: function(recType, merchantDocuments, params, finalCallback) {

            if (!recType) recType = Recommendation.type.RECOMMENDATION;

            _.defaults(params, {
                shouldPopulatePredictions: true,
                shouldPopulateHistory: false,
                shouldCheckOpenStatus: true
            });

            var merchantParams = _.pick(params, 'lat', 'lon', 'country', 'region', 'locality', 'neighbourhood', 'zip');

            //Parallel requests for ratings and recommendations

            if (params.userIds && merchantDocuments.length > 0) {
                var predictItems = [];
                var findRatings = [];
                var countryName = merchantDocuments[0].address.country;
                merchantDocuments.forEach(function(merchant) {
                    var dishDocuments = merchant.dishes;
                    dishDocuments.forEach(function(dish) {
                        try {
                            predictItems.push({
                                dishId: dish._id,
                                merchantId: merchant._id
                            });

                            findRatings.push({
                                $and: [{
                                    _dish: dish._id
                                }, {
                                    _merchant: merchant._id
                                }]
                            });
                        } catch (err) {
                            winston.error('Error occured pushing into DishData', {
                                err: err,
                                dishId: dish._id,
                                totalSelectedMerchants: selectedMerchant.length
                            });
                        }
                    });
                });

                var asyncFunctions = {};

                if (params.shouldPopulatePredictions && predictItems.length > 0 && params.shouldDiscoveries !== true) {
                    winston.info('Including func for predictions');
                    asyncFunctions.predictions = function(callback) {

                        var recCallback = function(users) {
                            winston.info('Predictions for users', _.pluck(users, 'id'));
                            winston.info('Predictions for users', _.pluck(users, 'birthday'));
                            winston.info('Predictions for users', _.pluck(users, 'gender'));
                            RecEngine.predictRatings(users, predictItems, function(err, predictions) {

                                if (err) {
                                    if (err.code === 404) {
                                        winston.error('Predictions Not found');
                                    } else {
                                        return callback(err);
                                    }
                                }
                                if (_.keys(predictions).length > 0) {
                                    callback(null, predictions);
                                } else {
                                    callback(null, null);
                                }

                            }, countryName);
                        };

                        if (params.users) return recCallback(params.users);

                        User.find({
                            _id: {
                                $in: params.userIds
                            }
                        }, function(err, users) {

                            if (err) return callback(err);

                            if (!users || !users.length || users.length === 0) return callback(new Error('Unauthorized user'));
                            if (users.length === 1) {
                                params.user = users[0];
                            }
                            recCallback(users);
                        })
                    };
                }

                if (findRatings.length > 0) {
                    winston.info('Including func for history');

                    asyncFunctions.history = function(callback) {

                        if (params.history) return callback(null, params.history);

                        Rating.find({
                            _user: params.userId,
                            $or: findRatings
                        }, function(err, ratings) {

                            if (err) return callback(err);

                            callback(null, ratings);
                        });
                    }
                }

                if (_.keys(asyncFunctions).length > 0) {
                    Async.parallel(asyncFunctions, function(err, data) {

                        if (err) return finalCallback(err);

                        var ratingMap = {}; //Map of ratings for population.
                        var wishList = []; //List of Ids wish listed for population.

                        if (data.history) {

                            data.history.forEach(function(rating) {

                                var dishId = rating._dish.toString();
                                var merchantId = rating._merchant.toString();
                                var userId = rating._user.toString();

                                if (rating.rating && userId == params.userId) {
                                    ratingMap[dishId] = rating.rating;
                                }

                                if (rating.eat_later && userId == params.userId) wishList.push(dishId);
                            });
                        }

                        winston.info('Rating map, wish list', {
                            ratingMap: ratingMap,
                            wishList: wishList
                        });

                        var merchantDataToSave = []; //Data to save in recommendation database
                        merchantDocuments = merchantDocuments.map(function(merchant) {
                            var dishes = [];
                            var selectedDishes = [];
                            if (!(merchant.dishes && merchant.dishes.length > 0)) return merchant;
                            var ifNot = JSON.parse(JSON.stringify(merchant.dishes));
                            var dishesScores = {};
                            merchant.dishes = merchant.dishes.map(function(dish) {
                                if (data.predictions) {

                                    if (params.users && params.users.length > 1) {
                                        params.users.forEach(function(user) {
                                            if (data.predictions[user.id] && ~user.veg_scale.indexOf(dish.veg_type)) {
                                                //Populate the ratings and wish list status
                                                var prediction;

                                                if (data.predictions[user.id][merchant._id]) {
                                                    if (data.predictions[user.id][merchant._id][dish._id]) {
                                                        prediction = data.predictions[user.id][merchant._id][dish._id];
                                                    }
                                                }

                                                if (!prediction) {
                                                    winston.info('Prediction not found for dish', dish._id);
                                                }

                                                if (!dishesScores[user.id]) {
                                                    dishesScores[user.id] = [];
                                                }
                                                dishesScores[user.id].push({
                                                    dish: dish._id,
                                                    score: prediction
                                                });
                                                //dish.score = prediction;
                                            }
                                        });
                                    } else {
                                        if (data.predictions[params.userId] && (params.shouldPopulateHistory || ~params.user.veg_scale.indexOf(dish.veg_type))) {
                                            //Populate the ratings and wish list status
                                            var prediction;

                                            if (data.predictions[params.userId][merchant._id]) {
                                                if (data.predictions[params.userId][merchant._id][dish._id]) {
                                                    prediction = data.predictions[params.userId][merchant._id][dish._id];
                                                }
                                            }

                                            if (!prediction) {
                                                winston.info('Prediction not found for dish', dish._id);
                                            }

                                            if (!dishesScores[params.userId]) {
                                                dishesScores[params.userId] = [];
                                            }
                                            dishesScores[params.userId].push({
                                                dish: dish._id,
                                                score: prediction
                                            });
                                            //dish.score = prediction;
                                        }
                                    }
                                }

                                if (ratingMap && ratingMap[dish._id]) {
                                    dish.rated = ratingMap[dish._id];
                                }
                                if (~wishList.indexOf(dish._id)) {
                                    dish.wishlist = true;
                                }
                                return dish;
                            });
                            if (data.predictions) {

                                var dishScores = [];
                                var size = Object.size(dishesScores);
                                var size1 = params.users ? params.users.length : 1;
                                console.log(size, size1);
                                console.log(params.shouldPopulateHistory);
                                if (size === size1) {
                                    for (var property in dishesScores) {
                                        if (dishesScores.hasOwnProperty(property)) {
                                            var dishes_ = _.sortBy(dishesScores[property], 'score')
                                            if (params.all && (size1 === 1 || !size1)) {
                                                if (params.shouldPopulateHistory || params.shouldDiscoveries !== true) {
                                                    dishes_.forEach(function(dish_) {
                                                        dishScores.push({
                                                            userId: property,
                                                            dish: dish_.dish,
                                                            scores: dish_.score
                                                        });
                                                    });
                                                } else {
                                                    //all dishes 
                                                    dishes_.forEach(function(dish_) {
                                                        dishScores.push({
                                                            userId: property,
                                                            dish: dish_.dish,
                                                            scores: dish_.score
                                                        });
                                                    });
                                                    //veg_scale matches only
                                                    /*                                                    var max = dishes_.length;
                                                                                                        //dishes_.forEach(function(dish_) {
                                                                                                        dishScores.push({
                                                                                                            userId: property,
                                                                                                            dish: dishes_[max - 1].dish,
                                                                                                            scores: dishes_[max - 1].score
                                                                                                        });
                                                                                                        //});*/
                                                }
                                            } else {
                                                var dishes_ = _.sortBy(dishesScores[property], 'score');
                                                var max = dishes_.length;
                                                var dish_ = dishes_[max - 1];
                                                var index = dishes_[max - 1];
                                                while (--max >= 0 && index) {
                                                    dish_ = dishes_[max];
                                                    index = _.findWhere(dishScores, {
                                                        dish: dish_.dish
                                                    });
                                                    if (max > 0) {
                                                        if (dishes_[max - 1].score < constants.groupRecommendation.assignAt) {
                                                            break;
                                                        }
                                                    }
                                                }
                                                dishScores.push({
                                                    userId: property,
                                                    dish: dish_.dish,
                                                    scores: dish_.score
                                                });
                                            }
                                        }
                                    }
                                } else if (params.all) {
                                    for (var property in dishesScores) {
                                        if (dishesScores.hasOwnProperty(property)) {
                                            var dishes_ = _.sortBy(dishesScores[property], 'score');
                                            if (params.all && (size1 === 1 || !size1)) {
                                                if (params.shouldPopulateHistory || params.shouldDiscoveries !== true) {
                                                    dishes_.forEach(function(dish_) {
                                                        dishScores.push({
                                                            userId: property,
                                                            dish: dish_.dish,
                                                            scores: dish_.score
                                                        });
                                                    });
                                                } else {
                                                    //all dishes 
                                                    dishes_.forEach(function(dish_) {
                                                        dishScores.push({
                                                            userId: property,
                                                            dish: dish_.dish,
                                                            scores: dish_.score
                                                        });
                                                    });
                                                    //veg_scale matches only
                                                    /*                                                    var max = dishes_.length;
                                                                                                        //dishes_.forEach(function(dish_) {
                                                                                                        dishScores.push({
                                                                                                            userId: property,
                                                                                                            dish: dishes_[max - 1].dish,
                                                                                                            scores: dishes_[max - 1].score
                                                                                                        });
                                                                                                        //});*/
                                                }
                                            } else {
                                                var dishes_ = _.sortBy(dishesScores[property], 'score');
                                                var max = dishes_.length;
                                                var dish_ = dishes_[max - 1];
                                                var index = dishes_[max - 1];
                                                while (--max >= 0 && index) {
                                                    dish_ = dishes_[max];
                                                    index = _.findWhere(dishScores, {
                                                        dish: dish_.dish
                                                    });
                                                    if (max > 0) {
                                                        if (dishes_[max - 1].score < constants.groupRecommendation.assignAt) {
                                                            break;
                                                        }
                                                    }
                                                }
                                                dishScores.push({
                                                    userId: property,
                                                    dish: dish_.dish,
                                                    scores: dish_.score
                                                });
                                            }
                                        }
                                    }
                                }

                                dishScores = _.sortBy(dishScores, 'dish');
                                var finalDishes = [];
                                var finalDishes0 = {};
                                for (var dishIndex = 0, len = dishScores.length; dishIndex < len; dishIndex++) {
                                    if (finalDishes0[dishScores[dishIndex].dish] && finalDishes0[dishScores[dishIndex].dish].scores) {
                                        finalDishes0[dishScores[dishIndex].dish].scores[dishScores[dishIndex].userId] = dishScores[dishIndex].scores;
                                    } else {
                                        finalDishes0[dishScores[dishIndex].dish] = {};
                                        finalDishes0[dishScores[dishIndex].dish].scores = {};
                                        finalDishes0[dishScores[dishIndex].dish].scores[dishScores[dishIndex].userId] = dishScores[dishIndex].scores;
                                    }
                                }

                                for (var key in finalDishes0) {
                                    if (finalDishes0.hasOwnProperty(key)) {
                                        finalDishes.push({
                                            dish: key,
                                            scores: finalDishes0[key].scores
                                        });
                                    }
                                }

                                merchant.dishes = merchant.dishes.map(function(dish) {
                                    var scores_;
                                    for (var i = 0, len = finalDishes.length; i < len; i++) {
                                        if (dish._id == finalDishes[i].dish) {
                                            scores_ = finalDishes[i].scores;
                                            dishes.push({
                                                _dish: dish._id,
                                                scores: scores_
                                            });
                                            break;
                                        }
                                    }
                                    dish.scores = scores_;
                                    if (scores_) {
                                        if(params.shouldPopulateHistory && !(~params.user.veg_scale.indexOf(dish.veg_type))){
                                            dish.scores = {};
                                        }
                                        return dish;
                                    } else {
                                        if (params.merchantId) {
                                            dishes.push({
                                                _dish: dish._id,
                                                scores: dish.scores_
                                            });
                                            return dish;
                                        } else if (recType == Recommendation.type.SEARCH) {
                                            return dish;
                                        } else {
                                            return undefined;
                                        }
                                    }
                                });
                                merchant.dishes = merchant.dishes.filter(function(d) {
                                    if (d) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                                /*                                if (merchant.dishes.length < 1) {
                                    merchant.dishes = ifNot;
                                    dishes = ifNot;
                                }*/
                            } else {
                                dishes = merchant.dishes.map(function(d) {
                                    return {
                                        _dish: d._id
                                    }
                                });
                            }

                            //Check and determine whether the merchants are currently open
                            if (params.shouldCheckOpenStatus) {
                                merchant.hours.open_currently = Common.merchantOpenStatus(merchant);
                                winston.info("Merchant : ", merchant.name, " is opened ", merchant.hours.open_currently);
                            }

                            if (merchant.dishes.length < 1) {
                                return undefined;
                            } else {
                                merchantDataToSave.push({
                                    _merchant: merchant._id,
                                    _dishes: dishes
                                });
                                return merchant;
                            }
                        });
                        merchantDocuments = merchantDocuments.filter(function(merchant) {
                            if (merchant) {
                                return true;
                            } else {
                                return false;
                            }
                        });

                        getAndUpdateRec(merchantDocuments, params, function(err, merchants) {
                            if (err) {
                                console.log(err);
                            } else {
                                merchantDocuments = merchants;
                            }
                            //Save or update the recommendation
                            var recCallback = function(err, recommendation) {

                                if (err) return callback(err);

                                winston.info('Rec finalCallback');
                                if (params.scope && merchantDocuments.length <= constants.fetchmerchants.suggestAfterMax && merchantDocuments.length >= constants.fetchmerchants.suggestAfterMin) {
                                    suggestions.suggested(recType, merchantDocuments, params, function(err, suggestedMerchants) {
                                        if (err) {
                                            finalCallback(null, {
                                                recId: recommendation._id,
                                                merchants: merchantDocuments
                                            });
                                        } else {
                                            finalCallback(null, {
                                                recId: recommendation._id,
                                                merchants: merchantDocuments,
                                                suggested: suggestedMerchants
                                            });
                                        }
                                    });
                                } else {
                                    finalCallback(null, {
                                        recId: recommendation._id,
                                        merchants: merchantDocuments
                                    });
                                }
                            };

                            if (_.keys(merchantDataToSave).length > 0 && params.shouldDiscoveries !== true) {
                                //winston.info('params available', params);

                                var paramsToSave = _.pick(params, 'distance', 'lon', 'lat', 'country', 'region', 'locality', 'neighbourhood', 'zip', 'term', 'scope', 'offset');

                                winston.info('Params to save', paramsToSave);

                                //If rec id exists, update the rec
                                if (params.current_rec_id) {
                                    Recommendation.findByIdAndUpdate(
                                        params.current_rec_id, {
                                            $push: {
                                                rec_merchants: merchantDataToSave,
                                                time_sent: Date.now()
                                            },
                                            $set: {
                                                params: paramsToSave
                                            }
                                        }, {
                                            upsert: true
                                        },
                                        recCallback);
                                    //Upsert is set to true for safety
                                } else {
                                    //Otherwise create a new one
                                    var saveData = {
                                        _user: params.userId,
                                        rec_merchants: [merchantDataToSave],
                                        params: paramsToSave,
                                        type: recType,
                                        time_sent: [Date.now()]
                                    };
                                    if (params.group_id) {
                                        saveData.group_id = params.group_id;
                                    }
                                    Recommendation.create(saveData, recCallback);
                                }
                            } else {
                                return finalCallback(null, {
                                    merchants: merchantDocuments
                                });
                            }
                        });
                    });
                }
            } else {
                if (params.scope && merchantDocuments.length <= constants.fetchmerchants.suggestAfterMax && merchantDocuments.length >= constants.fetchmerchants.suggestAfterMin) {
                    suggestions.suggested(recType, merchantDocuments, params, function(err, suggestedMerchants) {
                        finalCallback(null, {
                            merchants: merchantDocuments,
                            suggested: suggestedMerchants
                        });
                    });
                } else {
                    finalCallback(null, {
                        merchants: merchantDocuments
                    });
                }
            }
        },

        type: Recommendation.type
    };
}());
