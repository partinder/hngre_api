(function() {
    'use strict';
    var request = require('request');
    var apiKey = require('../../config').fullContact.apiKey;
    var protocol = require('../../config').fullContact.protocol;
    var winston = require('../../logger');

    var fullContact = {};

    fullContact.getContactsByEmail = function(email, callback) {
        var url = protocol + '://api.fullcontact.com/v2/person.json?email=';
        url += email + '&apiKey=' + apiKey;
        request.get(url, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                callback(null, body);
            } else {
                error = error || response.statusCode;
                callback(error, null);
            }
        });
    };
    module.exports = fullContact;
}())