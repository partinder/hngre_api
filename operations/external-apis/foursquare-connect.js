/*
    FOURSQUARE-CONNECT
    Relevant methods for connecting with the Foursquare API.
 */

var clientId = 'LJUOIMU3152FI0OVYPWWB3JSAUEC2YTK3XPY3IBBR1KVXPGZ';
var clientSecret = 'Z3X3TCEXO5R12KEU3FSBC5TGRCBE55Q3PVLLO4X5WRR2VKVG';
var winston = require('../../logger');

var foursquare = require('node-foursquare-venues')(clientId, clientSecret);

var daysArray = [
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat',
    'sun'
];

function transformFoursquareHours (foursquareHours)
{
    var transformed = {};

    daysArray.forEach(function(day) {
        transformed[day] = [];
    });

    foursquareHours.forEach(function (timeFrame) {

        timeFrame.days.forEach(function (day) {

            var openSlots = timeFrame.open;

            var slots = [];

            openSlots.forEach(function (openSlot) {

                slots.push([parseInt(openSlot.start), parseInt(openSlot.end)]);
            });

            transformed[daysArray[day-1]] = slots;
        });
    });

    return transformed;
}

function getHours (venueId, callback) {

    foursquare.venues.hours(venueId, function (err, result) {

        if (err) return callback(err);

        //Transform hours into readable format
        if (result.meta.code === 200)
        {
            if (result.response.hours.timeframes)
            {
                callback (null, transformFoursquareHours(result.response.hours.timeframes));
            }
            else
            {
                callback(new Error('No hours data available'));
            }
        }
        else
        {
            callback(new Error('Problem getting hours'))
        }
    });
}

//Testing
//var testVenueId = '4b67772af964a520ae502be3';
//
//getHours(testVenueId, function (err, res) {
//
//    if (err) throw err;
//
//    winston.info('Hours:', JSON.stringify(res, null, 4));
//});

module.exports = {
    hours: getHours
};

