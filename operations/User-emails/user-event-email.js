/*
    USER-EVENT-EMAIL
    Handles the sending of emails to users for various events.
 */

var HngreMailer = require('../email');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');

var welcomeEmailDir = path.join(__dirname, 'templates', 'welcome-email');

module.exports = {

    welcome: function (user, callback) {

        if (!callback) callback = function(){};

        if (user.email)
        {
            var welcomeEmail = new EmailTemplate(welcomeEmailDir);

            var renderObj = {};

            if (user.first_name) renderObj.name = user.first_name;
            else if (user.name) renderObj.name = user.name;
            else renderObj.name = 'there';

            welcomeEmail.render(renderObj, function (err, result) {

                if (err) throw err;

                if (result.html)
                {
                    //Finally, send the mail
                    var email = new HngreMailer('external');
                    email.to = user.email;
                    email.subject = 'Welcome to Hngre';
                    email.html = result.html;
                    email.send(callback);
                }
                else
                {
                    callback(new Error('HTML not rendered'));
                }
            })
        }
        else
        {
            callback (new Error('User email not found'));
        }
    }
}