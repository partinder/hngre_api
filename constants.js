(function() {
    module.exports = {
        frient_invite: {
            event: "friend-invite",
            setTitle: function() {
                return "Hngre";
            },
            setBody: function(me_first_name, me_gender, group_name) {
                me_gender = me_gender === "male" ? "his" : me_gender === "female" ? "her" : "their";
                me_first_name = me_first_name || "";
                return me_first_name + " added you to " + me_gender + " group " + group_name + ".";
                //return me_first_name + " added you to " + me_gender + " group " + group_name + ".";
            },
            setPushBody: function(me_first_name, me_gender, group_name) {
                me_gender = me_gender === "male" ? "his" : me_gender === "female" ? "her" : "their";
                me_first_name = me_first_name || "";
                return me_first_name + " added you to " + me_gender + " group " + group_name + ".";
                //return me_first_name + " added you to " + me_gender + " group " + group_name + ". Check out the group."
            },
            setSmsBodyForExistingUser: function(me_first_name, me_gender, group_name, deepLink, me_name, reciever_first_name) {
                me_gender = me_gender === "male" ? "his" : me_gender === "female" ? "her" : "their";
                return "Hey " + (reciever_first_name || "") + ". " + (me_name || "") + " added you to " + me_gender + " group " + group_name + ".\n\nCheck the group out- " + deepLink;
                //return "Hey. " + (me_first_name || "") + " has added you to " + me_gender + " group " + group_name + ".\n\nCheck out the group here- " + deepLink;
            },
            setSmsBodyForNewUser: function(reciever_first_name, me_first_name, me_name, me_gender, deepLink) {
                me_gender = me_gender === "male" ? "him" : me_gender === "female" ? "her" : "them";
                return "Hi " + (reciever_first_name || "") + ". " + (me_name || "") + " has invited you to plan a meal. Help " + me_gender + " find the perfect spot.\n\nJoin " + me_first_name + " on Hngre here- " + deepLink;
                //return "Hi " + (reciever_first_name || "") + ". " + (me_name || "") + " has invited you to plan a meal. Help " + me_gender + " find the perfect spot.\n\nJoin " + me_first_name + " on Hngre here- " + deepLink;            
            },
            to: "inapp-push",
            initiator: {
                from: "internal",
                type: "user"
            }
        },
        user_registered: {
            event: "user-registered",
            setTitle: function() {
                return "Hngre";
            },
            setBody: function(first_name) {
                return "Welcome to Hngre. Help us know you better - rate 10 dishes you’ve eaten.";
                //return "Welcome to Hngre " + (first_name || "") + ". Help us personalize our recommendations for YOU. Rate at least 10 dishes that you’ve eaten.";
            },
            initiator: {
                from: "hngre",
                type: "welcome"
            }
        },
        friend_invite_resolve: {
            event: "friend-invite-resolved",
            setTitleForMe: function() {
                return "Hngre";
            },
            setBodyForMe: function(group_name) {
                return "Yay! You’ve successfully joined the group " + group_name + ".";
            },
            setTitleForUser: function() {
                return "Hngre";
            },
            setBodyForUser: function(user_first_name, group_name) {
                return user_first_name + " has joined the group " + group_name;
                //return user_first_name + " joined the group " + group_name + ".";
            },
            setSmsBodyForUser: function(user_first_name, group_name, user_name) {
                return "Hey {{me_first_name}}.  " + user_name + " has joined the group " + group_name + ".\n\nCheck the group out- {{deepLink}}"
                    //return "Hi {{me_first_name}}. Your friend " + user_first_name + " just joined the group " + group_name + " on Hngre."
            },
            setPushBodyForUser: function(user_first_name, group_name) {
                return user_first_name + " has joined the group " + group_name;
                //return "Your friend " + user_first_name + " has joined the group " + group_name;
            },
            initiator: {
                from: "internal",
                type: "user"
            },
            toForMe: "inapp",
            toForUser: "inapp-push"
        },
        hngre_error: {
            event: "hngre-error"
        },
        rate_dish: {
            event: "friend-invite-resolved",
            setBody: function(dishId, rating_rating) {
                return "You rated the dish id" + dishId + " as " + rating_rating;
            },
            setTitle: function() {
                return "You rated the dish";
            },
            to: "inapp-push",
            initiator: {
                from: "internal",
                type: "user"
            }
        },
        default: {
            setDishPath: function(dish_id, merchant_id, title) {
                return "hngre://dish?dishId=" + dish_id + "&merchantId=" + merchant_id + "&title=" + title;
            },
            setMerchantPath: function(merchant_id, title) {
                return "hngre://merchant?merchantId=" + merchant_id + "&title=" + title;
            },
            setSearchPath: function(data) {
                //term, scope, title, latitude, longitude, country, region, locality, neighbourhood, zip_code
                if (data.latitude && data.longitude) {
                    return "hngre://search?term=" + data.term + "&scope=" + data.scope + "&title=" + data.title + "&lat=" + data.latitude + "&lon=" + data.longitude;
                } else if (data.country && data.region && data.locality && data.neighbourhood) {
                    return "hngre://search?term=" + data.term + "&scope=" + data.scope + "&title=" + data.title + "&country=" + data.country + "&region=" + data.region + "&locality=" + data.locality + "&neighbourhood=" + data.neighbourhood;
                } else if (data.country && data.region && data.locality && data.zip_code) {
                    return "hngre://search?term=" + data.term + "&scope=" + data.scope + "&title=" + data.title + "&country=" + data.country + "&region=" + data.region + "&locality=" + data.locality + "&zipcode=" + data.zip_code;
                } else {
                    return "hngre://search?term=" + data.term + "&scope=" + data.scope + "&title=" + data.title;
                }
            },
            setGroupPath: function(group_id) {
                //return "hngre://group";//?groupId=" + group_id;
                return "hngre://group?groupId=" + group_id;
            },
            setGroupDeepPath: function(group_id) {
                return "group?groupId=" + group_id;
            },
            hngre_path: "hngre://hngreindex",
            hngre_icon: "http://d2bdolvcqygmw9.cloudfront.net/commons/notification-default1.png",
            group_default_icon: "http://d2bdolvcqygmw9.cloudfront.net/commons/group-default.png",
            user_default_icon: "http://d2bdolvcqygmw9.cloudfront.net/groups/t_1448887477917.png"
        },
        range: {
            min: 99,
            max: 101,
            decrease: 1,
            specific: 70
        },
        distance: {
            min: 0,
            max: 15000,
            gap: 3000
        },
        merchants: {
            size: 7,
            gap: 1,
            set:200
        },
        fetchmerchants: {
            size: 100,
            suggestAfterMin: 1,
            suggestAfterMax: 1,
            total: 3,
            maxKm: 10
        },
        dishes: {
            pickDishesFrom: 6 - 1,
            innerDishes: 100
        },
        incremental: {
            max: 5
        },
        groupRecommendation: {
            assignAt: 50
        },
        cloudfront: {
            host: 'http://d2bdolvcqygmw9.cloudfront.net/',
            user: {
                folder: 'users/',
                prefix: 't_'
            },
            group: {
                folder: "groups/",
                prefix: 't_'
            },
            contribution: {
                folder: "contributions/",
                prefix: 'o_'
            }
        },
        locations: [{
            name: "New York",
            icon: "http://d2bdolvcqygmw9.cloudfront.net/commons/ny.png",
            enabled: true,
            location: {
                country: "United States",
                region: "New York",
                locality: "New York"
            }
        }, {
            name: "Delhi NCR",
            icon: "http://d2bdolvcqygmw9.cloudfront.net/commons/delhi-ncr.png",
            enabled: true,
            location: {
                country: "India",
                region: "Delhi NCR",
                locality: "Delhi NCR"
            }
        }/*, {
            name: "San Francisco",
            icon: "http://d2bdolvcqygmw9.cloudfront.net/commons/sf.png",
            enabled: false,
            location: {
                country: "United States",
                region: "California",
                locality: "San Francisco"
            }
        }*/]
    }
}());
